<?php

if ( ! defined('BASEPATH') )
    exit( 'No direct script access allowed' );

class Ippad{
    
    
      public function __construct()
      {
          
      }
    
      public static function loginIppad($username, $password, $ip){
		$command ="cmd=7200&loginid=".$username."&password=".$password."&";
		return self::socket($command, $ip);
                
      }
      
      public static function call($ip){
		$command = "cmd=7410&";
		return self::socket($command, $ip);
      }
      
      public static function executePhone($phone, $ip){					
		$command = "cmd=7400&dnis=".$phone."&";
		return self::socket($command, $ip);
        }
      public static function statusIppad($ip){
        $command = "cmd=7290&";
        $string =  self::socket($command, $ip);
        $pieces = explode("&", $string);
        array_pop($pieces); 
        return self::getStatusClick($pieces);
        }
      
      public static function setCrm($idCrm, $ip)
      {
        $command = "cmd=7270&idcrm=".$idCrm."&linea=0&";
		return self::socket($command, $ip);
      }
	  
	  public static function setTransf($destino, $ip)
      {
        $command = "cmd=7442&linea=0&destino=*888&";
		return self::socket($command, $ip);
      }
	  public static function setHold($ip)
      {
        $command = "cmd=7430&linea=0&";
		return self::socket($command, $ip);
      }


      public static function idCall($ip){
		$command = "cmd=7272&nombre=idLlamada&linea=0&";
		return self::socket($command, $ip);  
      }
      

      private static function socket($command, $ip){
		$fread  = '';
		$fp = stream_socket_client("udp://".$ip.":8544", $errno, $errstr);
		if(!$fp){
			throw new UnexpectedValueException("Failed to connect:". $errno ."-". $errstr);
		}else{
			fwrite($fp, $command);
                        stream_set_timeout($fp, 2);
			$fread = fread($fp, 65535);
			$info = stream_get_meta_data($fp);
			fclose($fp);
		}
        
            if ($info['timed_out']) {
                return FALSE;
            } else {
                return $fread;
            }
        
	}
 }



