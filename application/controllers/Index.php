<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends General {

        public function __construct(){
                parent::__construct();
                $this->load->library('session');
        }

        public function changeLang(){
           return empty($this->session->lang) ? $var['flag']='es' :$var['flag']= $this->session->lang;

        }
        public function index(){
            $this->lang->load($var['flag'] = $this->changeLang(),'language');
            $this->load->view('layouts/header');
            $this->load->view('home');
            $this->load->view('layouts/footer');
        }
        public function nosotros(){
          $this->lang->load($var['flag'] = $this->changeLang(),'language');
          $this->load->view('layouts/header');
      		$this->load->view('nosotros');
      		$this->load->view('layouts/footer');
        }
        public function staff(){
          $this->lang->load($var['flag'] = $this->changeLang(),'language');
          $this->load->view('layouts/header');
          $this->load->view('staff');
          $this->load->view('layouts/footer');
        }
        public function tax(){
          $this->lang->load($var['flag'] = $this->changeLang(),'language');
          $this->load->view('layouts/header');
          $this->load->view('tax');
          $this->load->view('layouts/footer');
        }
        public function mobility(){
          $this->lang->load($var['flag'] = $this->changeLang(),'language');
          $this->load->view('layouts/header');
          $this->load->view('mobility');
          $this->load->view('layouts/footer');
        }
        public function headhunting(){
          $this->lang->load($var['flag'] = $this->changeLang(),'language');
          $this->load->view('layouts/header');
          $this->load->view('headhunting');
          $this->load->view('layouts/footer');
        }
        public function talent(){
          $this->lang->load($var['flag'] = $this->changeLang(),'language');
          $this->load->view('layouts/header');
          $this->load->view('talent');
          $this->load->view('layouts/footer');
        }
        public function contacto(){
          $this->lang->load($var['flag'] = $this->changeLang(),'language');
          $this->load->view('layouts/header');
          $this->load->view('contacto');
          $this->load->view('layouts/footer');
        }


}
