<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Json extends General
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function changelanguage(){
      //$this->session->sess_destroy();
      header('Content-Type: application/json');
      $data 		= $this->input->post();
      $langs = array('lang'=>$data['language']);
      $this->session->set_userdata($langs);
      echo json_encode('true');
      //print_r($this->session->lang);
      //die();
    }

    public function senContact(){
  		header('Content-Type: application/json');
  		$data    = $this->input->post();
   		$datosjson = array('nombre' => $data['cname'] ,'apellido'  => $data['clastname'],'mail' => $data['cemail'], 'telefono' => $data['cphone'], 'asunto' => $data['cquestion'],'mensaje' => $data['cmessage']);
                  $mail = array(
                      "systemName" => "Contacto A3o",
                      "campaignID" => 56,
                      "emailAccount" => 'romina.cedeno@a3ogroup.com',
                      "cellphoneNumber" => '',
                      "SMStext" => '',
                      "clientID" => 0,
                      "jsonVariables" => json_encode($datosjson),
                      "cas" => "",
                  );
  				$enviar = $this->setMethodCI("http://201.161.41.136/mailingProtec/client.php?wsdl", "useMailingProtec", $mail);
  				$enviar = json_decode($enviar,true);
  				$respuesta = 0;
                  if ($enviar['responseEmail'] == 'Email successfully sent') {
  					$respuesta = 1;
  				}
  				echo json_encode($respuesta);

  	}

}
