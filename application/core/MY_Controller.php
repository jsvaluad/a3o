<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//extendemos el controlador base de codeigniter
class Mastercontroller extends CI_Controller
{
	public function __construct()
        {
		parent::__construct();
                

	}
}

//aquí tenemos todo lo que la clase Unodepiera_Controller tiene
class General extends Mastercontroller
{
	public function __construct(){
		parent::__construct();
                //$this->load->library('session');
                $this->load->library('nativesession');
	}
	//añadimos otra función
	public function startSession($path=''){

        $baseUrl = base_url();
       
        if(!isset($_SESSION['std_idUsuario'])){
          echo '<script>window.location.href ="'.$baseUrl.'login"</script>';
        }else{
        
        $data['idUsuario']= $this->nativesession->get('std_idUsuario');
        $data['idPerfil']= $this->nativesession->get('std_idPerfil');
        $data['usuario']= $this->nativesession->get('std_usuario');
        $data['nombresUsuario']= $this->nativesession->get('std_nombresUsuario');
        $data['apellidosUsuario']= $this->nativesession->get('std_apellidosUsuario');
        $data['nombreCompleto']= $this->nativesession->get('std_nombreCompleto');
        $data['email']= $this->nativesession->get('std_email');
        $data['genero']= $this->nativesession->get('std_genero');
        $data['perfil']= $this->nativesession->get('std_perfil');
        $data['fechaNacimiento']= $this->nativesession->get('std_fechaNacimiento');

        } 
       
        return $data;
    }
    
    
    
    public function layoutConstructor($params,$session=''){

    $this->load->model('usermodel');
    $response=$this->usermodel->getMasterCliente($params);  
    
    $layoutVariables['logo']= $response[0]['Logotipo'];
    $layoutVariables['colorGeneral']= $response[0]['Color'];
    $layoutVariables['footer']= $response[0]['footer'];
    $layoutVariables['ico']= $response[0]['URL'];
    $layoutVariables['title']= $response[0]['Cliente'];
    

    $layoutVariables['params']= $params;
    if($session){
    $layoutVariables['username']=$session['username'];
    $layoutVariables['nivel']= $session['nivel'];    
    $layoutVariables['color']= $session['color'];    
    $layoutVariables['nombrePerfil']= $session['nombrePerfil'];    
    $layoutVariables['idCliente']= $session['idCliente'];    
    }
    return $layoutVariables;
    }    
    
    public function setStored($data, $conexion, $storedProcedureName)
    {
        $cadena="EXEC ".$storedProcedureName." ";
        
        foreach($data as $key => $value){
        $cadena.="'".$value."',";
        }
        $cadena=trim($cadena,",");
        $cadena.=" ";

//        print_r($cadena);
//        die();
        try{
        $var = new GetInfo($this->getServiceLocator()->get('Zend\Db\Adapter'));
        $response = $var->normalQuery($conexion, $cadena);
        
        }catch(Zend_Exception $e){
         echo "Caught exception: " . get_class($e) . "\n";
         echo "Message: " . $e->getMessage() . "\n";
        }
       
       return $response;

    }
     public function setUpdate($data, $where = "", $table) {
        $cadena = "UPDATE " . $table . " SET ";
        foreach ($data as $key => $value) {
            $cadena.="" . $key . "='" . $value . "',";
        }
        $cadena = trim($cadena, ",");
        if (!empty($where)) {
            $cadena.=' ' . $where;
        }
        //print_r($cadena);
        //die();
        try {
            $this->load->model('usermodel');
            $resp=$this->usermodel->sqlQuery($cadena);  
            $response =true;
        } catch (Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
            $response =false;
        }
        return $response;
    }

    public function setInsert($data,$table)
    {
            $cadena="INSERT INTO ".$table." (";
                    foreach($data as $key => $value){
            $cadena.="".$key.",";
            }
            $cadena=trim($cadena,",");
            $cadena.=") VALUES (";

            foreach($data as $key => $value){
            $cadena.="'".$value."',";
            }
            $cadena=trim($cadena,",");
            $cadena.=")";

            //echo $cadena;
            try {
            $this->load->model('usermodel');
            $resp=$this->usermodel->sqlQuery($cadena);  
            $response =true;
            } catch (Zend_Exception $e) {
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
            $response =false;
        }

        return $response;
    }  
    
    public function createField($tipo,$nombreCampo,$tag='',$icono='',$value='',$required='',$placeholder='',$disabled='', $SQL='',$conexion='',$idvalue='',$descvalue='',$tipoSelect=''){
        
        $campo='<div id="'.$nombreCampo.'form" class="form-group">
                <div class="textbox">
                <label for="'.$nombreCampo.'" id="'.$nombreCampo.'label" class="col-xs-12 col-md-12 control-label" style="text-align: left; margin-top: 6px;color:#234263">'.$tag.'</label>
                <div class="col-xs-11 col-md-11">
                <div style="margin-bottom: 10px" class="input-group">
                <span class="input-group-addon" style="color:#497a99"><i class="'.$icono.'"></i></span>';
                    
if($tipo==1){
    $campo.='<input style="color:#444;font-weight:bold;height:40px;font-size:15px;text-transform:uppercase" type="text" class="form-control '.$required.'" '.$disabled.' name="'.$nombreCampo.'" id="'.$nombreCampo.'" value="'.$value.'" placeholder="'.$placeholder.'">';                                 
}else if($tipo==2){
    $campo.=$this->createCombo($tipoSelect,$conexion,$SQL,$idvalue,$descvalue,$nombreCampo,$disabled,$required,$value);
}else if($tipo==3){
    $campo.='<textarea class="form-control" name="'.$nombreCampo.'" id="'.$nombreCampo.'" rows="4" cols="50" '.$required.' '.$disabled.' placeholder="'.$placeholder.'">'.$value.'</textarea>';                                 
}else if($tipo==4){
   $campo.='<input style="color:#444;font-weight:bold;height:40px;font-size:18px;text-transform:uppercase" type="password" class="form-control '.$required.'" '.$disabled.' name="'.$nombreCampo.'" id="'.$nombreCampo.'" value="'.$value.'" placeholder="'.$placeholder.'">';  
}else if($tipo==5){
     $campo='<div id="'.$nombreCampo.'form" class="form-group">
                <div class="textbox">
                <label for="'.$nombreCampo.'" id="'.$nombreCampo.'label" class="col-xs-12 col-md-12 control-label" style="text-align: left; margin-top: 6px;color:#234263">'.$tag.'</label>
                <div class="col-xs-11 col-md-12 text-justify">
                <div style="margin-bottom: 15px;margin-top: 5px;" >';
    $campo.=$this->createRadio($tipoSelect,$conexion,$SQL,$idvalue,$descvalue,$nombreCampo,$disabled,$required,$value);
}else if($tipo==6){
     $campo='<div id="'.$nombreCampo.'form" class="form-group">
                <div class="textbox">
                <label for="'.$nombreCampo.'" id="'.$nombreCampo.'label" class="col-xs-12 col-md-4 control-label" style="text-align: left; margin-top: 6px;">'.$tag.'</label>
                <div class="col-xs-11 col-md-11 text-justify">
                <div style="margin-bottom: 15px;margin-top: 5px;" >';
                $campo.=$this->createCheck($tipoSelect,$conexion,$SQL,$idvalue,$descvalue,$nombreCampo,$disabled,$required,$value);
}
                              
$campo.='</div>
                </div>
                </div>
            </div>';
        return $campo;
    }
    public function createFieldW($tipo,$nombreCampo,$tag='',$icono='',$value='',$required='',$placeholder='',$disabled='', $SQL='',$conexion='',$idvalue='',$descvalue='',$tipoSelect=''){
        
        $campo='<div id="'.$nombreCampo.'form" class="form-group">
                <div class="textbox">
                <label for="'.$nombreCampo.'" id="'.$nombreCampo.'label" class="col-xs-12 col-md-3 control-label" style="text-align: left; margin-top: 6px;">'.$tag.'</label>
                <div class="col-xs-11 col-md-8">
                <div class="input-group">
                ';
                    
if($tipo==1){
    $campo.='<input type="text" class="form-control '.$required.'" '.$disabled.' name="'.$nombreCampo.'" id="'.$nombreCampo.'" value="'.$value.'" placeholder="'.$placeholder.'">';                                 
}else if($tipo==2){
    $campo.=$this->createCombo($tipoSelect,$conexion,$SQL,$idvalue,$descvalue,$nombreCampo,$disabled,$required,$value);
}else if($tipo==3){
    $campo.='<textarea class="form-control" name="'.$nombreCampo.'" id="'.$nombreCampo.'" rows="4" cols="50" '.$required.' '.$disabled.' placeholder="'.$placeholder.'">'.$value.'</textarea>';                                 
}else if($tipo==4){
   $campo.='<input type="password" class="form-control '.$required.'" '.$disabled.' name="'.$nombreCampo.'" id="'.$nombreCampo.'" value="'.$value.'" placeholder="'.$placeholder.'">';  
}
                              
$campo.='</div>
                </div>
                <div class="col-xs-1 col-md-1" style="padding-left:0; padding-top:5px;">
                    <div id="'.$nombreCampo.'valid"></div>
                </div>
                </div>
            </div>';
        return $campo;
    }
    
    public function createCombo($tipo,$conexion='',$SQL='',$optionfield='',$descfield='',$nombreCampo,$disabled='', $required='', $value=''){
    $campo='';
    if($tipo==1){
    
    $this->load->model('usermodel');
    $response=$this->usermodel->normalQuery($SQL);
    //print_r($response);
    $campo.='<select style="color:#444;font-weight:bold;height:40px;font-size:15px;" class="form-control '.$required.'" name="'.$nombreCampo.'" id="'.$nombreCampo.'" '.$disabled.'>'
                         . '<option value="0">Elija...</option>';
    $selected='';
    if($response){
    foreach($response as $r){
        if($value!=''){
            if($r[$optionfield]==$value){
                $selected='selected';
            }else{
                $selected='';
            }
        }
        $campo.='<option value="'.$r[$optionfield].'" '.$selected.'>'.$r[$descfield].'</option>';
    }
    }else{
        $campo.='<option value="Otro">Otro</option>';
    }
    
    $campo.='</select>';
    }else if($tipo==2){
    $selected1='';
    $selected2='';
    if($value==1){
        $selected1='selected';
    }
    if($value==2){
        $selected2='selected';
    }
    $campo='<select class="form-control '.$required.'" name="'.$nombreCampo.'" id="'.$nombreCampo.'" '.$disabled.'>'
                         . '<option value="0">Elija...</option><option value="1" '.$selected1.'>Si</option><option value="2" '.$selected2.'>No</option></select>';
    }
        return $campo;
    }
    
    public function createRadio($tipo,$conexion='',$SQL='',$optionfield='',$descfield='',$nombreCampo,$disabled='', $required='', $value=''){
    $campo=''; 
    $campo.='<div class="row">';
    if($tipo==1){
    $this->load->model('usermodel');
    $response=$this->usermodel->normalQuery($SQL);
     
   
    ///$campo.='<input class="form-control radio-inline" type="radio" '.$required.'" name="'.$nombreCampo.'" id="'.$nombreCampo.'" '.$disabled.'><option value="0">Elija...</option>';
    $selected='';
    if($response){
    foreach($response as $r){
    $checked='';
    if($value==utf8_encode($r[$optionfield])){
        $checked='checked';
        //echo utf8_encode($r[$optionfield]);
    }else{
         $checked='';
    }    
    $campo.='<div class="col-xs-4" style="font-size:20px;">';
    $campo.='<label class="radio-inline"><input type="radio" name="'.$nombreCampo.'" '.$required.' '.$disabled.' value="'.utf8_encode($r[$optionfield]).'" '.$checked.'>'.utf8_encode($r[$descfield]).'</label>';
    $campo.='</div>';
    }
    }else{
        $campo.='No hay datos de opciones';
    }
    }
    
    if($tipo==2){

     
   
    $selected='';
    $campo.='<div class="col-xs-6">';
    $campo.='<label class="radio-inline"><input type="radio" name="'.$nombreCampo.'" '.$required.' '.$disabled.' value="1">Si</label>';
    $campo.='</div>';
    $campo.='<div class="col-xs-6">';
    $campo.='<label class="radio-inline"><input type="radio" name="'.$nombreCampo.'" '.$required.' '.$disabled.' value="0">No</label>';
    $campo.='</div>';
    }
 

    
     $campo.='</div>';
        return $campo;
    }
    
    public function createCheck($tipo,$conexion='',$SQL='',$optionfield='',$descfield='',$nombreCampo,$disabled='', $required='', $value=''){
    $campo='';
    if($tipo==1){
    $this->load->model('usermodel');
    $response=$this->usermodel->normalQuery($SQL);
     
    $campo='<div class="row">';
    $selected='';
    if($response){
    foreach($response as $r){
    $campo.='<div class="col-xs-6">';
    $campo.='<label class="checkbox-inline"><input type="checkbox" name="'.$nombreCampo.'[]" '.$required.' '.$disabled.' value="'.utf8_encode($r[$optionfield]).'">'.utf8_encode($r[$descfield]).'</label>';
    $campo.='</div>';
    }
    }else{
        $campo.='No hay datos de opciones';
    }
    }
     $campo.='</div>';
     
     return $campo;
    }
    
    
    function calculaEdad($fecha){
                list($Y,$m,$d) = explode("-",$fecha);
                return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
    }
    
    
    function randomText($length) {
    $pattern = "123456789ABCDEFGHJKLMNPQRSTUVWXYZ"; 
    $key='';
    for($i = 0; $i < $length; $i++) { 
        $key .= $pattern{rand(0, 32)}; 
    } 
    return $key; 
    }  
    
    function traducefecha($fecha){ 
   	$fecha= strtotime($fecha); // convierte la fecha de formato mm/dd/yyyy a marca de tiempo 
   	$diasemana=date("w", $fecha);// optiene el número del dia de la semana. El 0 es domingo 
      	switch ($diasemana) 
      	{ 
      	case "0": 
         	$diasemana="Domingo"; 
         	break; 
      	case "1": 
         	$diasemana="Lunes"; 
         	break; 
      	case "2": 
         	$diasemana="Martes"; 
         	break; 
      	case "3": 
         	$diasemana="Miércoles"; 
         	break; 
      	case "4": 
         	$diasemana="Jueves"; 
         	break; 
      	case "5": 
         	$diasemana="Viernes"; 
         	break; 
      	case "6": 
         	$diasemana="Sábado"; 
         	break; 
      	} 
   	$dia=date("d",$fecha); // día del mes en número 
   	$mes=date("m",$fecha); // número del mes de 01 a 12 
      	switch($mes) 
      	{ 
      	case "01": 
         	$mes="Enero"; 
         	break; 
      	case "02": 
         	$mes="Febrero"; 
         	break; 
      	case "03": 
         	$mes="Marzo"; 
         	break; 
      	case "04": 
         	$mes="Abril"; 
         	break; 
      	case "05": 
         	$mes="Mayo"; 
         	break; 
      	case "06": 
         	$mes="Junio"; 
         	break; 
      	case "07": 
         	$mes="Julio"; 
         	break; 
      	case "08": 
         	$mes="Agosto"; 
         	break; 
      	case "09": 
         	$mes="Septiembre"; 
         	break; 
      	case "10": 
         	$mes="Octubre"; 
         	break; 
      	case "11": 
         	$mes="Noviembre"; 
         	break; 
      	case "12": 
         	$mes="Diciembre"; 
         	break; 
      	} 
        //print_r($fecha);
   	$ano=date("Y",$fecha); 
        $hora=date("g",$fecha);
        $min=date("i",$fecha);
        $pm=date("a",$fecha);
   	$fecha= $diasemana.", ".$dia." de ".$mes." del ".$ano." a las ".$hora.":".$min." ".$pm; // unimos el resultado en una unica cadena 
   	return $fecha; //enviamos la fecha al programa 
   	}
        
        public function setMethod($wsdl, $method, array $data){ 
        $client = new Client($wsdl, array('compression' => SOAP_COMPRESSION_ACCEPT,"soap_version" => SOAP_1_1));  
        //print_r( $client -> $method($data));
        //die();
        return  $client -> $method($data);
        }
        public function setMethodCI($wsdl, $method, array $data){ 
        set_time_limit(0);
            try{
        $clienteSOAP = new SoapClient($wsdl);
        $resultado =  $clienteSOAP->__soapCall($method, $data);
        } catch(SoapFault $e){
        $resultado ="Exception ".$e;
        } 
        return $resultado;
        }
    
        public function setMethodEmail($wsdl, $method, array $data){ 
            $client = new Client($wsdl, array('compression' => SOAP_COMPRESSION_ACCEPT,"soap_version" => SOAP_1_1)); 

            return  $client -> call($method, $data);
        }
        
        
        
        function traducefechashort($fecha){ 
   	$fecha= strtotime($fecha); // convierte la fecha de formato mm/dd/yyyy a marca de tiempo 
   	
   	$dia=date("d",$fecha); // día del mes en número 
   	$mes=date("m",$fecha); // número del mes de 01 a 12
   	$ano=date("Y",$fecha); 
        
   	$fecha= $dia."/".$mes."/".$ano; // unimos el resultado en una unica cadena 
   	return $fecha; //enviamos la fecha al programa 
   	}
        
    public function useMailing($systemName,$clientID,$email = '', $cellphone = '',$SMStext,$variables='',$template) {
    $conexion=1;
    //require_once('fpdf/procesaDatosDB.php');

    $token = hash("md2", (string) microtime());

    if ($clientID == 0) {
        $clientID = "Does not apply";
    }
    
    $responseMail = 'Email no fue especificado';
    $responseSMS = 'Celular no fue especificado';
    $this->load->model('usermodel');
    $request=$this->usermodel->getMailingData(2);
    //////Variables declared in all cases////
    $dataArrayMail['systemName'] = $systemName;
    $dataArrayMail['clientID'] = $clientID;
    $dataArrayMail['token'] = $token;
    /////////////////////////////////////////
    if($request){
    if ($email) {
        $dataArrayMail['jsonVariables'] = $variables;
        
        foreach ($request as $r) {
            $from = $r['fromAccount'];
            $fromname = $r['nameAccount'];
            $replyTo = $r['replyTo'];
            $subject = $r['subject'];
            $serverSMTP = $r['serverSMTP'];
            $userSMTP = $r['userSMTP'];
            $passSMTP = $r['passSMTP'];
            $portSMTP = $r['portSMTP'];
            if($template){
                $templateatt=$template;
                if (strpos($template, '/') !== false) {
                    $templatesub = explode("/", $template);
                    $template = $templatesub[0];
                }
                
            }else{
                $templateatt = $r['template'];
                $template = $r['template'];
            }
        }
        
        if($variables){
            $variables= json_decode($variables,true);
        }
        
        $files='';
        //ATTACHMENTS
        $dir    = windowsPath.'/assets/attachments/'.$templateatt;
        if (file_exists($dir)) {
        $files = scandir($dir);
        }
        //print_r($files);
        $body='';
        
        if ($template) {
        include windowsPath."/assets/bodyMail/".$template.".php";
        }
         

        $dataArrayMail['template'] = $templateatt;
        $dataArrayMail['fromAccount'] = $from;
        $dataArrayMail['nameAccount'] = $fromname;
        $dataArrayMail['email'] = $email;
        $dataArrayMail['replyTo'] = $replyTo;
        $dataArrayMail['documento'] = '';
        $dataArrayMail['subject'] = $subject;
        $dataArrayMail['body'] = $body;
        $dataArrayMail['serverSMTP'] = $serverSMTP;
        $dataArrayMail['userSMTP'] = $userSMTP;
        $dataArrayMail['passSMTP'] = $passSMTP;
        $dataArrayMail['portSMTP'] = $portSMTP;
        if($files){
        $dataArrayMail['attachments'] = $files;
        }else{
        $dataArrayMail['attachments'] ='';
        }
       
        $responseMail = $this->sendMail($dataArrayMail);
        
    }

//    if ($cellphone) {
//        foreach ($request as $r) {
//            $smsProviderID = $r['smsProviderID'];
//        }
//        $dataArrayMail['smsProviderID'] = $smsProviderID;
//        $dataArrayMail['cellphone'] = $cellphone;
//        $dataArrayMail['SMStext'] = $SMStext;
//        $responseSMS = $this->sendSMS($dataArrayMail);
//    }
    
    }
    $json = "No response body";
    $json = '{"responseEmail":"' . utf8_encode($responseMail) . '","responseSMS":"' . $responseSMS . '","token":"' . utf8_encode($token) . '"}';
    
    return $json;
    }

    public function alertDanger($text){
        $html='<div class="row" style="padding:10px"><div class="col-xs-12"><div class="alert alert-danger text-center"><b>'.$text.'</b></div></div></div>';
        return $html;
    }
    
    public function sendMail($data) {
        include windowsPath."/assets/PHPMailer/class.phpmailer.php";
        include windowsPath."/assets/PHPMailer/class.smtp.php";
        include windowsPath."/assets/PHPMailer/class.pop3.php";
        //print_r($data);
        //die();
        $data['email'] = str_replace(' ', '', $data['email']);
        $mail = new \PHPMailer;
        //$mail -> isSMTP();
        $mail -> Host = 'relay-hosting.secureserver.net';
        //$mail -> Host = $data['serverSMTP'];
        //$mail -> SMTPAuth = false;
        //$mail -> Username = $data['userSMTP'];
        //$mail -> Password = $data['passSMTP'];
        //$mail -> Port = $data['portSMTP'];
        $mail -> From = $data['fromAccount'];
        //$mail -> Sender = $data['fromAccount'];
        $mail -> FromName = $data['nameAccount'];
        $mail -> addAddress($data['email']);
        $mail -> addReplyTo($data['replyTo'], 'No Reply');
        //$mail -> WordWrap = 50;
    
        //Attachments
        if($data['attachments']){
        foreach($data['attachments'] as $key=>$val){
            if(strlen($val)<4){
                unset($data['attachments'][$key]);
            }
        }
        foreach($data['attachments'] as $key=>$val){
        //echo 'fpdf/attachments/template'.$data['campaignID'].'/'.$data['attachments'][$key];
        $mail -> addAttachment(windowsPath.'/assets/attachments/'.$data['template'].'/'.$data['attachments'][$key]);
        }
        }
        $mail -> isHTML(true);
        $mail -> Subject = $data['subject'];
        $mail -> Body = $data['body'];
        $mail -> AltBody = $data['body'];

        if (!$mail -> send()) {
                $msj = 'Error al Enviar el Email: ';
                $msj .= '' . $mail -> ErrorInfo;
        } else {
                $msj = 'Email Enviado Exitosamente';
        }    

        return $msj;
	}
}