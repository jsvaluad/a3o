<?php
/*Variables Menu*/
$lang['home'] = 'HOME';
$lang['solutions'] = 'SOLUTIONS';
$lang['staff'] = 'STAFF ADMINISTRATION';
$lang['tax'] = 'TAX & ACCOUNTING';
$lang['global'] = 'GLOBAL MOBILITY';
$lang['headhunting'] = 'HEADHUNTING';
$lang['talent'] = 'TALENT ENGINEERING';
$lang['about'] = 'WE ARE';
$lang['contact'] = 'CONTACT';
$lang['blog'] = 'BLOG';
$lang['candidates'] = 'JOB SEARCH ENGINE';
$lang['employees'] = 'EMPLOYEES';
/*Inicia traduccion Ingles Home*/
$lang['titleBanner']='Your partner creating value in your business<br><br><a href="'. base_url('Index/Nosotros').'" class="bannerLink">Learn more</a>';
$lang['homesecgray']='We support the improvement of your <br>Human Resources and Finance areas through Consulting and Back Office solutions.';
$lang['titlegrayHome']=', Your strategic partner';
$lang['readmore'] ='Read More';
$lang['title1back']='Staff<br>Administration';
$lang['subtitle1back']='We manage your staff with a dedicated and specialized team and online based support tools.';
$lang['title2back']='Tax <br>& Acconunting';
$lang['subtitle2back']='We make sure that your company complies with local and international Tax requirements.';
$lang['title3back']='Global<br>Mobility';
$lang['subtitle3back']='We support your foreign staff in the management of their payroll complexity.';
$lang['title1consul']='Headhunting';
$lang['subtitle1consul']='We indentify, select and recruit the talent your company needs.';
$lang['title2consul']='Talent <br> Engineering';
$lang['subtitle2consul']='We are passionate about developing talent and improving organizational processes.';
$lang['estructur']='A<br> Structured<br> Methodology';
$lang['ti1']='Advisory';
$lang['sti1']='We understand your needs and<br> propose advanced solutions.';
$lang['ti2']='Administration';
$lang['sti2']='We manage Consulting and Back Office<br>activities with quality and efficiency.';
$lang['ti3']='Attention';
$lang['sti3']='We support you every step of the way.';
/*Termina traduccion Ingles Home*/
/*Incia traduccion Ingles Banners*/
$lang['bannerstaf']= '<span class="grande">Staff Administration</span> <br><br><span class="naranja">Simplifying your <br>payroll and personnel <br>administration processes.</span> ';
$lang['bannertax']= '<span class="grande">Tax & Accounting</span> <br><br><span class="naranja">Making financial decisions simpler</span><br><span class="naranja">With your strategic partner</span>';
$lang['bannermobility']= '<span class="grande">Global Mobility</span> <br><br><span class="naranja">Facilitating the entry and administration</span><br><span class="naranja">of your foreign staff</span> ';
$lang['bannerhead']= '<span class="grande">Headhunting</span> <br><br><span class="blanco">Identifying, selecting and recruiting</span><br><span class="cian small">the talent your company needs.</span> ';
$lang['bannertalent']= '<span class="grande azul">Talent Engineering</span> <br><br><span class="cian">Developing talent</span><br><span class="cian">and improving <br>organizational processes</span> ';
/*Termina traduccion Ingles Banners*/
/*Inicia traduccion Ingles Descripciones*/
$lang['defineTitle1']='Staff Administration';
$lang['defineSubtite1']='We manage your staff with a dedicated and specialized team and online based support tools.';
$lang['definemotto1']='We create VALUE TOGETHER';
$lang['listadomotto1']= '<ul class="listados">
                        <li>Payroll calculations.</li>
                        <li>Administration of social prevision programs.</li>
                        <li>Compliance of Social Security Obligations.</li>
                        <li>Tax Returns and withholdings.</li>
                        <li>Preparation of accounting policies.</li>
                        <li>HR Reports.</li>
                        <li>Advisory on compliance in terms of the Ministry of Labor and Social Prevision (STPS)</li>
                        <li>Specialized outsourcing staff</li>
                      </ul>';
$lang['defineTitle2']='Tax & Accounting';
$lang['defineSubtite2']='Our financial experts make sure that your company complies with local and international Tax requirements.';
$lang['definemotto2']='We create VALUE TOGETHER';
$lang['listadomotto2']='<ul class="listados">
                          <li>Accounting schema structure.</li>
                          <li>Elaboration and analysis of accounting information.</li>
                          <li>Tax advice and structuring.</li>
                          <li>Presentation of tax declarations.</li>
                          <li>Development of financial KPI´S.</li>
                        </ul>';
$lang['defineTitle3']='Global Mobility';
$lang['defineSubtite3']='We assist your foreign staff in each stage of the immigration process and international payroll.';
$lang['definemotto3']='We create VALUE TOGETHER';
$lang['listadomotto3']='<ul class="listados">
                          <li>Online control for the management of formalities and documentation.</li>
                          <li>International migratory and fiscal procedures.</li>
                          <li>Annual international fiscal declaration.</li>
                          <li>International mobility.</li>
                        </ul>';

$lang['defineTitle4']='Headhunting';
$lang['defineSubtite4']='Our experts specialize in different areas and identify the right candidates through a structured process.';
$lang['definemotto4']='We create VALUE TOGETHER';
$lang['listadomotto4']='<ul class="listados2">
                          <li>Defining the profile of the ideal candidate.</li>
                          <li>Hunting the candidate through our database, networking and industry expertise.</li>
                          <li>Professional filtering and interviews to evaluate the candidates.</li>
                          <li>Constant progress reports and candidate comparison score card.</li>
                          <li>Integration follow up with an in-placement program.</li>
                        </ul>';
$lang['defineTitle5']='Talent Engineering';
$lang['defineSubtite5']='We are passionate about developing personnel talent and improving organizational processes. We integrate measurable and structured solutions for the employees and the organization.';
$lang['definemotto5']='We create VALUE TOGETHER';
$lang['listadomotto5']= '<ul class="listados2">
                            <li>Human Capital performance improvement.</li>
                            <li>Optimization of internal processes.</li>
                            <li>Creation of a favorable organizational culture.</li>
                            <li>Change Management.</li>
                            <li>Goals alignment with the company´s strategic vision.</li>
                          </ul>';
$lang['titleshare']='Do you know anyone who might be interested?';
$lang['subtitleshare']='SHARE THIS SERVICE';
$lang['titleDescrip1']='Supporting your Employment Obligations';
$lang['titlespec1']='Based on a multidimensional model.';
$lang['subtitleDescrip1']='We create a customized solution for the management of your human capital.';
$lang['listadoStaff2']='<br><ul class="listadoStaff gray">
                        <li>1. Expert team in paysheet matter: IMSS, LFT, SAT (Social Security, work laws, taxes).</li>
                        <li>2. Process methodology and service level agreements.</li>
                        <li>3. Extensive service for Key Accounts, full Back Office assistance and/or Inplants support. </li>
                        <li>4. Constant progress report and update from our Normative area. </li>
                        <li>5. Efficient and controlled workflows through our online platform (NEBVLA). </li>
                        <li>6. Cutting edge human capital consultation and reporting online tools. </li>
                      </ul>';
$lang['titleDescrip2']='Making your<br>financial desicions<br> easier.';
$lang['titlespe2']='With assertive information and web solutions for online monitoring.';
$lang['subtitleDescrip2']='We support financial performance with the principle: &quot;measure to improve&quot;.';
$lang['titleDescrip3']='Assistance every<br>step of the way';
$lang['subtitleDescrip3']='We support your foreign staff through out the whole migratory process: from the first contact, understanding requirements and compiling the paperwork to achieve a successful outcome.';
$lang['titleDescrip4']='<strong>Understanding<br>your expectations</strong><br>';
$lang['titleespe4']='<small class="especial azul"> To improve your recruiting process</small>';
$lang['subtitleDescrip4']='We support you defining the ideal candidate´s profile with a structured model that applies three key factors:';
$lang['titleDescrip5']='<strong>ENDOGENOUS CHANGE</strong><br>Integrated solution';
$lang['subtitleDescrip5']='In order to boost the individual and organizational improvement processes into a context of change.';
/*Termina traduccion Ingles como te apoyamos*/
/*Inicia traduccion Ingles Como te apoyamos*/
$lang['titlesupport']='HOW DO WE SUPPORT YOU?';
$lang['name1support']='STAFF ADMINISTRATION';
$lang['name2support']='TAX & ACCOUNTING';
$lang['name3support']='GLOBAL MOBILITY';
$lang['name4support']='HEADHUNTING';
$lang['name5support']='TALENT ENGINEERING';
$lang['subtitle5support']='We are passionate about developing personnel talent and improving organizational processes.<br>We integrate measurable and structured solutions for the employees and the organization.';

$lang['titlepointTalent1']='Talent<br>Performance';
$lang['textpointTalent1']='We promote the performance of your staff, aligning it with the company´s culture and organizational strategy.';
$lang['listpointTalent1']='• Core Competencies Training Program<br><br>• Team Building <br><br>• Coaching';
$lang['titlepointTalent2']='Process<br>Engineering';
$lang['textpointTalent2']='We support your organization through continuous improvement processes for the competitive and organizational maturity.';
$lang['listpointTalent2']='• Lean Six Sigma / Kaizen<br><br>• Process Mapping';
$lang['titlepointTalent3']='Change<br>Management';
$lang['textpointTalent3']='We create favorable conditions to control transitions, generating an endogenous vision of change.';
$lang['listpointTalent3']='• Change Management Projects<br><br>• Organizational Improvement';

$lang['titlepointStaff1']='Payroll<br>Administration';
$lang['textpointStaff1']='We process your company´s payroll with accuracy and security within the regulatory framework.';
$lang['titlepointStaff2']='Staff<br>Administration';
$lang['textpointStaff2']='We manage the human capital you need to fulfill your employer obligations.';
$lang['titlepointStaff3']='Updates';
$lang['textpointStaff3']='Our legal experts keep you up to date with the latest Mexican labor laws.';

$lang['titlepointTax1']='Accounting';
$lang['textpointTax1']='We elaborate and analyze your accounting information allowing you to understand the current financial state of your company.';
$lang['titlepointTax2']='Taxes';
$lang['textpointTax2']='We advise on fiscal matters, calculate and present your declaration for the timely fulfillment of your tax obligations.';
$lang['titlepointTax3']='Financial<br>Administration';
$lang['textpointTax3']='We analyze your financial processes through indicators and online tools to guide the evaluation of the performance of your company.';

$lang['titlepointGlobal1']='Migratory<br>processes';
$lang['textpointGlobal1']='We fully manage the immigration process of your foreign staff and their families to ensure their legal stay in Mexico.';
$lang['titlepointGlobal2']='Fiscal <br>Transactions';
$lang['textpointGlobal2']='We manage your fiscal documentation to ful fill your legal obligations';
$lang['titlepointGlobal3']='Special<br>Transactions';
$lang['textpointGlobal3']='We manage your special transactions and requests of official documentation.';

$lang['titlepointHead1']='Management';
$lang['textpointHead1']='From medium to senior leadership profiles.';
$lang['titlepointHead2']='Talent';
$lang['textpointHead2']='From entry level to middle management positions.';
/*Termina traduccion Ingles Como te Apoyamos*/



/*Inicia traduccion Como Lo Hacemos Soluciones*/
$lang['titledoit']='HOW DO WE DO IT?';
$lang['subtitledoit']='With a structured methodology';
$lang['advisoryndoit']='We support you in structuring a solution to facilitate your payroll and personnel administration strategy.';
$lang['administrationdoit']='We support you with efficient processes and accessible online tools for the improvement of your human resources area.';
$lang['attentiondoit']='We resolve your staffing needs with constant personalized assistance.';

$lang['advisoryndoitTax']='We guide you to correctly fulfill your financial obligations through a solution adapted to your needs.';
$lang['administrationdoitTax']='We control and analyze your accounting, fiscal and financial information with efficient processes, according to your requirements.';
$lang['attentiondoitTax']='We support financial interpretation, clarify your accounting queries, and keep you informed of fiscal updates.';

$lang['advisoryndoitGlobal']='We explain you the regulations of the immigration procedures you need.';
$lang['administrationdoitGlobal']='We manage your proceeding before the corresponding authorities and we inform you of the progress until is finished.';
$lang['attentiondoitGlobal']='We provide in advanced preparation of renewal requests and documentation updates.';

$lang['advisoryndoitHead']='We design a profile based on your requirements.';
$lang['administrationdoitHead']='We identify the best talent of your industry.';
$lang['attentiondoitHead']='We assist the selected candidate through an in-placement program for the correct integration into your company.';

$lang['advisoryndoitTalent']='We analyze and define the action plan your company needs to improve its organizational processes.';
$lang['administrationdoitTalent']='We set up the right solution focused on professional and personal improvement showing the progress and results.';
$lang['attentiondoitTalent']='We have a customer service culture based on quality and proximity.';
/*Termina traduccion Como Lo Hacemos Soluciones*/
/*Inicia Traduccion Ingles Contacto Soluciones*/
$lang['titlecontsolt'] = '  PAYROLL WITHOUT SETBACKS <br>AND STAFFING WITHOUT RISKS';
$lang['titlecontsolt2'] = 'CATCH UP ON YOUR FINANCES';
$lang['titlecontsolt3'] = 'RELEASE OF FORMALITIES';
$lang['titlecontsolt4'] = 'ATTRACT THE BEST TALENT FOR YOUR COMPANY';
$lang['titlecontsolt5'] = 'DO NOT RESIST CHANGE';
$lang['subtitlecontsolt'] = 'Contact us!';
$lang['namecontsolt'] = 'Name';
$lang['lastnamecontsolt'] = 'Last Name';
$lang['emailcontsolt'] = 'E-mail';
$lang['phonecontsolt'] = 'Phone';
$lang['questioncontsolt'] = 'How can and help you?';
$lang['messagecontsolt'] = 'Message';
$lang['termscontsolt'] = 'I agree to the <a href="" class="terminos">terms and conditions</a>';
$lang['sendcontsolt'] = 'SEND';
/*Termna traduccion Ingles Contacto Solutions*/
/*Inicia traduccion Ingles Nosotros*/
$lang['tbnosotros'] ='Our experts analyze <br>and understand your business<br><span class="grisClaro">"CULTURE A<small>3</small>O"</span>';
$lang['culture'] ='<img src='.base_url('assets/images/logo.svg').' alt="" class="imgTitles"> &nbsp;METHODOLOGY';
$lang['advisoryn'] ='Advisory';
$lang['textadvn'] ='We understand your needs and propose solutions for your company.';
$lang['administration'] ='Administration';
$lang['textadmn']='We manage Consulting and Back Office activities with a high level of quality and efficiency.';
$lang['attention'] ='Attention';
$lang['textattn']='We support you every step of the way.';
$lang['icon1'] ='Staff Administration';
$lang['icon2'] ='Tax & Accounting';
$lang['icon3'] ='Global Mobility';
$lang['icon4'] ='Headhunting';
$lang['icon5'] ='Talent Enginnering';
$lang['advantage']= '<img src="'.base_url('assets/images/logo.svg').'" alt="" class="imgTitles"> &nbsp;ADVANTAGES';
$lang['subtitleadvantage'] = 'As your strategic partner';
$lang['vtitle1']='Structured Methodology';
$lang['vsubtitle1']=' We ensure that our services follow a continuous improvement process.';
$lang['vtitle2']='Complete solution and Consulting';
$lang['vsubtitle2']='We integrate the main activities of Human Resources, Finance and Consulting.';
$lang['vtitle3']='Experts support';
$lang['vsubtitle3']='Our team is composed of experts in different fields and sectors.';
$lang['vtitle4']='Flexibility ';
$lang['vsubtitle4']='We structure solutions for small, medium and multinational entreprises.';
$lang['mission']='Our Mission<br><strong>TO BE YOUR STRATEGIC PARTNER</strong>';
/*Terina traduccion Ingles Nosotros*/
/*Inicia traduccion Ingles Imagenes*/
$lang['cultureimg'] = 'culturaa3oIngles.svg';
$lang['staffstructureimg'] ='staffstructureIngles.svg';
$lang['taxstructureimg'] ='taxstructureIngles.svg';
$lang['headstructureimg'] = 'headstructureIngles.svg';
/*Termina Traducccion Ingles Imagenes*/
/*Inicia traduccion Ingles Contacto*/
$lang['title'] = 'CONTACT US';
$lang['subtitle'] = 'Create value for your company!';
$lang['paraphrase'] = 'If you are interested or have any doubts about our solutions, write to us !<br> One of our experts will be in contact with you shortly. ';
$lang['name'] = 'Name';
$lang['lastname'] = 'LastName';
$lang['email'] = 'E-mail';
$lang['phone'] = 'Phone';
$lang['question'] = 'How can we help you?';
$lang['message'] = 'Message';
$lang['terms'] = 'I agree to the terms and conditions';
$lang['send'] = 'SEND';
/*Termina traduccion Ingles Contacto*/
/*Inicia traducccion Ingles Footer*/
$lang['Followus'] = 'Follow Us';
$lang['solutionsF'] = 'SOLUTIONS';
$lang['privacyPolicy'] = 'Privacy Policy';
/*Termina traduccion Ingles Footer*/
?>
