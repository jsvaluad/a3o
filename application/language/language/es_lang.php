<?php
$lang['home'] = 'INICIO';
$lang['solutions'] = 'SOLUCIONES';
$lang['staff'] = 'STAFF ADMINISTRATION';
$lang['tax'] = 'TAX & ACCOUNTING';
$lang['global'] = 'GLOBAL MOBILITY';
$lang['headhunting'] = 'HEADHUNTING';
$lang['talent'] = 'TALENT ENGINEERING';
$lang['about'] = 'NOSOTROS';
$lang['contact'] = 'CONTACTO';
$lang['blog'] = 'BLOG';
$lang['candidates'] = 'PORTAL DE EMPLEO';
$lang['employees'] = 'EMPLEOS';
/*Inicia traduccion Español Home*/
$lang['titleBanner']='Somos el socio estratégico que<br> te apoya a crear valor para tu empresa<br><br><a href="'.base_url('Index/Nosotros').'" class="bannerLink">Conoce más</a>';
$lang['homesecgray']='Como expertos, te acompañamos para mejorar tus áreas de <br>Recursos Humanos y Finanzas con soluciones de Consulting y Back Office.';
$lang['titlegrayHome']='es tu socio estratégico';
$lang['readmore'] ='Leer más';
$lang['title1back']='Staff<br>Administration';
$lang['subtitle1back']='Administramos tus procesos de nómima y tercerizamos tu staff especializado.';
$lang['title2back']='Tax <br>& Accounting';
$lang['subtitle2back']='Nos encargamos de tu cumplimiento fiel ante el SAT y preparamos información contable/financiera de valor para tu toma de decisiones.';
$lang['title3back']='Global<br>Mobility';
$lang['subtitle3back']='Acompañamos la localización de tu equipo extranjero tanto en materia migratoria como en normativa de nóminas globales.';
$lang['title1consul']='Headhunting';
$lang['subtitle1consul']='Identificamos y seleccionamos al talento que tu empresa necesita.';
$lang['title2consul']='Talent <br> Engineering';
$lang['subtitle2consul']='Impulsamos el cambio en las organizaciones y su capital humano.';
$lang['estructur']='Una cultura<br>de trabajo <br>estructurada';
$lang['ti1']='Asesoría';
$lang['sti1']='Comprendemos tus necesidades para proponer <br>soluciones ideales para tu empresa.';
$lang['ti2']='Administración';
$lang['sti2']='Ponemos en marcha la solución mientras tú  <br>te enfocas en crear valor para tu empresa.';
$lang['ti3']='Atención';
$lang['sti3']='Te acompañamos de ahora y en adelante.';
/*Termina traduccion Español Home*/
$lang['bannerstaf']= '<span class="grande">Staff Administration</span> <br><br><span class="naranja">Administramos<br>tus procesos de nómina y<br>tercerizamos tu staff especializado</span> ';
$lang['bannertax']= '<span class="grande">Tax & Accounting</span> <br><br><span class="naranja">Con un socio estratégico</span><br><span class="naranja">tomar decisiones financieras<br> es más simple</span> ';
$lang['bannermobility']= '<span class="grande">Global Mobility</span> <br><br><span class="naranja">Facilita el ingreso y la administración</span><br><span class="naranja">de personal expatriado</span> ';
$lang['bannerhead']= '<span class="grande">Headhunting</span> <br><br><span class="cian">Identificamos y seleccionamos</span><br><span class="cian">al talento que tu empresa necesita</span> ';
$lang['bannertalent']= '<span class="grande azul">Talent Engineering</span> <br><br><span class="cian">Desempeño Individual</span><br><span class="cian">Desarrollo Organizacional</span> ';
$lang['defineTitle1']='Staff Administration';
$lang['defineSubtite1']='A través de nuestro servicio de Administración de Personal, apoyamos a nuestros clientes en el cumplimiento de sus obligaciones patronales y fomentamos precisión, seguridad y eficiencia en la ejecución de sus procesos administrativos.';
$lang['definemotto1']='Para apoyarte a CREAR VALOR';
$lang['listadomotto1']= '<ul class="listados"><li>Cálculos de Nómina.</li>
                        <li>Administración de programas de previsión social.</li>
                        <li>Cumplimiento de obligaciones ante el IMSS.</li>
                        <li>Declaraciones & Retenciones Fiscales.</li>
                        <li>Preparación de pólizas contables.</li>
                        <li>Layout dispersión de tesorería.</li>
                        <li>Reporting Recursos Humanos.</li>
                        <li>Asesoría en cumplimiento en materia STPS.</li>
                        <li>Tercerización de personal especializado.</li>
                      </ul>';
$lang['defineTitle2']='Tax & Accounting';
$lang['defineSubtite2']='Te acompañamos en la administración financiera de tu empresa con el respaldo de expertos que hacen oportuno el cumplimiento de tus obligaciones financieras.';
$lang['definemotto2']='Para apoyarte a CREAR VALOR';
$lang['listadomotto2']='<ul class="listados">
                          <li>Estructuración de Esquema Contable.</li>
                          <li>Elaboración y análisis de Información Contable.</li>
                          <li>Asesoría y Estructuración Fiscal.</li>
                          <li>Elaboración y Declaraciones Fiscales.</li>
                          <li>Elaboracion de KPI´s Financieros.</li>
                        </ul>';
$lang['defineTitle3']='Global Mobility';
$lang['defineSubtite3']='Asistimos a tu personal internacional en la coordinación de su proceso migratorio y administrando su  nómina internacional.';
$lang['definemotto3']='Para apoyarte a CREAR VALOR';
$lang['listadomotto3']='<ul class="listados">
                          <li>Control gracias a la accesibilidad a sus trámites y documentos en línea.</li>
                          <li>Cumplimiento de trámites y normativa nacional e internacional.</li>
                          <li>Declaración anual internacional.</li>
                          <li>Movilización internacional controlada.</li>
                        </ul>';

$lang['defineTitle4']='Headhunting';
$lang['defineSubtite4']='Con expertos especializados en áreas funcionales identificamos al candidato ideal a través de un proceso ágil y estructurado.';
$lang['definemotto4']='Para apoyarte a CREAR VALOR';
$lang['listadomotto4']='<ul class="listados2">
                          <li>Definimos juntos las características del candidato ideal.</li>
                          <li>Realizamos la búsqueda del candidato a través de nuestra red de contactos, networking y por conocimiento de la industria.</li>
                          <li>Evaluamos a nuestros candidatos con filtros y herramientas de selección.</li>
                          <li>Entregables para el cliente para conocer el avance del proceso.</li>
                          <li>Facilitamos la integración del candidato con un programa de acompañamiento.</li>
                        </ul>';
$lang['defineTitle5']='Talent Engineering';
$lang['defineSubtite5']='Nos apasiona desarrollar al talento y mejorar los procesos organizacionales. Por eso, integramos soluciones estructuradas y medibles de acompañamiento para el colaborador y la organización.';
$lang['definemotto5']='Para apoyarte a CREAR VALOR';
$lang['listadomotto5']= '<ul class="listados2">
                            <li>Mejora el desempeño del capital humano.</li>
                            <li>Optimiza los procesos internos.</li>
                            <li>Crea una cultura organizacional favorable.</li>
                            <li>Facilita la transición al cambio.</li>
                            <li>Alinea los objetivos con la visión estratégica de tu empresa</li>
                          </ul>';
$lang['titleshare']='¿Conoces a alguien a quien podría interesarle?';
$lang['subtitleshare']='Comparte este servicio';
$lang['titleDescrip1']='Respaldando tus obligaciones patronales';
$lang['titlespec1']='Con base en un modelo multidimensional y soluciones de automatización de vanguardia.';
$lang['subtitleDescrip1']='Nuestro modelo de servicio avanzado y de alto nivel, permite un flujo de trabajo eficiente y un control estricto de revisión <br>a fin de evitar riesgos y contingencias operativas.<br><br>Más que un simple servicio de maquila de nómina. ';
$lang['listadoStaff2']='<br><ul class="listadoStaff gray">
                        <li>1.  Equipo de expertos en materia de nómina: IMSS, LFT, SAT.</li>
                        <li>2. Cultura de Procesos y Acuerdos de Niveles de Servicio.</li>
                        <li>3.  Servicio amplio por parte de Key Accounts, Back Office completo y/o inplants. </li>
                        <li>4. Acompañamiento & Actualización por parte de nuestra área de Normativa. </li>
                        <li>5. Worklows eficiente y controlado, a través de la plataforma online. </li>
                        <li>6.   Ambiente de RH de vanguardia, gracias a consultas y  reportes online. </li>
                      </ul>';
$lang['titleDescrip2']='Facilitando tus<br>decisiones<br> financieras';
$lang['titlespe2']='Con información asertiva y soluciones web para <br>monitoreo en línea.';
$lang['subtitleDescrip2']='Apoyamos el desempeño financiero de tu empresa con el principio de &quot;Medir para Mejorar&quot;.';
$lang['titleDescrip3']='Respaldo<br>en cada momento';
$lang['subtitleDescrip3']='Control de la vigencia de tus procesos migratorios y apoyo en el cumplimiento con normativa nacional de tu staff expatriado.';
$lang['titleDescrip4']='<strong>Entendiendo<br>tus expectativas </strong><br>';
$lang['titleespe4']='<small class="especial azul"> Para facilitar la búsqueda con el candidato ideal.</small>';
$lang['subtitleDescrip4']='Te acompañamos en la definición del perfil que necesita tu empresa basándonos en el Modelo Matricial que aplica tres factores clave:';
$lang['titleDescrip5']='<strong>Cambio endógeno</strong><br>Solución integrada';
$lang['subtitleDescrip5']='Para impulsar los procesos internos de mejora, individuales y organizacionales, en un contexto de cambio.';

/*Variables Como te apoyamos*/

$lang['titlesupport']='¿CÓMO TE APOYAMOS?';
$lang['name1support']='STAFF ADMINISTRATION';
$lang['name2support']='TAX & ACCOUNTING';
$lang['name3support']='GLOBAL MOBILITY';
$lang['name4support']='HEADHUNTING';
$lang['name5support']='TALENT ENGINEERING';
$lang['subtitle5support']='El desarrollo del talento es la punta de lanza para el éxito de una empresa.<br>Conoce nuestros servicios y permítenos ser tu socio estratégico.';

$lang['titlepointTalent1']='Talent<br>Performance';
$lang['textpointTalent1']='Potenciamos el desempeño de tus colaboradores alineándolo a tu cultura y estrategia organizacional para una aplicación inmediata y duradera de los aprendizajes.';
$lang['listpointTalent1']='• Formación Integral por Competencias<br><br>• Capacitación <br><br>• Coaching Sistémico® Organizacional';
$lang['titlepointTalent2']='Process<br>Engineering';
$lang['textpointTalent2']='Apoyamos tu organización con procesos de mejora continua hacia la competitividad y madurez  organizacional.';
$lang['listpointTalent2']='• Lean Six Sigma / Kaizen<br><br>• Mapeo de Procesos';
$lang['titlepointTalent3']='Change<br>Management';
$lang['textpointTalent3']='Creamos las condiciones favorables para controlar eficientemente la  transición, generando una visión endógena del cambio.';
$lang['listpointTalent3']='• Administración Sistémica del Cambio<br><br>• Mejora Organizacional';
$lang['titlepointStaff1']='Administración<br>de Nómina';
$lang['textpointStaff1']='Procesamos la nómina de tu empresa con exactitud y seguridad para ti y tu personal dentro del marco normativo.';
$lang['titlepointStaff2']='Administración<br>Personal';
$lang['textpointStaff2']='Administramos tu staff especializado en estricto cumplimiento de la ley dentro del marco normativo.';
$lang['titlepointStaff3']='Actualización';
$lang['textpointStaff3']='A través de nuestra área de Normativa te actualizamos sobre cambios regulatorios que afectan tus procesos de administración de Personal.';
$lang['titlepointTax1']='Contabilidad';
$lang['textpointTax1']='Elaboramos y analizamos tu información contable que te permitirá entender el estado financiero actual de tu empresa.';
$lang['titlepointTax2']='Impuestos';
$lang['textpointTax2']='Asesoramos en materia fiscal, calculamos y presentamos tus impuestos para el cumplimiento oportuno de tus obligaciones  tributarias.';
$lang['titlepointTax3']='Administración<br>financiera';
$lang['textpointTax3']='Analizamos tu gestión financiera por medio de indicadores y herramientas en línea para que evalues el desempeño de tu  empresa.';
$lang['titlepointGlobal1']='Trámites<br>migratorios';
$lang['textpointGlobal1']='Gestionamos por completo el proceso migratorio de tu personal extranjero y sus familias para asegurar su estancia legal en México.';
$lang['titlepointGlobal2']='Trámites<br>fiscales';
$lang['textpointGlobal2']='Gestionamos los trámites fiscales para cumplir con sus obligaciones tributarias.';
$lang['titlepointGlobal3']='Trámites<br>especiales';
$lang['textpointGlobal3']='Gestiones especiales de trámites y solicitudes de documentación oficial.';

$lang['titlepointHead1']='Management';
$lang['textpointHead1']='Reclutamiento especializado para perfiles de gerencia media a alta dirección.';
$lang['titlepointHead2']='Talent';
$lang['textpointHead2']='Reclutamiento especializado para perfiles de entrada a gerencias medias.';




/*Variables Como lo hacemos Solutions*/
$lang['titledoit']='¿CÓMO LO HACEMOS?';
$lang['subtitledoit']='Con una cultura de trabajo estructurada';
$lang['advisoryndoit']='Te apoyamos a estructurar una solución para facilitar la administración de  personal y nómina para cumplir con tus necesidades estratégicas.';
$lang['administrationdoit']='Te acompañamos con procesos eficientes y herramientas accesibles y online para la mejora de tu área de recursos humanos.';
$lang['attentiondoit']='Te atendemos de manera continua y personalizada en tus solicitudes y las de tu personal.';

$lang['advisoryndoitTax']='Te orientamos para que cumplas correctamente tus obligaciones financieras a través de una solución adaptada a tus necesidades.';
$lang['administrationdoitTax']='Procesamos y analizamos tu información contable-fiscal y financiera con procesos eficientes, controlados y acorde a tus requerimientos.';
$lang['attentiondoitTax']='Te respaldamos en la interpretación financiera, aclaramos tus consultas contables, y te informamos de las actualizaciones fiscales';

$lang['advisoryndoitGlobal']='Un experto te orienta con base en la normatividad de los trámites migratorios que necesitas.';
$lang['administrationdoitGlobal']='Gestionamos tus trámites ante las autoridades correspondientes y te informamos del avance hasta su término.';
$lang['attentiondoitGlobal']='Brindamos seguimiento anticipado a solicitudes de renovaciones y actualizaciones de documentación.';

$lang['advisoryndoitHead']='Diseñamos un perfil con base en tus requerimentos y respaldo por expertos.';
$lang['administrationdoitHead']='Identificamos el talento preciso dentro de nuestra base de datos y/o red de contactos, acompañado con el conocimiento que tenemos de cada industria.';
$lang['attentiondoitHead']='Acompañamos al candidato seleccionado a través de un programa de acompañamiento para su pronta integración en tu empresa.';

$lang['advisoryndoitTalent']='Analizamos el contexto de tu empresa y diseñamos una propuesta de valor acorde a tus necesidades.';
$lang['administrationdoitTalent']='Implementamos la solución basada en una metodología de administración de proyectos para mostrar los avances y resultados.';
$lang['attentiondoitTalent']='Mantenemos una cultura de servicio al cliente basada en la calidad y calidez.';


/*Variables Contacto Solutions*/
$lang['titlecontsolt'] = '  NÓMINA EXACTA SIN CONTRATIEMPOS <br>Y PERSONAL SIN RIESGOS';
$lang['titlecontsolt2'] = 'PON AL DÍA TUS FINANZAS';
$lang['titlecontsolt3'] = 'LIBÉRATE DE TRÁMITES';
$lang['titlecontsolt4'] = 'SUMA TALENTO PARA TU EMPRESA';
$lang['titlecontsolt5'] = 'IMPULSA EL CAMBIO EN TU EMPRESA';
$lang['subtitlecontsolt'] = 'Escríbenos y nos pondremos en contacto contigo';
$lang['namecontsolt'] = 'Nombre';
$lang['lastnamecontsolt'] = 'Apellidos';
$lang['emailcontsolt'] = 'Correo electrónico';
$lang['phonecontsolt'] = 'Teléfono';
$lang['questioncontsolt'] = '¿Cómo podemos ayudarte?';
$lang['messagecontsolt'] = 'Mensaje';
$lang['termscontsolt'] = 'Acepto <a href="'.base_url('assets/pdf/avisoPrivacidad.pdf').'" target="_blank" class="terminos">términos y condiciones</a>';
$lang['sendcontsolt'] = 'ENVIAR';


/*Variables Nosotros*/
$lang['tbnosotros'] ='Nuestros consultores expertos<br>te acompañan basándose en la<br><span class="grisClaro">"CULTURA A<small>3</small>O"</span>';
$lang['culture'] ='CULTURA &nbsp;<img src='.base_url('assets/images/logo.svg').' alt="" class="imgTitles">';
$lang['advisoryn'] ='Asesoría';
$lang['textadvn'] ='Entendemos tus necesidades para proponerte soluciones ideales para tu empresa.';
$lang['administration'] ='Administración';
$lang['textadmn']='Aplicamos la solución con procesos controlados y  herramientas online que garantizan el cumplimiento de tus operaciones.';
$lang['attention'] ='Atención';
$lang['textattn']='Expertos te acompañan en cada etapa para responder a tus solicitudes y te respaldamos cuando lo necesites.';
$lang['mission']='Nuestra Misión<br><strong>ES SER TU SOCIO ESTRATÉGICO</strong>';
$lang['icon1'] ='Staff Administration';
$lang['icon2'] ='Tax & Accounting';
$lang['icon3'] ='Global Mobility';
$lang['icon4'] ='Headhunting';
$lang['icon5'] ='Talent Engineering';
$lang['advantage']= 'VENTAJAS &nbsp;<img src="'.base_url('assets/images/logo.svg').'" alt="" class="imgTitles">';
$lang['subtitleadvantage'] = 'Como socio estratégico';
$lang['vtitle1']='Cultura de trabajo estructurada';
$lang['vsubtitle1']='Aseguramos un proceso de mejora constante en nuestros servicios.';
$lang['vtitle2']='Solución completa';
$lang['vsubtitle2']='Integramos las principales actividades de Recursos Humanos y Finanzas.';
$lang['vtitle3']='Respaldo de expertos';
$lang['vsubtitle3']='Te acompañamos de manera efectiva.';
$lang['vtitle4']='Adaptabilidad';
$lang['vsubtitle4']='Estructuramos soluciones desde empresas pequeñas hasta organizaciones multinacionales.';


/*Variables Imagenes*/
$lang['cultureimg'] = 'culturaa3o.svg';
$lang['staffstructureimg'] ='staffstructure.svg';
$lang['taxstructureimg'] ='taxstructure.svg';
$lang['headstructureimg'] = 'headstructure.svg';
/*Variables contacto*/
$lang['title'] = 'CONTÁCTANOS';
$lang['subtitle'] = '¡Crea valor para tu empresa!';
$lang['paraphrase'] = 'Si tienes interés o dudas en cualquiera de nuestras soluciones, escríbenos y uno<br> de nuestros expertos te contactará';
$lang['name'] = 'Nombre';
$lang['lastname'] = 'Apellidos';
$lang['email'] = 'Correo electrónico';
$lang['phone'] = 'Teléfono';
$lang['question'] = '¿Cómo podemos ayudarte?';
$lang['message'] = 'Mensaje';
$lang['terms'] = 'Acepto <a href="'.base_url('assets/pdf/avisoPrivacidad.pdf').'" target="_blank" class="terminos">términos y condiciones</a>';
$lang['send'] = 'ENVIAR';

/*Variables Footer*/
$lang['Followus'] = 'Síguenos';
$lang['solutionsF'] = 'SOLUCIONES';
$lang['privacyPolicy'] = 'Políticas de Privacidad';

?>
