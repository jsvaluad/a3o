<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
		<div class="container-fluid no-padding">
				<div class="row-fluid no-padding">
						<div class="col-md-12 no-padding banners">
								<img src="<?= base_url('assets/images/bnosotros.jpg');?>" alt="" class="img-responsive">
								<h2 class="text-left"><?= lang('tbnosotros');?></h2>
						</div>
				</div>
		</div>
		<div class="container-fluid espaciCulture">
				<div class="row-fluid">
						<div class="col-md-9 col-center">
								<div class="container no-padding">
										<div class="row no-padding">
												<div class="col-md-12 titulos">
													<h3 class="text-center titlesNosostros"><?= lang('culture');?></h3>
													<img src="<?= base_url('assets/images/').lang('cultureimg');?>" alt="" class="img-responsive center-block imgCulture">
												</div>
												<div class="col-md-4">
														<h4 class="text-center titleCulture"><?= lang('advisoryn');?></h4>
														<p class="text-justify subtitleCulture"><?= lang('textadvn');?></p>
												</div>
												<div class="col-md-4">
														<h4 class="text-center titleCulture"><?= lang('administration');?></h4>
														<p class="text-justify subtitleCulture"><?= lang('textadmn');?></p>
												</div>
												<div class="col-md-4">
														<h4 class="text-center titleCulture"><?= lang('attention');?></h4>
														<p class="text-justify subtitleCulture"><?= lang('textattn');?></p>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
		<div class="container-fluid espasAdvantage">
				<div class="row-fluid">
						<div class="col-md-12 no-padding">
								<div class="container no-padding">
										<div class="row no-padding">
												<div class="col-md-12 titulos">
														<h3 class="text-center titlesNosostros"><?= lang('advantage');?></h3>
														<h5 class="text-center subtitleswe"><?= lang('subtitleadvantage');?></h5>
												</div>
												<div class="col-md-12 espaciadoGris"></div>
												<div class="col-md-12">
														<div class="row align-self-center">
																<div class="col-md-2 no-padding">
																		<img src="<?= base_url('assets/images/separador.png');?>" alt="" class="img-responsive center-block separadorN">
																</div>
																<div class="col-md-4 no-padding align-self-center">
																		<h3 class="titleVent"><?= lang('vtitle1');?></h3>
																</div>
																<div class="col-md-6 align-self-center">
																		<h4 class="subtitleVent"><?= lang('vsubtitle1');?></h4>
																</div>
														</div>
												</div>
												<div class="col-md-12 espaciadoGris"></div>
												<div class="col-md-12">
														<div class="row align-self-center">
																<div class="col-md-2 no-padding">
																		<img src="<?= base_url('assets/images/separador.png');?>" alt="" class="img-responsive center-block separadorN">
																</div>
																<div class="col-md-4 no-padding align-self-center">
																		<h3 class="titleVent"><?= lang('vtitle2');?></h3>
																</div>
																<div class="col-md-6 align-self-center">
																		<h4 class="subtitleVent"><?= lang('vsubtitle2');?></h4>
																</div>
														</div>
												</div>
												<div class="col-md-12 espaciadoGris"></div>
												<div class="col-md-12">
														<div class="row align-self-center">
																<div class="col-md-2 no-padding">
																		<img src="<?= base_url('assets/images/separador.png');?>" alt="" class="img-responsive center-block separadorN">
																</div>
																<div class="col-md-4 no-padding align-self-center">
																		<h3 class="titleVent"><?= lang('vtitle3');?></h3>
																</div>
																<div class="col-md-6 align-self-center">
																		<h4 class="subtitleVent"><?= lang('vsubtitle3');?></h4>
																</div>
														</div>
												</div>
												<div class="col-md-12 espaciadoGris"></div>
												<div class="col-md-12">
														<div class="row align-self-center">
																<div class="col-md-2 no-padding">
																		<img src="<?= base_url('assets/images/separador.png');?>" alt="" class="img-responsive center-block separadorN">
																</div>
																<div class="col-md-4 no-padding align-self-center">
																		<h3 class="titleVent"><?= lang('vtitle4');?></h3>
																</div>
																<div class="col-md-6 align-self-center">
																		<h4 class="subtitleVent"><?= lang('vsubtitle4');?></h4>
																</div>
														</div>
												</div>
												<div class="col-md-12 espaciadoGris"></div>
										</div>
								</div>
						</div>
				</div>
		</div>
		<div class="container espacSolution">
				<div class="row">
						<div class="col-md-12">
								<div class="row no-padding">
										<a href="<?= base_url('Index/Staff');?>" class="iconografiaLink">
												<div class="col-md-2 iconografia">
														<img src="<?= base_url('assets/images/icon-staff.svg');?>" alt="" class="img-responsive center-block">
														<h4 class="text-center"><?= lang('icon1');?></h4>
												</div>
										</a>
										<a href="<?= base_url('Index/Tax');?>" class="iconografiaLink">
												<div class="col-md-2 iconografia">
													<img src="<?= base_url('assets/images/icon-tax.svg');?>" alt="" class="img-responsive center-block">
													<h4 class="text-center"><?= lang('icon2');?></h4>
												</div>
										</a>
										<a href="<?= base_url('Index/Mobility');?>" class="iconografiaLink">
												<div class="col-md-2 iconografia">
													<img src="<?= base_url('assets/images/icon-global.svg');?>" alt="" class="img-responsive center-block">
													<h4 class="text-center"><?= lang('icon3');?></h4>
												</div>
										</a>
										<a href="<?= base_url('Index/Headhunting');?>" class="iconografiaLink">
												<div class="col-md-2 iconografia">
													<img src="<?= base_url('assets/images/icon-head.svg');?>" alt="" class="img-responsive center-block">
													<h4 class="text-center"><?= lang('icon4');?></h4>
												</div>
										</a>
										<a href="<?= base_url('Index/Talent');?>" class="iconografiaLink">
												<div class="col-md-2 iconografia">
													<img src="<?= base_url('assets/images/icon-talentN.svg');?>" alt="" class="img-responsive center-block">
													<h4 class="text-center"><?= lang('icon5');?></h4>
												</div>
										</a>
					</div>
				</div>

			</div>
		</div>

		<div class="container-fluid no-padding">
			<div class="row-fluid no-padding">
				<div class="col-md-12 no-padding mission">
					<img src="<?= base_url('assets/images/bfnosotros.jpg');?>" alt="" class="img-responsive center-block">
					<img src="<?= base_url('assets/images/lineInf.jpg');?>" alt="" class="center-block img-resposnive" style="max-width:100%">
					<h2 class="text-center"><?= lang('mission');?></h2>
					<h3 class="text-center"><?= lang('missionText');?></h3>
				</div>
			</div>
		</div>
