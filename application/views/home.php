<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid no-padding">
		<div class="row-fluid no-padding">
				<div class="col-md-12 no-padding">
						<img src="<?= base_url('assets/images/bhome.jpg');?>" alt="" class="img-responsive center-block">
						<h3 class="text-center textBannerHome"><img src="<?= base_url("assets/images/logo.svg");?>" alt="" class="img-responsive center-block logoBanner"><br><?= lang('titleBanner');?></h3>
				</div>
		</div>
</div>
<div class="container-fluid seccionGray">
		<div class="row-fluid">
				<div class="col-md-12">
						<div class="container">
								<div class="row">
										<div class="col-md-12 ">
												<h3 class="text-center titlesHome"><img src="<?= base_url('assets/images/logoWhite.svg');?>" alt="" class="imgTitles">&nbsp;<?= lang('titlegrayHome');?></h3>
												<p class="text-center"><?= lang('homesecgray');?></p>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>
<div class="container-fluid  no-padding">
		<div class="row-fluid no-padding">
				<div class="col-md-12 no-padding">
						<div class="container espacOffice">
								<div class="row-fluid">
										<div class="col-md-12">
												<img src="<?= base_url('assets/images/logoBackOffice.svg');?>" alt="" class="img-responsive center-block imgsHoms">
										</div>
								</div>
						</div>
						<div class="container-fluid no-padding">
								<div class="row-fluid no-padding">
										<div class="col-md-12 no-padding delinHoms">
												<div class="container">
														<div class="row">
																<div class="col-md-12">
																		<div class="row align-self-center">
																				<div class="col-md-2 no-padding">
																						<a href="<?= base_url('Index/Staff');?>"><img src="<?= base_url('assets/images/iconStaff.svg');?>" alt="" class="img-responsive img-right imgsDescrip" id="himgStaff"></a>
																				</div>
																				<div class="col-md-2 align-self-center">
																						<a href="<?= base_url('Index/Staff');?>"><h3 class="titleVent"><?= lang('title1back');?></h3></a>
																				</div>
																				<div class="col-md-6 align-self-center">
																						<h4 class="subtitleVent"><?= lang('subtitle1back');?></h4>
																				</div>
																				<div class="col-md-2 align-self-center">
																						<a href="<?= base_url('Index/Staff');?>" class="linksHoms"><?= lang('readmore');?></a>
																				</div>
																		</div>
																</div>
														</div>
												</div>
										</div>
										<div class="col-md-12 espaciadoHome"></div>
										<div class="col-md-12 no-padding delinHoms">
												<div class="container">
														<div class="row">
															<div class="col-md-12">
																	<div class="row align-self-center">
																			<div class="col-md-2 no-padding">
																					<a href="<?= base_url('Index/Tax');?>"><img src="<?= base_url('assets/images/iconTax.svg');?>" alt="" class="img-responsive img-right imgsDescrip" id="himgtax"></a>
																			</div>
																			<div class="col-md-2 align-self-center">
																					<a href="<?= base_url('Index/Tax');?>"><h3 class="titleVent"><?= lang('title2back');?></h3></a>
																			</div>
																			<div class="col-md-6 align-self-center">
																					<h4 class="subtitleVent"><?= lang('subtitle2back');?></h4>
																			</div>
																			<div class="col-md-2 align-self-center">
																					<a href="<?= base_url('Index/Tax');?>" class="linksHoms"><?= lang('readmore');?></a>
																			</div>
																	</div>
															</div>
														</div>
												</div>
										</div>
										<div class="col-md-12 espaciadoHome"></div>
										<div class="col-md-12 no-padding delinHoms">
												<div class="container">
														<div class="row">
															<div class="col-md-12">
																	<div class="row align-self-center">
																			<div class="col-md-2 no-padding">
																					<a href="<?= base_url('Index/Mobility');?>"><img src="<?= base_url('assets/images/iconGlobal.svg');?>" alt="" class="img-responsive img-right imgsDescrip" id="himgGlobal"></a>
																			</div>
																			<div class="col-md-2 align-self-center">
																					<a href="<?= base_url('Index/Mobility');?>"><h3 class="titleVent"><?= lang('title3back');?></h3></a>
																			</div>
																			<div class="col-md-6 align-self-center">
																					<h4 class="subtitleVent"><?= lang('subtitle3back');?></h4>
																			</div>
																			<div class="col-md-2 align-self-center">
																					<a href="<?= base_url('Index/Mobility');?>" class="linksHoms"><?= lang('readmore');?></a>
																			</div>
																	</div>
															</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>
<div class="container-fluid primHomsInfer">
		<div class="row-fluid no-padding">
				<div class="col-md-12 no-padding">
						<div class="container espacConsulting">
								<div class="row-fluid">
										<div class="col-md-12">
												<img src="<?= base_url('assets/images/logoConsulting.svg');?>" alt="" class="img-responsive center-block imgsHoms">
										</div>
								</div>
						</div>
						<div class="container-fluid no-padding">
								<div class="row-fluid no-padding">
										<div class="col-md-12 no-padding delinHoms">
												<div class="container">
														<div class="row">
																<div class="col-md-12">
																		<div class="row align-self-center">
																				<div class="col-md-2 no-padding">
																						<a href="<?= base_url('Index/Headhunting');?>"><img src="<?= base_url('assets/images/iconHeadHunting.svg');?>" alt="" class="img-responsive img-right imgsDescrip" id="himgHead"></a>
																				</div>
																				<div class="col-md-2 align-self-center">
																						<a href="<?= base_url('Index/Headhunting');?>"><h3 class="titleVent"><?= lang('title1consul');?></h3></a>
																				</div>
																				<div class="col-md-6 align-self-center">
																						<h4 class="subtitleVent"><?= lang('subtitle1consul');?></h4>
																				</div>
																				<div class="col-md-2 align-self-center">
																						<a href="<?= base_url('Index/Headhunting');?>" class="linksHoms"><?= lang('readmore');?></a>
																				</div>
																		</div>
																</div>
														</div>
												</div>
										</div>
										<div class="col-md-12 espaciadoHome"></div>
										<div class="col-md-12 no-padding delinHoms">
												<div class="container">
														<div class="row">
															<div class="col-md-12">
																	<div class="row align-self-center">
																			<div class="col-md-2 no-padding">
																					<a href="<?= base_url('Index/Talent');?>"><img src="<?= base_url('assets/images/iconTalent.svg');?>" alt="" class="img-responsive img-right imgsDescrip" id="himgTalent"></a>
																			</div>
																			<div class="col-md-2 align-self-center">
																					<a href="<?= base_url('Index/Talent');?>"><h3 class="titleVent"><?= lang('title2consul');?></h3></a>
																			</div>
																			<div class="col-md-6 align-self-center">
																					<h4 class="subtitleVent"><?= lang('subtitle2consul');?></h4>
																			</div>
																			<div class="col-md-2 align-self-center">
																					<a href="<?= base_url('Index/Talent');?>" class="linksHoms"><?= lang('readmore');?></a>
																			</div>
																	</div>
															</div>
														</div>
												</div>
										</div>
										<div class="col-md-12 espaciadoHome"></div>
								</div>
						</div>
				</div>
		</div>
</div>
<div class="container-fluid circuleHoms">
		<div class="row-fluid no-padding">
				<div class="col-md-12 no-padding">
						<div class="container">
								<div class="row">
										<div class="col-md-6 no-padding">
											<img src="<?= base_url('assets/images/circule.svg');?>" alt="" class="img-responsive center-block estruc" id="himgCircule">
											<h3 class='text-center textestruc'><?= lang('estructur');?></h3>
										</div>
										<div class="col-md-6 no-padding align-self-center"><br><br>
											<h4 class="ti1"><?= lang('ti1');?></h4>
											<p class="sti"><?= lang('sti1');?></p>
											<h4 class="ti2"><?= lang('ti2');?></h4>
											<p class="sti"><?= lang('sti2');?></p>
											<h4 class="ti3"><?= lang('ti3');?></h4>
											<p class="sti"><?= lang('sti3');?></p>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>
<div class="container-fluid no-padding">
	<div class="row-fluid no-padding">
		<div class="col-md-12 no-padding">
			<img src="<?= base_url('assets/images/lineInf.jpg');?>" alt="" class="center-block img-resposnive" style="max-width:100%">
		</div>
	</div>
</div>
