<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="backContacto">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-center">
                <h1 class="text-center"><?= lang('title');?></h1>
                <h3 class="text-center"><?= lang('subtitle');?></h3>
                <p class="text-center"><?= lang('paraphrase');?></p>
                <form class="" id="formContacto" name="formContacto">
                    <div id="c1" class="form-group"><input type="text" class="form-control" id="cname" name="cname" placeholder="<?= lang('name');?>"></div>
                    <div id="c2" class="form-group"><input type="text" class="form-control" id="clastname" name="clastname" placeholder="<?= lang('lastname');?>"></div>
                    <div id="c3" class="form-group"><input type="text" class="form-control" id="cemail" name="cemail" placeholder="<?= lang('email');?>"></div>
                    <div id="c4" class="form-group"><input type="text" class="form-control" id="cphone" name="cphone" placeholder="<?= lang('phone');?>"></div>
                    <div id="c5" class="form-group"><input type="text" class="form-control" id="cquestion" name="cquestion" placeholder="<?= lang('question');?>"></div>
                    <div id="c6" class="form-group"><textarea name="cmessage" id="cmessage" rows="8" cols="80" class="form-control" placeholder="<?= lang('message');?>"></textarea></div>
                    <div class="row">
                      <div class="col-md-6"><div class="checkbox"><label><input type="checkbox" id="terms" name="terms" value="TRUE">&nbsp;<?= lang('terms')?></label></div></div>
                      <div class="col-md-6 text-right"><button class="btn btn-sm btn-primary btnContact" href="" role="button"><?= lang('send');?></button></div>
                    </div>
                    <div class="row">
                      <div class="col-md-12" id="mensaje"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
