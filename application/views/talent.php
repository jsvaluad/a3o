<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="container-fluid no-padding">
		<div class="row-fluid no-padding">
				<div class="col-md-12 no-padding">
						<img src="<?= base_url('assets/images/btalent.jpg');?>" alt="" class="img-responsive">
            <h2 class="text-center titlesSolutions cian"><img src="<?= base_url('assets/images/icon-talent.svg');?>" alt="" class="img-responsive center-block"><br><?= lang('bannertalent');?></h2>
				</div>
		</div>
</div>
<div class="container-fluid backDefine">
    <div class="row-fluid no-padding">
        <div class="col-md-12 no-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="<?= base_url('assets/images/iconTalent.svg');?>" alt="" class="img-responsive center-block" id="himgTalent">
                    </div>
                    <div class="col-md-6">
											<h3 class="defineTitle2 morado"><?= lang('defineTitle5');?></h3>
											<p class="text-justify defineSubtite2"><?= lang('defineSubtite5');?></p>
											<p class="definemotto2 morado"><?= lang('definemotto5');?></p>
											<hr class="sepLine">
											<?= lang('listadomotto5');?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="paddsupport">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-center morado"><strong> <?= lang('titlesupport');?></strong></h2>
				<h3 class="text-center gray titleLog"><img src="<?= base_url('assets/images/logoConsuOnly.svg');?>" alt="" class="logoTitle"> &nbsp;|&nbsp; <strong> <?= lang('name5support');?></strong></h3>
				<p class="text-center gray"><?= lang('subtitle5support');?></p>
			</div>
		</div>
		<div class="row espaSupDes">
			<div class="col-md-4">
				<div class="container">
					<div class="row no-padding">
						<div class="col-md-3">
							<img src="<?= base_url('assets/images/circuleBlueDark.svg')?>" alt="" class="img-responsive center-block">
						</div>
						<div class="col-md-8 no-padding">
							<h3 class="gray"><strong><?= lang('titlepointTalent1');?></strong></h3>
							<p class="text-justify gray textSupDes"><?= lang('textpointTalent1');?></p>
							<p class="text-left gray textSupDes"><?= lang('listpointTalent1');?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="container">
					<div class="row no-padding">
						<div class="col-md-3">
							<img src="<?= base_url('assets/images/circuleBlueKing.svg')?>" alt="" class="img-responsive center-block">
						</div>
						<div class="col-md-8 no-padding">
							<h3 class="gray"><strong><?= lang('titlepointTalent2');?></strong></h3>
							<p class="text-justify gray textSupDes"><?= lang('textpointTalent2');?></p>
							<p class="text-left gray textSupDes"><?= lang('listpointTalent2');?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="container">
					<div class="row no-padding">
						<div class="col-md-3">
							<img src="<?= base_url('assets/images/circuleBlueKing.svg')?>" alt="" class="img-responsive center-block">
						</div>
						<div class="col-md-8 no-padding">
							<h3 class="gray"><strong><?= lang('titlepointTalent3');?></strong></h3>
							<p class="text-justify gray textSupDes"><?= lang('textpointTalent3');?></p>
							<p class="text-left gray textSupDes"><?= lang('listpointTalent3');?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="bredes">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h4 class="text-center blanco"><?= lang('titleshare');?></h4>
				<h4 class="text-center naranja"> <?= lang('subtitleshare');?></h4>
			</div>
			<div class="col-md-2 col-center">
				<div class="container">
					<div class="row">
						<div class="col-sm-4 col-md-4 redesCol">
							<a href="https://www.facebook.com/A3O.mx/" target="_blank" class="linkRedes">
								<img src="<?= base_url('assets/images/facebook.svg')?>" alt="" class="center-block img-responsive">
							</a>
						</div>
						<div class="col-sm-4 col-md-4 redesCol">
							<a href="https://twitter.com/A3OGroup" target="_blank" class="linkRedes">
								<img src="<?= base_url('assets/images/twitter.svg')?>" alt="" class="center-block img-responsive">
							</a>
						</div>
						<div class="col-sm-4 col-md-4 redesCol">
							<a href="https://www.linkedin.com/company-beta/2729495/" target="_blank" class="linkRedes">
								<img src="<?= base_url('assets/images/linkedin.svg')?>" alt="" class="center-block img-responsive">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container descripcion2">
		<div class="row">
			<div class="col-md-12">
				<div class="container">
					<div class="row">
						<div class="col-md-9 col-center">
							<div class="col-md-12">
								<div class="container">
									<div class="row">
										<div class="col-md-7 align-self-center">
											<h1 class="azul"><?= lang('titleDescrip5');?></h1>
											<hr class="sepLine">
											<p class="gray"><?= lang('subtitleDescrip5');?></p>
										</div>
										<div class="col-md-5">
											<img src="<?= base_url('assets/images/icon-talentN.svg');?>" alt="" class="center-block img-responsive iconSolutions">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="backhacemos">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="text-center morado"><?= lang('titledoit');?></h3>
				<h4 class="text-center gray"><?= lang('subtitledoit');?></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 recuadros">
				<img src="<?= base_url('assets/images/hace4.svg')?>" alt="" class="center-block img-responsive">
				<h4 class="text-center titulosRecuadros"><?= lang('advisoryn'); ?></h4><br><br>
				<p class="text-justify parrafosRecuadros"><?= lang('advisoryndoitTalent'); ?></p>
			</div>
			<div class="col-md-4 recuadros">
				<img src="<?= base_url('assets/images/hace5.svg')?>" alt="" class="center-block img-responsive">
				<h4 class="text-center titulosRecuadros"><?= lang('administration'); ?></h4><br><br>
				<p class="text-justify parrafosRecuadros"><?= lang('administrationdoitTalent'); ?></p>
			</div>
			<div class="col-md-4 recuadros">
				<img src="<?= base_url('assets/images/hace6.svg')?>" alt="" class="center-block img-responsive">
				<h4 class="text-center titulosRecuadros"><?= lang('attention'); ?></h4><br><br>
				<p class="text-justify parrafosRecuadros"><?= lang('attentiondoitTalent'); ?></p>
			</div>
		</div>
	</div>
</section>


<section class="backContSolut">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-center">
                <h1 class="text-center"><?= lang('titlecontsolt5');?></h1>
                <h3 class="text-center azulClaro"><?= lang('subtitlecontsolt');?></h3>
								<form class="" id="formContacto" name="formContacto"><br><br>
                    <div class="form-group"><input type="text" class="form-control" id="cname" name="cname" placeholder="<?= lang('namecontsolt');?>"></div>
                    <div class="form-group"><input type="text" class="form-control" id="clastname" name="clastname" placeholder="<?= lang('lastnamecontsolt');?>"></div>
                    <div class="form-group"><input type="text" class="form-control" id="cemail" name="cemail" placeholder="<?= lang('emailcontsolt');?>"></div>
                    <div class="form-group"><input type="text" class="form-control" id="cphone" name="cphone" placeholder="<?= lang('phonecontsolt');?>"></div>
                    <div class="form-group"><input type="text" class="form-control" id="cquestion" name="cquestion" placeholder="<?= lang('questioncontsolt');?>"></div>
                    <div class="form-group"><textarea name="cmessage" id="cmessage" rows="8" cols="80" class="form-control" placeholder="<?= lang('messagecontsolt');?>"></textarea></div>
                    <div class="row">
                      <div class="col-md-6"><div class="checkbox"><label><input type="checkbox"id="terms" name="terms" value="TRUE">&nbsp;<?= lang('termscontsolt')?></label></div></div>
                      <div class="col-md-6 text-right"><button class="btn btn-sm btn-primary btnContact" href="" role="button"><?= lang('sendcontsolt');?></button></div>
                    </div>
										<div class="row">
                      <div class="col-md-12" id="mensaje"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
