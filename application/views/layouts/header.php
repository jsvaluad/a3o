<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="es">
	<head>
		<title>A3O</title>
		<meta charset="utf-8" />
		<meta name="description" content="Te apoyamos con soluciones de recursos humanos y finanzas, entendiendo tus necesidades y brindándote la solución ideal para tu empresa, a través de nuestras herramientas online y expertos que te acompañan durante el proceso.">
  	<meta name="keywords" content="soluciones consulting, soluciones back office, soluciones de nómina, recursos humanos, human resources, finanzas, finance, staff administration, administración de personal, fácil administración de nómina, easy payroll administration, administración de nómina, fácil administración de personal, easy staff administration, errores de nómina, payroll errors, staffing, staff administration software, administración de recursos humanos, cálculos de nómina, preparación de pólizas contables, obligaciones patronales, expertos en IMSS, LFT, SAT, tax & accounting, impuestos, contabilidad, administración financiera, financial administration, cumplimiento de obligaciones financieras, esquemas contables, elaboración y declaraciones fiscales, KPI´s financieros, asesoría fiscal, tax advice, global mobility, migration, migración, expats, personal extranjero, trámites para extranjeros, procedures foreigners, proceso migratorio, declaración anual internacional, international annual declaration, headhunting, outsourcing, selección de personal, staff pick, proceso de selección, selection process, networking, herramientas de selección, management, reclutamiento, talent, recruitment, talent engineering, change management, mejora organizacional, capital humano, uman capital, desempeño, cultura organizacional, organizational culture, estrategia empresarial, business strategy, talent performance, process engineering, change management, capacitación, coaching, Lean Six Sigma, madurez organizacional, consultores de nómina, consultores outsourcing, consultores recursos humanos, consultores de finanzas, consultores de migración, consultores de coaching, empresa mexicana, outsourcing méxico, outsourcing international, herramientas online, expertos en recursos humanos, expertos en nómina, expertos en procesos administrativos, expertos en contabilidad, expertos en impuestos, identificación talento, expertos áreas funcionales, evaluación candidatos, reclutamiento, reclutamiento especializado, identificación de talento">
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,700,900" rel="stylesheet">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" href="<?= base_url('assets/images/a3o.ico');?>" />
		<link rel="stylesheet" href="<?= base_url('assets/css/jquery-ui.css');?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css');?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/animate.min.css');?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css');?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/a3o.css');?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/jquery.notifications.css');?>">
		<link rel="stylesheet" href="<?= base_url('assets/css/jquery.dataTables.min.css');?>">
	</head>
<body>
	<nav class="navbar navbar-expand-lg" id="mainNav">
			<div class="container-fluid">
    			<div class="navbar-header">
      				<a class="navbar-brand" href="<?= base_url('');?>"><img src="<?= base_url('assets/images/logo.svg')?>" alt="" class="img-responsive center-block"></a>
    			</div>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse justify-content-end" id="navbarResponsive">
							<ul class="navbar-nav navbar-center">
									<li class="nav-item"><a href="<?= base_url();?>" class="nav-link"><?= lang('home');?></a></li>
									<li class="nav-item dropdown">
											<a href="" class="nav-link dropdown-toggle" data-toggle="dropdown"><?= lang('solutions');?> <b class="caret"></b></a>
											<ul class="dropdown-menu">
													<li class="nav-item"><a href="<?= base_url('Index/Staff');?>" class="nav-link"><?= lang('staff');?></a></li>
													<li class="nav-item"><a href="<?= base_url('Index/Tax');?>" class="nav-link"><?= lang('tax');?></a></li>
													<li class="nav-item"><a href="<?= base_url('Index/Mobility');?>" class="nav-link"><?= lang('global');?></a></li>
													<li class="nav-item"><a href="<?= base_url('Index/Headhunting');?>" class="nav-link"><?= lang('headhunting');?></a></li>
													<li class="nav-item"><a href="<?= base_url('Index/Talent');?>" class="nav-link"><?= lang('talent');?></a></li>
											</ul>
									</li>
									<li class="nav-item"><a href="<?= base_url('Index/Nosotros');?>" class="nav-link lank"><?= lang('about');?></a></li>
									<li class="nav-item"><a href="http://www.oxygio.com/" target="_blank" class="nav-link"><?= lang('candidates');?></a></li>
									<li class="nav-item"><a href="<?= base_url('Index/Contacto'); ?>" class="nav-link"><?= lang('contact');?></a></li>

							</ul>
					</div>
					<ul class="nav navbar-nav navbar-right linkRight">
							<!--<li class="nav-item"><a href="http://www.oxygio.com/" target="_blank" class="nav-link"><?= lang('candidates');?></a></li>
							<li class="nav-item"><a href="" target="_blank" class="nav-link"><span class="separador">&nbsp;&nbsp;|&nbsp;&nbsp;</span></a></li>
							<li class="nav-item"><a href="#" class="nav-link"><?= lang('employees');?></a></li>-->
					</ul>
					<ul class="nav navbar-nav navbar-right linkLang">
							<li class="nav-item"><img src="<?= base_url('assets/images/mexico.svg');?>" alt="" class="img-responsive center-block lang" style="max-width:30px;float:left;" data-flag="es">&nbsp;&nbsp;</li>
							<li class="nav-item"><img src="<?= base_url('assets/images/usa.svg');?>" alt="" class="img-responsive center-block lang" style="max-width:30px;" data-flag="en"></li>
					</ul>
			</div>
	</nav>
