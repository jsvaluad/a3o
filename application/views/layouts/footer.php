<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<section class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-center">
        <div class="container">
          <div class="row">
            <div class="col-md-5 align-self-center">
              <img src="<?= base_url('assets/images/a3oGroup.svg')?>" alt="" class="img-responsive center-block">
              <p class="parrafosFooter gray">Álvaro Obregón 121-11, Roma Norte Ciudad de México, C.P. 06700</p>
              <p class="parrafosFooter gray">+52 (55) 4172 0660</p>
              <p class="parrafosFooter gray"><a href="mailto:contacto@a3ogroup.com" class=" gray">contacto@a3ogroup.com</a></p>
              <p class="parrafosFooter gray"><?= lang('Followus');?></p>
              <ul class="redesFooter">
                <li><a href="https://www.facebook.com/A3O.mx/" target="_blank" class="linkRedes"><img src="<?= base_url('assets/images/facebookBlack.svg');?>" alt="" class="center-block img-responsive"></a></li>
                <li><a href="https://twitter.com/A3OGroup" target="_blank" class="linkRedes"><img src="<?= base_url('assets/images/twitterBlack.svg');?>" alt="" class="center-block img-responsive"></a></li>
                <li><a href="https://www.linkedin.com/company-beta/2729495/" target="_blank" class="linkRedes"><img src="<?= base_url('assets/images/linkedBlack.svg');?>" alt="" class="center-block img-responsive"></a></li>
              </ul>

            </div>
            <div class="col-md-2">
              <hr class="v">

            </div>
            <div class="col-md-5 align-self-center">
              <p class="parrafosFooter gray"><?= lang('solutionsF');?></p>
              <ul class="subcategoFooter">
                <li class=""><a href="<?= base_url('Index/Staff');?>" class="navFooter gray"><?= lang('staff');?></a></li>
                <li class=""><a href="<?= base_url('Index/Tax');?>" class="navFooter gray"><?= lang('tax');?></a></li>
                <li class=""><a href="<?= base_url('Index/Mobility');?>" class="navFooter gray"><?= lang('global');?></a></li>
                <li class=""><a href="<?= base_url('Index/Headhunting');?>" class="navFooter gray"><?= lang('headhunting');?></a></li>
                <li class=""><a href="<?= base_url('Index/Talent');?>" class="navFooter"><?= lang('talent');?></a></li>
              </ul>
              <p class="pFooter2"><a href="<?= base_url('Index/Nosotros');?>" class="parrafosFooter  gray"><?= lang('about');?></a></p>
              <p class="pFooter2"><a href="http://www.oxygio.com/" target="_blank" class="parrafosFooter  gray"><?= lang('candidates');?></a></p>
              <p class="pFooter2"><a href="<?= base_url('Index/Contacto'); ?>" class="parrafosFooter  gray"><?= lang('contact');?></a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid grayFooter">
    <div class="row-fluid">
      <div class="col-md-12 no-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <p class="text-center pfooterS"><img src="<?= base_url('assets/images/mexico.svg');?>" alt="" class="img-responsive lang" style="max-width:30px;" data-flag="es">&nbsp; &nbsp;<img src="<?= base_url('assets/images/usa.svg');?>" alt="" class="img-responsive lang" style="max-width:30px;" data-flag="en"></p>
              <p class="text-center no-margin"><i><a href="<?= base_url('assets/pdf/avisoPrivacidad.pdf');?>" target="_blank"><?= lang('privacyPolicy');?></a> </i> </p>
              <p class="text-center no-margin"><i>A<small>3</small>O</i> All Rights Reserved </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script src="<?= base_url('assets/js/jquery.js');?>"></script>
<script src="<?= base_url('assets/js/jquery-ui.js');?>"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="<?= base_url('assets/js/popper.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/a30.js');?>" ></script>
<script src="<?= base_url('assets/js/jquery.notifications.js');?>" ></script>
<script src="<?= base_url('assets/js/jquery.dataTables.min.js');?>" ></script>
<script src="<?= base_url('assets/ajax/generalFunctions.js');?>" ></script>
<script src="<?= base_url('assets/ajax/functions.js');?>" ></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/56d9d55dfd8c937066731e5d/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
