var baseurl=window.location.origin+"/json";
var baseurlassets=window.location.origin+"/assets";
$(document).ready(function() {
	index();
});
function index() {
	$.ajax({ 
		type: 'post', 
		url: baseurl + '/home', 
		success: function (response){ 
			$('#home').html(response.view);
  		},
  		error: function () {
  		}
  	});
}
function addwarn(message) {
	var warning='<font color="black"><font size="4"><b><i class="fa fa-exclamation-circle " title="'+message+'"></i></b></font></font>';
	return warning;
}
function adderror(message) {
	var times='<font color="#990000"><font size="4"><b><i class="fa fa-times-circle " title="'+message+'"></i></b></font></font>';
	return times;
}
function addok() {
	var check='<font color="#3c763d"><font size="4"><b><i class="fa fa-check-circle " title="El campo es correcto."></i></b></font></font>';
	return check;
}
function error(tagmessage,tag,message){
	$('#'+tagmessage).show();
	$('#'+tagmessage).html("<center><b><i class='fa fa-times-circle faa-pulse animated'></i> "+message+"</b></center>");
	$('#'+tag+'form').removeClass("has-success").addClass("has-error");
	var times = adderror(message);
	$('#'+tag+'valid').html(times);
}
function success(tagmessage,tag){
	$('#'+tagmessage).hide();
	$('#warningmessage').hide();
	$('#'+tag+'form').removeClass("has-error").addClass("has-success");
	var check = addok();
	$('#'+tag+'valid').html(check);
}
function warn(tagmessage,tag,message){
    $('#'+tagmessage).show();
    $('#'+tagmessage).html("<center><b><i class='fa fa-times-circle faa-pulse animated'></i> "+message+"</b></center>");
    var warn = addwarn(message);
    $('#'+tag+'form').removeClass("has-error").removeClass("has-success").addClass("has-warning");;
    $('#'+tag+'valid').html(warn);
}
function normal(tagmessage,tag){
    $('#'+tagmessage).hide();
    $('#'+tag+'form').removeClass("has-error");
    $('#'+tag+'form').removeClass("has-success");
    $('#'+tag+'form').removeClass("has-warning");
    $('#'+tag+'valid').html('');
}
function createNotification(title,content,image,alive,fadeIn,fadeOut,sticky){
	$.notifications('add', {
		title: title,
		content: content,
		image: image, 
		alive: alive,
		fadeIn: fadeIn,
		fadeOut: fadeOut,
		sticky: sticky
	});
}
function alertmodal(type,title,subtitle,message,button, buttonlabel, faicon){
	var a = "<br><div class='alert alert-"+type+" alert-sm alert-dismissable'><center><h4><b><i class='icon fa fa-"+faicon+"'></i> "+title+"</b></h4></center></div>";
	bootbox.dialog({
		message: "<center><b><h3> "+subtitle+"</h3></b><br> <b>"+message+"</b></center>",
		title: a,
		buttons: {
			main: {
				label: buttonlabel,
				className: "btn-md btn-"+button,
				callback: function() {
				}
			}
		}
	});
	return a;
}
function commonerror(){
	alertmodal('danger','HA OCURRIDO UN ERROR INESPERADO','Ha ocurrido un error en el servidor','El servidor de base de datos esta teniendo problemas de comunicación, por favor, intente de nuevo y disculpe las molestias','danger', 'Aceptar','exclamation-circle');
}
function rollback(mensaje) {
   createNotification('Error de Formulario',mensaje,baseurlassets+'/img/error.png',3000,600,800,false);
}
function validarCampo(tipocampo,nombreCampo,longitudminima,expresionregular,requerido) {
	var nombre=$("#"+nombreCampo).val();
	if(tipocampo==1){
		if(nombre==''){
			if(requerido==0){
				normal("errormessage",nombreCampo);
				return true;
			}else{
				error("errormessage",nombreCampo,"Campo no puede estar vacio");
			}
		}else if(nombre.length<longitudminima){
			error("errormessage",nombreCampo,"El campo es demasiado corto, la longitud minima es de "+longitudminima);
			return false;
		}else{
			if(expresionregular==''){
				success("errormessage",nombreCampo);
				return true;
			}else{
				alert('ENTRO');
				var regex = new RegExp(expresionregular);
				if (!regex.test(nombre)){
					error("errormessage",nombreCampo,"La validacion del campo es incorrecta");
					return false;
				}else{
					success("errormessage",nombreCampo);
					return true;
				}
			}
		}
	}else if(tipocampo==2){
		if((nombre==0) || (nombre=="")){
			if(requerido==1){
				error("errormessage",nombreCampo,"Elija una Opción");
				return false;
			}
			return true;
		}else{
			success("errormessage",nombreCampo);
			return true;
		}
	}
}
var traduccion = { 
		"sProcessing": "Procesando...", 
		"sLengthMenu": "Mostrar _MENU_ registros", 
		"sZeroRecords": "No se encontraron resultados", 
		"sEmptyTable": "Ningún dato disponible en esta tabla", 
		"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", 
		"sInfoFiltered": "(filtrado de un total de MAX registros)", 
		"sInfoPostFix": "", 
		"sSearch": "Buscar:", 
		"sUrl": "", "sInfoThousands": ",", 
		"sLoadingRecords": "Cargando...", 
		"oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, 
		"oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", 
		"sSortDescending": ": Activar para ordenar la columna de manera descendente" } 
	};