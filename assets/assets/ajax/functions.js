$(document).on('click', '#hospitales', function (e) {
	$.ajax({
        type: 'post',
        url: baseurl + '/getViewHospitales',
        success: function (response) {
            $('#home').html(response.view);
        },
        error: function () {
        }
    });
});
$(document).on('click', '#doctores', function (e) {
	$.ajax({
        type: 'post',
        url: baseurl + '/getViewDoctores',
        success: function (response) {
            $('#home').html(response.view);
        },
        error: function () {
        }
    });
});

$(document).on('click', '.menuclick', function (e) {
	var id = '';
	if($(this).data('flag') == 1){
			theID='#about';
	}else if($(this).data('flag') ==2){
			theID='#services';
	}else if($(this).data('flag') == 3){
			theID='#contact';
	}
	
	$.ajax({
        type: 'post',
        url: baseurl + '/home',
        success: function (response) {
        	console.log(theID);
            $("#home").html(response.view);
            $('html,body').animate({
		        scrollTop: $(theID).offset().top},
		        'slow');
        },
        error: function () {
 
        }
    });

});


$(document).on('change', '#estados', function (e) {
	
	$.ajax({
        type: 'POST',
        url: baseurl + '/traerciudad',
        data: {estado: $("#estados").val()},
        success: function (response) {
            $("#selectCiudad").html(response.comboCiudad);
        },
        error: function () {
			 commonerror();
        }
    });
    

});

$(document).on('change', '#estadosdoc', function (e) {
	
	$.ajax({
        type: 'POST',
        url: baseurl + '/traerCiudadDoc',
        data: {estado: $("#estadosdoc").val()},
        success: function (response) {
            $("#selectCiudad").html(response.comboCiudad);
        },
        error: function () {
			 commonerror();
        }
    });
    

});

$(document).on('click', '#botonEnviar', function (e) {
     e.preventDefault();
    if(validarCampo(2,'estados',0,'',1)){

    }else{
      rollback("Por favor elija un estado");
      return false;
    } 
     if(validarCampo(2,'ciudad',0,'',1)){

    }else{
      rollback("Por favor elija un ciudad");
      return false;
    } 
    $('#cargador').css('display', 'block');
    $.ajax({
        type: 'post',
        url: baseurl + '/traetablahospitales',
        data: {estado: $("#estados").val(),ciudad:$("#ciudad").val()},
        success: function (response) {
        	$("#datosHospitales").DataTable().clear().destroy();
        	$('#cargador').css('display', 'none');
            var tabla = '';
	    	$.each(response.hospitales, function (index, value) {
	    		    tabla+="<tr><td>"+value.nombreHospital+"</td><td>"+value.calle+" " +value.colonia+"</td><td>"+value.ciudad+"</td><td>"+value.estado+"</td><td>"+value.telefonoHospital+"</td><td><a href='http://"+ value.paginaWeb +"' target='_blank'>"+ value.paginaWeb+"</a></td><td><button value='"+ value.hospitalID +"' class='btn btnModals'><i class='fa fa-map' aria-hidden='true'></i></button></td></tr>";
	        });
	        $("#datosHospitales tbody").html(tabla);
	        $('#datosHospitales').DataTable({"language": traduccion, });
	        $("#datosHospitales").show();
	                
        },
        error: function () {
 			commonerror();
        }
    });
});


$(document).on('click', '.btnModals', function (e) {
    $.ajax({
        type: 'post',
        url: baseurl + '/getViewMap',
        data: {id: $(this).val()},
        success: function (response) {
        	$("#viewMaps").html(response.view);   
        	$("#modalMaps").modal('show');
        },
        error: function () {
 			commonerror();
        }
    });
});



$(document).on('click', '#botonDoctores', function (e) {
     e.preventDefault();
    if(validarCampo(2,'estadosdoc',0,'',1)){

    }else{
      rollback("Por favor elija un estado");
      return false;
    } 
     if(validarCampo(2,'ciudaddoc',0,'',1)){

    }else{
      rollback("Por favor elija un ciudad");
      return false;
    } 
    $('#cargador').css('display', 'block');
    $.ajax({
        type: 'post',
        url: baseurl + '/traetabladoctores',
        data: {estado: $("#estadosdoc").val(),ciudad:$("#ciudaddoc").val()},
        success: function (response) {
        	$("#datosDoctores").DataTable().clear().destroy();
        	$('#cargador').css('display', 'none');
        	var tabla = '';
	    	$.each(response.doctores, function (index, value) {
	                tabla += "<tr><td>" + value.nombre + " " + value.paterno + " " + value.materno + "</td><td>" + value.calle + value.colonia + "</td><td>"+ value.telefonoConsultorio +"</td><td>"+ value.especialidad +"</td><td>" + value.ciudad + "</td><td>" + value.estado + "</td></tr>";
	        });
	        $("#datosDoctores tbody").html(tabla);
	        $('#datosDoctores').DataTable({"language": traduccion, });
	        $("#datosDoctores").show();
	        /*$("#datosDoctores").dataTable();*/        
        },
        error: function () {
 			commonerror();
        }
    });
});

$(document).on('click', '#enviarCoti', function (e) {
	if(validarCampo(1,'correo',0,'',1)){

    }else{
      rollback("Por favor introduzca un correo valido");
      return false;
    } 
    
    if(validarCampo(2,'tipoCobertura',0,'',1)){

    }else{
      rollback("Por favor elija un tipo de Cobertura");
      return false;
    } 
    if($("#parati1").is(':checked') || $("#parati2").is(':checked')) {
    	
    }else{
    	rollback("Por favor Seleccione una opción");
    	return false;
    }
    
    if($("#genero1").is(':checked') || $("#genero2").is(':checked')) {
    	
    }else{
    	rollback("Por favor Seleccione un genero");
    	return false;
    }
    
    if(validarCampo(1,'nombre',0,'',1)){

    }else{
      rollback("Por favor escriba su nombre");
      return false;
    } 
    
    if(validarCampo(1,'apellidos',0,'',1)){

    }else{
      rollback("Por favor escriba sus apellidos");
      return false;
    } 
    
    if(validarCampo(1,'edad',0,'',1)){

    }else{
      rollback("Por favor escriba su edad");
      return false;
    } 
    if(validarCampo(1,'telefono',0,'',1)){

    }else{
      rollback("Por favor escriba un telefono");
      return false;
    } 
    if(validarCampo(1,'codigoPostal',0,'',1)){

    }else{
      rollback("Por favor escriba su codigo postal");
      return false;
    } 
    
    $("#enviarCoti").html('<i class="fa fa-spinner fa-spin fa-3x fa-fw fa-1x"></i>');
	$.ajax({
        type: 'post',
        url: baseurl + '/sendCotizador',
        data: $('#formCotiza').serialize(),
        
        success: function (response) {
        	if(response == 1){
        		$("#enviarCoti").html('Su mensaje a sido enviado');	
        	}else{
        		$("#enviarCoti").html('Intente nuevamente');
        	}
        	
        	
        },
        error: function () {
 
        }
    });

});

$(document).on('click', '#enviarform', function (e) {
	if(validarCampo(1,'namec',0,'',1)){

    }else{
      rollback("Por favor escriba su nombre");
      return false;
    } 
    
    if(validarCampo(1,'phonec',0,'',1)){

    }else{
      rollback("Por favor escriba su telefono");
      return false;
    } 
    
    if(validarCampo(1,'emailc',0,'',1)){

    }else{
      rollback("Por favor escriba su email");
      return false;
    } 
    if(validarCampo(1,'commentsc',0,'',1)){

    }else{
      rollback("Por favor escriba algun comentario");
      return false;
    }
        
    $("#enviarform").html('<i class="fa fa-spinner fa-spin fa-3x fa-fw fa-1x"></i>');
	$.ajax({
        type: 'post',
        url: baseurl + '/senContact',
        data: $('#formcontact').serialize(),
        
        success: function (response) {
        	if(response == 1){
        		$("#enviarform").html('Su mensaje a sido enviado');	
        	}else{
        		$("#enviarform").html('Intente nuevamente');
        	}
        	
        	
        },
        error: function () {
 
        }
    });

});

