var baseurl=window.location.origin+"/Json";
$( document ).ready(function() {
  $("#formContacto").on("submit", function(e){
        e.preventDefault();
        if($("#cname").val() == ""){
          $("#cname").addClass( "is-invalid" );
          $("#cname").focus();
          return false;
        }else{
          $("#cname").removeClass( "is-invalid" );
        }
        if($("#clastname").val() == ""){
          $("#clastname").addClass( "is-invalid" );
          $("#clastname").focus();
          return false;
        }else{
          $("#clastname").removeClass( "is-invalid" );
        }
        if($("#cemail").val() == ""){
          $("#cemail").addClass( "is-invalid" );
          $("#cemail").focus();
          return false;
        }else{
          $("#cemail").removeClass( "is-invalid" );
        }
        email_address = $("#cemail");
          email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
          if(!email_regex.test(email_address.val())){
          $("#cemail").addClass( "is-invalid" );
          $("#cemail").focus();
            e.preventDefault();
            return false;
          }

          if($("#cphone").val() == ""){
            $("#cphone").addClass( "is-invalid" );
            $("#cphone").focus();
            return false;
          }else{
            $("#cphone").removeClass( "is-invalid" );
          }
          if($("#cquestion").val() == ""){
            $("#cquestion").addClass( "is-invalid" );
            $("#cquestion").focus();
            return false;
          }else{
            $("#cquestion").removeClass( "is-invalid" );
          }
          if($("#cmessage").val() == ""){
            $("#cmessage").addClass( "is-invalid" );
            $("#cmessage").focus();
            return false;
          }else{
            $("#cmessage").removeClass( "is-invalid" );
          }
          if (!jQuery("#terms").is(":checked")) {
              alert("Debes aceptar los términos y condiciones para continuar");
              return false;
          }
          var formData = new FormData(document.getElementById("formContacto"));
          $.ajax({url: baseurl+'/senContact',type: "post",dataType: "html",data: formData,cache: false,contentType: false,processData: false})
          .done(function(res, responseEmail){
            $('#cname').val('');
            $('#clastname').val('');
            $('#cemail').val('');
            $('#cphone').val('');
            $('#cquestion').val('');
            $('#cmessage').val('');
            $("#mensaje").html("<p class='text-center' style='color:#FFFFFF'><strong>¡Tu solicitud se envió con éxito! </strong><br><small>Pronto nos pondremos en contacto contigo</small></p>: " + res.responseEmail);});
    });

});
