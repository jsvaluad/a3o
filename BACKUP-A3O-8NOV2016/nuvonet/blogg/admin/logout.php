<?PHP
require_once("./include/membersite_config.php");

if ($fgmembersite->LogOut()){
        $fgmembersite->RedirectToURL("index.php");
   }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
      <title>Login</title>
      <link rel="STYLESHEET" type="text/css" href="assets/css/fg_membersite.css" />
      <script type='text/javascript' src='scripts/gen_validatorv31.js'></script>
</head>
<body>
<div>
	<fieldset>
	    <h2 style="color:orange;"> Log Out </h2>
	    <div class='links'><a href='login.php'>Acceder Nuevamente</a></div>
	 </fieldset>
</div>
</body>
</html>