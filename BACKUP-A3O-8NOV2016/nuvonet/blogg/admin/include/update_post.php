<?php
	require_once("membersite_config.php");
	if(!$fgmembersite->connect())
	{
		echo "<script type='text/javascript'>
				alert('No me pude conectar a la base de datos');
		</script>";
	}
	else{
		$id_post = $_POST['id_post'];
		echo $id_post."<br/>";
		$titulo = $_POST['title'];
		$contenido = $_POST['text'];
		$resumen = $_POST['summary'];
		$tipo_post = $_POST['type_post'];
		$categoria = $_POST['category'];
		$now = date('Y-m-d H:i:s', time());
		if(!$fgmembersite->updatePost($id_post,$titulo,$contenido,$resumen,$now,$categoria)){
			echo "<script type='text/javascript'>
						alert('Post NO guardado, intente nuevamente');
						window.location.replace('../index.php');
				</script>";
		}
		else{
			$urlConfirm = $fgmembersite->GetAbsoluteURLFolder();
			$urlConfirmY = trim($urlConfirm,"include");
			$url = $urlConfirmY."destacados.php";
			echo "<script type='text/javascript'>
				alert('Post Actualizado');
				window.location.replace('".$url."');
			</script>";
		}

		
	}
	
?>