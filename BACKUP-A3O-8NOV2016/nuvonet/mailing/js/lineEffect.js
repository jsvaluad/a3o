var myCanvas;
var myCanvas2;
var myCanvas3;
var ctx;
var ctx2;
var ctx3;
var timer;
var angle = 0;
var angle2 = 0;




// shim layer with setTimeout fallback
window.requestAnimFrame = (function() {
    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
    function( /* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
        window.setTimeout(callback, 1000 / 60);
    };
})();



function animloop() {
    // alert("WW");
    draw();
    requestAnimFrame(animloop, myCanvas);
}
function animloop2() {
    // alert("WW");
    draw();
    requestAnimFrame(animloop, myCanvas2);
}
function animloop3() {
    // alert("WW");
    draw();
    requestAnimFrame(animloop, myCanvas3);
}

function init() {
    myCanvas = document.getElementById('myCanvas');
    ctx = myCanvas.getContext('2d');
	
	myCanvas2 = document.getElementById('myCanvas2');
    ctx2 = myCanvas2.getContext('2d');
	
	myCanvas3 = document.getElementById('myCanvas3');
    ctx3 = myCanvas3.getContext('2d');
    timer = 0;

	animloop();
    //animloop2();
	//animloop3();
    

}

function draw() {
    angle += 0.01;
	//angle2 -= 0.01;

    clear();
	clear2();
	clear3();
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(147,104.5);
    ctx.arc(150, 150, 250, 0, angle * Math.PI, false);
    ctx.closePath();
    ctx.clip();
    ctx.drawImage(img, 0, 0, 294, 219);
    ctx.restore();
    
    ctx.beginPath();
    //ctx.arc(100, 100, 50, 0, angle * Math.PI, false);    
    ctx.lineCap = 'round';
    ctx.lineWidth = 2;
    ctx.stroke();
	
	ctx2.save();
    ctx2.beginPath();
    ctx2.moveTo(147,154.5);
    ctx2.arc(150, 150, 250, 1* Math.PI, (angle* Math.PI)+( 1* Math.PI) , false);
    ctx2.closePath();
    ctx2.clip();
    ctx2.drawImage(img2, 0, 0, 344, 221);
    ctx2.restore();
    
    ctx2.beginPath();
    //ctx.arc(100, 100, 50, 0, angle * Math.PI, false);    
    ctx2.lineCap = 'round';
    ctx2.lineWidth = 2;
    ctx2.stroke();
	
	ctx3.save();
    ctx3.beginPath();
    ctx3.moveTo(171,81);
    ctx3.arc(171,81, 250, 0, angle * Math.PI, false);
    ctx3.closePath();
    ctx3.clip();
    ctx3.drawImage(img3, 0, 0, 342, 162);
    ctx3.restore();
    
    ctx3.beginPath();
    //ctx.arc(100, 100, 50, 0, angle * Math.PI, false);    
    ctx3.lineCap = 'round';
    ctx3.lineWidth = 2;
    ctx3.stroke();
	
}

function clear() {
    return ctx.clearRect(0, 0, 294, 219);
}
function clear2() {
    return ctx2.clearRect(0, 0, 344, 221);
}
function clear3() {
    return ctx3.clearRect(0, 0, 342, 162);
}


function resetTimer() {
    return clearInterval(timer);
}


var img = new Image();
img.onload = init;
img.src = 'img/line1.png';
//img.src = 'http://eofdreams.com/data_images/dreams/green/green-03.jpg';

var img2 = new Image();
img2.onload = init;
img2.src = 'img/line2.png';
//img2.src = 'http://www.fablifemagazine.com/wp-content/uploads/2013/06/green-tea-small-300x336.jpg';

var img3 = new Image();
img3.onload = init;
img3.src = 'img/line3.png';