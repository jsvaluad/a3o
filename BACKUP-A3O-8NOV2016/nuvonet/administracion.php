<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico">

<meta name="Description" content="INTEGRATED OUTSOURCING SOLUTIONS, We are an interdisciplinary team with wide experience in business administration and advisory services. In addition to our experience and our talented in house team we add the expertise of our allied external partners, which creates an unrivalled working proposition of the highest standards.">

<meta name="Keywords" content="a3o, a30, a3o integrated outsourcing, optimum solutions company, services, determine, implement, active attention customers, integral part company, control criteria, based process effiency, administration outsourced activity, supporting step needs,company active,attention customers,process effiency,activity based,administration outsourced,integral part,company supporting, Advisory, attention, interdisciplinary team, business Expertise, Experience, Process, Control, excellence, Headhunting, Staff Administration, Finance,Optimizacion de Personal,Outsourcing de Personal,Administracion de Personal,Administracion de Nomina,Maquila,Empleado Temporal,Selección personal,Gestión personal,Outsourcing personal,RRHH,Asesoria recursos humanos,Expertos RRHH,Adecco,Manpower,Tercerizacion,Despacho Contable,Contabilidad PIME,Contabilidad Mexicana,Asesoria contable,Asesoria financiera,Asesoria fiscal,Gestión contable,Gestión financiero,Asesores impuestos,Asesoramiento RRHH,Asesoramiento fiscal,Asesoramiento contable,Despacho fiscal,Fiscalistas,Despacho fiscal contable,Micheal Page,Adecco,Headhunter - Empresa de,Headhunting - Empresa de,Headhunters,HH,Reclutamiento,Reclutamiento Especializado,Busqueda de Ejecutivos,Busqueda de Ejecutivos,Vacantes,Gerencia Media  /Ejecutivos de / Busqueda de,Busqueda de Empleo,Middle Management Search,Headhunting Firm,Headhunting Mexico">

<title>A3O - WHO WE ARE </title>

<link href="css/menu.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

<script type = "text/javascript">
    // Define the entry point
$(document).ready(
	function()
	{


$('#triangulo').show();
$('#triangulo').animate({ left: 1500}, 0);
$('#triangulo').animate({ left: 279},{duration: 1000,easing:'easeInQuad'});
$('#texto').show();
$('#texto').animate({ left: 1500}, 0);
$('#texto').animate({ left: 730},{duration: 1500,easing:'easeInQuad'});








});// fin on ready





</script>






<style type="text/css">
body
{
	margin:0px;
}
#triquiback{
   position:fixed;
   left: 50%;
   margin-left:-800px;
   top: 0;
   width:1600px;
   height:1000px;
   overflow:hidden;
   zIndex: -9999;
   background-image:url(img/quienes/back.jpg);
   background-repeat:no-repeat;
}

#main {
	position: absolute;
	left: 50%;
	width: 1000px;
	height: 800px;
	margin-left: -500px;
	overflow:hidden;


}

#menu2 {
	position:absolute;
	width:272px;
	height:170px;
	left: 10px;
	top: 140px;
	color:#666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
}
#menu2  ul li{
	color:#666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
}
#menu2 ul li:hover{
	color:#003597;
	cursor:pointer;
}
#menu2 ul {list-style-type: square;}


#menu2 A:link {text-decoration: none; color:#666;}
#menu2 A:visited {text-decoration: none; color:#666;}
#menu2 A:active {text-decoration: none; color:#666;}
#menu2 A:hover {text-decoration: none; color:#003597;}
#menu2 strong { color:#003597;}


#triangulo {
	position:absolute;
	width:436px;
	height:439px;
	z-index:1;
	left: 279px;
	top: 129px;
	background-image:url(img/quienes/diagr_adm_efic_contr.png);
	background-repeat:no-repeat;
	display:none;
}
#texto {
	position:absolute;
	width:270px;
	height:339px;
	z-index:1;
	left: 726px;
	top: 335px;
	color:#666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:9pt;
	display:none;

}
#texto ul {list-style-type: square;}

#texto p {
	margin:0px;
}
#texto ul {
	margin:5px;
}
</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34899845-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>


<div id="main">
<div id="titulo_principal">WHO WE ARE</div>
<? include 'menu.php'; ?>


<div id="menu2">
<p style="line-height:5px;"><a href="quienes.php">Who we are</a></p>
  <p style="line-height:5px;"><a href="soluciones.php">Integrated Solutions A3O</a><br /></p>
  <p style="line-height:5px;"><strong>The A3O Culture</strong></p>
<ul>
  <li><a href="implementacion.php">Advisory & Implementation Methodology A3O</a></li>
  <li><a href="administracion.php"><strong>Efficient & Controlled Administration A3O</strong></a></li>
  <li><a href="aten_servicio.php">Advanced Service Culture A3O</a></li>
</ul>
</div>


<div id="triangulo"></div>


<div id="texto">

<h2 >Efficient & Controlled Administration A3O</h2>

<p >We administer your solutions using Lean Six Sigma methodologies.</p>
<p >&nbsp;</p>

<ul>
  <li><strong>Measurement:</strong> Definition of service levels, KPI's and elaboration of related scorecards.</li>
  <li><strong>Analysis:</strong> Review of compliance with predefined Service levels.</li>
  <li><strong>Improvement:</strong> Elaboration and implementation of improvement.</li>
  <li><strong>Control:</strong> Ensure that improvement actions lead to expected results.</li>
</ul>


<p >Our methodology allows us to ensure the controlled delivery of our service levels.</p>


</div>


<?php
if(isset($_GET['ref']))
{
echo '<div title="Regresar" id="regreso" style=" cursor:pointer; position:absolute; left:32px; top:352px;">';
echo '<a href="'.$_GET['ref'].'">';
echo '<img src="img/return.png" width="55" height="30" border="0"/>';
echo '</a>';
echo '</div>';
}
?>



</div>



</body>
</html>
