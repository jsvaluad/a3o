<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico">

<meta name="Description" content="INTEGRATED OUTSOURCING SOLUTIONS, We are an interdisciplinary team with wide experience in business administration and advisory services. In addition to our experience and our talented in house team we add the expertise of our allied external partners, which creates an unrivalled working proposition of the highest standards.">

<meta name="Keywords" content="a3o, a30, a3o integrated outsourcing, optimum solutions company, services, determine, implement, active attention customers, integral part company, control criteria, based process effiency, administration outsourced activity, supporting step needs,company active,attention customers,process effiency,activity based,administration outsourced,integral part,company supporting, Advisory, attention, interdisciplinary team, business Expertise, Experience, Process, Control, excellence, Headhunting, Staff Administration, Finance,Optimizacion de Personal,Outsourcing de Personal,Administracion de Personal,Administracion de Nomina,Maquila,Empleado Temporal,Selección personal,Gestión personal,Outsourcing personal,RRHH,Asesoria recursos humanos,Expertos RRHH,Adecco,Manpower,Tercerizacion,Despacho Contable,Contabilidad PIME,Contabilidad Mexicana,Asesoria contable,Asesoria financiera,Asesoria fiscal,Gestión contable,Gestión financiero,Asesores impuestos,Asesoramiento RRHH,Asesoramiento fiscal,Asesoramiento contable,Despacho fiscal,Fiscalistas,Despacho fiscal contable,Micheal Page,Adecco,Headhunter - Empresa de,Headhunting - Empresa de,Headhunters,HH,Reclutamiento,Reclutamiento Especializado,Busqueda de Ejecutivos,Busqueda de Ejecutivos,Vacantes,Gerencia Media  /Ejecutivos de / Busqueda de,Busqueda de Empleo,Middle Management Search,Headhunting Firm,Headhunting Mexico">

<link href="css/menu.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>

<title>A3O - SERVICES</title>
<script type = "text/javascript">
    // Define the entry point
$(document).ready(
	function()
	{


	$('#circulo1').show();
	$('#circulo2').show();
	$('#circulo3').show();

	$('#circulo1').animate({ opacity: 0, left: 2000}, 0);
	$('#circulo2').animate({ opacity: 0, left: 2000}, 0);
	$('#circulo3').animate({ opacity: 0, left: 2000}, 0);

	$('#circulo1').animate({ opacity: 1, left: 75}, 500);
	$('#circulo2').animate({ opacity: 1, left: 381}, 1000);
	$('#circulo3').animate({ opacity: 1, left: 685}, 1500);




});// fin on ready





</script>






<style type="text/css">
body
{
	margin:0px;
}
#triquiback{
   position:fixed;
   left: 50%;
   margin-left:-800px;
   top: 0;
   width:1600px;
   height:1000px;
   overflow:hidden;
   zIndex: -9999;
   background-image:url(img/servicios/back_serv.jpg);
   background-repeat:no-repeat;
}
#main {
	position: absolute;
	left: 50%;
	width: 1000px;
	height: 800px;
	margin-left: -500px;
	overflow:hidden;


}



#circulo1 {
	position:absolute;
	width:257px;
	height:257px;
	z-index:1;
	left: 75px;
	top: 285px;
	background-image:url(img/servicios/botns_serv_headhunting.png);
	background-repeat:no-repeat;
	cursor:pointer;
	display:none;
}

#circulo1 #titulo,#circulo2 #titulo,#circulo3 #titulo {
	position:absolute;
	width:100%;
	height:33px;
	z-index:1;
	left: 0px;
	top: -52px;
	color:#666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:17pt;
	text-align:center;
	cursor:pointer;
}
a,
a:hover,
a:focus,
a:active
{
text-decoration: none;
}



#circulo2 {
	position:absolute;
	width:257px;
	height:257px;
	z-index:1;
	left: 381px;
	top: 287px;
	background-image:url(img/servicios/botns_serv_rh.png);
	background-repeat:no-repeat;
	cursor:pointer;
	display:none;
}
#circulo3 {
	position:absolute;
	width:257px;
	height:257px;
	z-index:1;
	left: 685px;
	top: 289px;
	background-image:url(img/servicios/botns_serv_finanzas.png);
	background-repeat:no-repeat;
	cursor:pointer;
	display:none;
}

</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34899845-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>


<div id="main">

<div id="titulo_principal">SERVICES</div>

<? include 'menu.php'; ?>



<a href="headhunting.php"><div id="circulo1"><div id="titulo">Headhunting</div></div></a>
<a href="administracion_rh.php"><div id="circulo2"><div id="titulo">Staff Administration</div></div></a>
<a href="finanzas.php"><div id="circulo3"><div id="titulo">Finance</div></div></a>
</div>



</body>
</html>
