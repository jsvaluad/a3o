<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico">

<meta name="Description" content="INTEGRATED OUTSOURCING SOLUTIONS, We are an interdisciplinary team with wide experience in business administration and advisory services. In addition to our experience and our talented in house team we add the expertise of our allied external partners, which creates an unrivalled working proposition of the highest standards.">

<meta name="Keywords" content="a3o, a30, a3o integrated outsourcing, optimum solutions company, services, determine, implement, active attention customers, integral part company, control criteria, based process effiency, administration outsourced activity, supporting step needs,company active,attention customers,process effiency,activity based,administration outsourced,integral part,company supporting, Advisory, attention, interdisciplinary team, business Expertise, Experience, Process, Control, excellence, Headhunting, Staff Administration, Finance,Optimizacion de Personal,Outsourcing de Personal,Administracion de Personal,Administracion de Nomina,Maquila,Empleado Temporal,Selección personal,Gestión personal,Outsourcing personal,RRHH,Asesoria recursos humanos,Expertos RRHH,Adecco,Manpower,Tercerizacion,Despacho Contable,Contabilidad PIME,Contabilidad Mexicana,Asesoria contable,Asesoria financiera,Asesoria fiscal,Gestión contable,Gestión financiero,Asesores impuestos,Asesoramiento RRHH,Asesoramiento fiscal,Asesoramiento contable,Despacho fiscal,Fiscalistas,Despacho fiscal contable,Micheal Page,Adecco,Headhunter - Empresa de,Headhunting - Empresa de,Headhunters,HH,Reclutamiento,Reclutamiento Especializado,Busqueda de Ejecutivos,Busqueda de Ejecutivos,Vacantes,Gerencia Media  /Ejecutivos de / Busqueda de,Busqueda de Empleo,Middle Management Search,Headhunting Firm,Headhunting Mexico">

<title>A3O - SERVICES</title>

<link href="css/menu.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>


<script type = "text/javascript">
    // Define the entry point
$(document).ready(
	function()
	{

$("#mnu_1").click(function() {

	$(".textos").hide('slow');
	$("#texto1").show('slow' );

	$('#circulo').animate({ opacity: 0}, 500,
		function()
		{
			$('#circulo').css("background-image","url(img/servicios/asesoria_headhunting_serv.png)");
			$('#circulo').animate({ opacity: 1}, 500);
		}
	);

});
$("#mnu_2").click(function() {
	$(".textos").hide('slow');
	$("#texto2").show('slow' );

	$('#circulo').animate({ opacity: 0}, 500,
		function()
		{
			$('#circulo').css("background-image","url(img/servicios/adm_headhunting_serv.png)");
			$('#circulo').animate({ opacity: 1}, 500);
		}
	);
});
$("#mnu_3").click(function() {
	$(".textos").hide('slow');
	$("#texto3").show('slow' );

	$('#circulo').animate({ opacity: 0}, 500,
		function()
		{
			$('#circulo').css("background-image","url(img/servicios/atencion_headhunting_serv.png)");
			$('#circulo').animate({ opacity: 1}, 500);
		}
	);;
});






});// fin on ready





</script>




<style type="text/css">
body
{
	margin:0px;
}
#triquiback{
   position:fixed;
   left: 50%;
   margin-left:-800px;
   top: 0;
   width:1600px;
   height:1000px;
   overflow:hidden;
   zIndex: -9999;
   background-image:url(img/servicios/back_headhunting_serv.jpg);
   background-repeat:no-repeat;
}
#main {
	position: absolute;
	left: 50%;
	width: 1000px;
	height: 800px;
	margin-left: -500px;
}


#circulo {
	position:absolute;
	width:468px;
	height:465px;
	z-index:1;
	left: 276px;
	top: 155px;
	background-image:url(img/servicios/circ_descriptivo_serv.png);
	background-repeat:no-repeat;
}



#menu2 {
	position:absolute;
	width:278px;
	height:130px;
	left: 26px;
	top: 258px;
	color:#666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
}
#menu2 table td:hover{
	color:#003597;
	cursor:pointer;
}
#menu2 A:link {text-decoration: none; color:#666;}
#menu2 A:visited {text-decoration: none; color:#666;}
#menu2 A:active {text-decoration: none; color:#666;}
#menu2 A:hover {text-decoration: none; color:#003597;}
#menu2 strong { color:#003597;}
#circulo #texto0,#circulo #texto1,#circulo #texto2,#circulo #texto3 {
	position:absolute;
	width:268px;
	height:273px;
	z-index:1;
	left: 98px;
	top: 93px;
	color:#666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:9pt;
}
#icon {
	position:absolute;
	width:67px;
	height:64px;
	z-index:1;
	left: 407px;
	top: -14px;
	background-image:url(img/servicios/icon_cabeza.jpg);
	background-repeat:no-repeat;
}
.textos ul {list-style-type:disc;}
</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34899845-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>


<div id="main">
<div id="titulo_principal">Headhunting
  <div id="icon"></div>
</div>

<? include 'menu.php'; ?>


<div id="menu2">
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td id="mnu_1">Advisory</td>
    </tr>
    <tr>
      <td id="mnu_2">Administration</td>
    </tr>
    <tr>
      <td id="mnu_3">Attention </td>
    </tr>
  <tr>
    <td><a href="servicios.php"><img src="img/return.png" width="55" height="30" border="0"/></a></td>
  </tr>
  </table>
</div>

<div id="circulo">
  <div id="texto0" class="textos" >
    <p><h2 style="text-align:center;"><br><br><br><br><br>More than a simple search...</h2></p>

   </div>

  <div id="texto1" class="textos" style="display:none">
    <p><br><br>We advise on the definition of profiles that best fit your vacancies:<br />
      </p>
    <ul>
      <li>Elaboration of Job Profiles</li>
      <li>Analysis of personality profiles </li>
      <li>Integration coaching for candidates & companies.</li>
    </ul>
    <p align="center" title="See Advisory & Implementation Methodology A3O"><a href="implementacion.php?ref=headhunting.php">View methodology</a></p>
  </div>

  <div id="texto2" class="textos" style="display:none">

<p><br /><br />Administration of the search process under a controlled and efficient environment.<br />
 </p>
  <ul>
    <li>Hunting base on an agreed "Job ScoreCard"</li>
    <li>Psychometric Testing and reference checking </li>
    <li>Elaboration of candidate 3D reports </li>
  </ul>
<p align="center" title=" See Efficient & Controlled Administration A3O"><a href="administracion.php?ref=headhunting.php">View methodology</a></p>
  </div>

  <div id="texto3" class="textos" style="display:none" >

      <p><br><br>Attention to our candidates and our customers during the integration process of the candidate.<br />
      </p>

    <ul>
      <li>Inplacement programs</li>
      <li>Followup of customer satisfaction</li>
      <li>A3O employee attention program </li>
    </ul>
<p align="center" title=" See Advanced Service Culture A3O"><a href="aten_servicio.php?ref=headhunting.php">View methodology</a></p>
  </div>
</div>


</div>



<img src="img/servicios/adm_headhunting_serv.png" width="463" height="461" style="display:none" />
<img src="img/servicios/asesoria_headhunting_serv.png" width="463" height="461" style="display:none" />
<img src="img/servicios/atencion_headhunting_serv.png" width="463" height="461" style="display:none" />



</body>
</html>
