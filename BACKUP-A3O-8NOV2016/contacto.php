﻿<?php
session_start();
include "lib/util.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="Description" content="INTEGRATED OUTSOURCING SOLUTIONS, We are an interdisciplinary team with wide experience in business administration and advisory services. In addition to our experience and our talented in house team we add the expertise of our allied external partners, which creates an unrivalled working proposition of the highest standards.">

<meta name="Keywords" content="a3o, a30, a3o integrated outsourcing, optimum solutions company, services, determine, implement, active attention customers, integral part company, control criteria, based process effiency, administration outsourced activity, supporting step needs,company active,attention customers,process effiency,activity based,administration outsourced,integral part,company supporting, Advisory, attention, interdisciplinary team, business Expertise, Experience, Process, Control, excellence, Headhunting, Staff Administration, Finance,Optimizacion de Personal,Outsourcing de Personal,Administracion de Personal,Administracion de Nomina,Maquila,Empleado Temporal,Selección personal,Gestión personal,Outsourcing personal,RRHH,Asesoria recursos humanos,Expertos RRHH,Adecco,Manpower,Tercerizacion,Despacho Contable,Contabilidad PIME,Contabilidad Mexicana,Asesoria contable,Asesoria financiera,Asesoria fiscal,Gestión contable,Gestión financiero,Asesores impuestos,Asesoramiento RRHH,Asesoramiento fiscal,Asesoramiento contable,Despacho fiscal,Fiscalistas,Despacho fiscal contable,Micheal Page,Adecco,Headhunter - Empresa de,Headhunting - Empresa de,Headhunters,HH,Reclutamiento,Reclutamiento Especializado,Busqueda de Ejecutivos,Busqueda de Ejecutivos,Vacantes,Gerencia Media  /Ejecutivos de / Busqueda de,Busqueda de Empleo,Middle Management Search,Headhunting Firm,Headhunting Mexico">

<title>A3O - CONTACT US</title>
   	<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>


    <link href="css/menu.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/ajaxfileupload.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
    <script type="text/javascript" src="js/languages/jquery.validationEngine-en.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery.validationEngine.js" charset="utf-8"></script>


<script type = "text/javascript">
    // Define the entry point
$(document).ready(
	function()
	{

	$('#ficha #btn_1').click(function() {

		if ($('#contenido_btn_1').height() < 1 )
		{
			$('.contenido_btn').animate({ height: 0}, 500);
			$('#contenido_btn_1').stop(true,true).animate({ height: 300}, 500);
			botones_mas_menos('#btn_1',false);
		}
		else
		{
			$('#contenido_btn_1').animate({ height: 0}, 500);
			botones_mas_menos('#btn_1',true);
		}
	});


	$('#ficha #btn_2').click(function() {

		if ($('#contenido_btn_2').height() < 1 )
		{
			$('.contenido_btn').animate({ height: 0}, 500);
			$('#contenido_btn_2').stop(true,true).animate({ height: 300}, 500);
			botones_mas_menos('#btn_2',false);		}
		else
		{
			$('#contenido_btn_2').animate({ height: 0}, 500);
			botones_mas_menos('#btn_2',true);
		}
	});


	$('#ficha #btn_3').click(function() {

		if ($('#contenido_btn_3').height() < 1 )
		{
			$('.contenido_btn').animate({ height: 0}, 500);
			$('#contenido_btn_3').stop(true,true).animate({ height: 300}, 500);
			botones_mas_menos('#btn_3',false);
		}
		else
		{
			$('#contenido_btn_3').animate({ height: 0}, 500);
			botones_mas_menos('#btn_3',true);
		}
	});


$("#ficha #btn_1 #text").hide();
$("#ficha #btn_2 #text").hide();
$("#ficha #btn_3 #text").hide();

$("#ficha #btn_1").hover(function () {$("#ficha #btn_1 #text").show();},  function () {$("#ficha #btn_1 #text").hide();});
$("#ficha #btn_2").hover(function () {$("#ficha #btn_2 #text").show();},  function () {$("#ficha #btn_2 #text").hide();});
$("#ficha #btn_3").hover(function () {$("#ficha #btn_3 #text").show();},  function () {$("#ficha #btn_3 #text").hide();});



});// fin on ready

function botones_mas_menos(id_btn,mas)
{

	$('#ficha #btn_1 #mas').show();
	$('#ficha #btn_1 #menos').hide();
	$('#ficha #btn_2 #mas').show();
	$('#ficha #btn_2 #menos').hide();
	$('#ficha #btn_3 #mas').show();
	$('#ficha #btn_3 #menos').hide();

	if (mas == true)
	{
		$('#ficha '+id_btn+' #mas').show();
		$('#ficha '+id_btn+' #menos').hide();
	}
	else
	{
		$('#ficha '+id_btn+' #mas').hide();
		$('#ficha '+id_btn+' #menos').show();
	}
}




//*********************** funcion para envio de archivo y datos del cv *********************
function enviar_archivo_por_ajax()
{
	$("#loading")
	.ajaxStart(function(){
		$(this).show();
	})
	.ajaxComplete(function(){
		$(this).hide();
	});

	$.ajaxFileUpload
	(
		{
			url:'procesa_envio_cv.php',
			secureuri:false,
			fileElementId:'fileToUpload',
			dataType: 'json',
			data:{ nombre:$("#upload #nombre").val(), apellido:$("#upload #apellido").val(), email:$("#upload #email").val(),tel:$("#upload #tel").val(),localiza:$("#upload #localiza").val(),especializacion:$("#upload #especializacion").val()},//datos del from
			success: function (data, status)
			{
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						alert(data.error.replace(", ","\n"));//si hubo error
					}else
					{
						alert(data.msg.replace(", ","\n"));//si todo salio bien
						$('#upload').each (function(){  this.reset();});
						$("#ficha #btn_3").trigger("click");
					}
				}
			},
			error: function (data, status, e)
			{
				alert(e);
			}
		}
	)

	return false;

}

$(document).ready(function(){ $("#upload").validationEngine('attach'); });
$(document).ready(function(){ $("#msg").validationEngine('attach'); });


</script>




<style type="text/css">
body
{
	margin:0px;
}
#triquiback{
   position:fixed;
   left: 50%;
   margin-left:-800px;
   top: 0;
   width:1600px;
   height:1000px;
   overflow:hidden;
   zIndex: -9999;
   background-image:url(img/contacto/back_gris.jpg);
   background-repeat:no-repeat;
}
#main {
	position: absolute;
	left: 50%;
	width: 1000px;
	height: 800px;
	margin-left: -500px;
}




#ficha {
	position:absolute;
	width:637px;
	height:465px;
	z-index:1;
	left: 50%;
	margin-left: -318px;
	top: 155px;

}

#ficha #btn_1 {
	position:relative;
	width:100%;
	height:52px;
	background-image:url(img/contacto/botn_pregunta.jpg);
	background-repeat:no-repeat;
}

#ficha #btn_2 {
	position:relative;
	width:100%;
	height:52px;
	background-image:url(img/contacto/botn_cv.jpg);
	background-repeat:no-repeat;
}

#ficha #btn_3 {
	position:relative;
	width:100%;
	height:52px;
	background-image:url(img/contacto/botn_contact.jpg);
	background-repeat:no-repeat;
}
#contenido_btn_1,#contenido_btn_2,#contenido_btn_3{
	position:relative;
	width:100%;
	height:0px;
	overflow:hidden;
	background-image:url(img/contacto/linea_punteada.png);
	background-repeat:no-repeat;
	border-left:thin #CCC solid;
	border-right:thin #CCC solid;


}


#mas_menos {
	position:absolute;
	width:12px;
	height:12px;
	z-index:1;
	left: 614px;
	top: 19px;
}
#text {
	position:absolute;
	width:366px;
	height:70%;
	left: 239px;
	top: 11px;
	color:#666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:8pt;
}

#form1 {
	color:#333;
	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
}
#form1 input[type=text] {
	border:none;
	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
	border-bottom:thin dashed #000;
}
#form1 textarea{

	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
	border:thin dashed #000;
	width:98%
}


#form2 {
	color:#333;
	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
}
#form2 input[type=text] {
	border:none;
	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
	border-bottom:thin dashed #000;
}
#form2 textarea{

	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
	border:thin dashed #000;
	width:100%
}
#form2 select{

	border:none;
	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
	border-bottom:thin dashed #000;
	width:190px;
}

#txt_msg {
	position:absolute;
	width:650px;
	height:20px;
	left: 50%;
	margin-left: -325px;
	top: 126px;
	color:#666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:12pt;
	text-align:center;
	z-index:1;
}

</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34899845-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script language="JavaScript" type="text/javascript">
<!--
function PopWindow()
{
window.open('http://a3ogroup.com/aviso.php','Aviso de Privacidad','width=1000,height=600,menubar=yes,scrollbars=yes,toolbar=yes,location=yes,directories=yes,resizable=yes,top=0,left=0');
}
//-->
</script>

</head>

<body onload="PopWindow()">


<div id="main">
<div id="titulo_principal">CONTACT US</div>

<div id="txt_msg"><? if(isset($_SESSION["msg_envio"]))
{

	echo utf8_decode($_SESSION["msg_envio"]);

	if($_SESSION["open_ficha"] != '0')
	{
		echo '<script>$(document).ready(function(){ $("#ficha #btn_'.$_SESSION["open_ficha"].'").trigger("click");  });</script>';
	}

} ?></div>

<? include 'menu.php'; ?>

<div id="ficha">
  <div id="btn_1">
    <div id="mas_menos"><img  id="mas" src="img/contacto/mas.png"/><img src="img/contacto/menos.png" id="menos" style="display:none;"/></div>
    <div id="text">We are here to answer any question you may have regarding who we are and what we do.</div>
  </div>
  <div id="contenido_btn_1" class="contenido_btn">

        <form id="msg" action="procesa_msg.php" method="post">

        <table id="form1" width="100%" height="300" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>NAME</td>
            <td>LAST NAME</td>
            <td>E-MAIL</td>
          </tr>
          <tr>
            <td>
            <input type="text" name="nombre" id="nombre" class="validate[required]" data-prompt-position="bottomLeft"/></td>
            <td><input type="text" name="apellido" id="nombre2" class="validate[required]" data-prompt-position="bottomLeft"/></td>
            <td><input type="text" name="email" id="nombre3" class="validate[required,custom[email]]" data-prompt-position="bottomLeft"/></td>
          </tr>
          <tr>
            <td>MESSAGE</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3">
            <textarea name="mensaje" id="mensaje"  rows="7"  class="validate[required]" data-prompt-position="bottomLeft:0,-110"></textarea>
            <p style="margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12px">All fields are required.</p></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right"><input name="" type="image" src="img/contacto/botn_enviar_preg_over.png"  onClick="return $('#msg').validationEngine('validate');"/></td>
          </tr>
        </table>

        </form>



  </div>

  <div id="btn_2">
   <div id="mas_menos"><img  id="mas" src="img/contacto/mas.png"/><img src="img/contacto/menos.png" id="menos" style="display:none;"/></div>
   <div id="text">We are always interested in identifying new talent. Are you looking for a new professional challenge? Send us your resume.</div>
  </div>
  <div id="contenido_btn_2" class="contenido_btn">

<form name="upload" id="upload" action="" method="POST" enctype="multipart/form-data">

<table id="form2" width="100%" height="300" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>NAME</td>
    <td>LAST NAME</td>
    <td>E-MAIL</td>
  </tr>
  <tr>
    <td><label for="nombre"></label>
    <input type="text" name="nombre" id="nombre"  class="validate[required]" data-prompt-position="bottomLeft"/></td>
    <td><input type="text" name="apellido" id="apellido"  class="validate[required]"  data-prompt-position="bottomLeft"/></td>
    <td><input type="text" name="email" id="email" class="validate[required,custom[email]]" data-prompt-position="bottomLeft"/></td>
  </tr>
  <tr>
    <td>PHONE</td>
    <td>LOCATION</td>
    <td>YOUR EXPERTISE</td>
  </tr>
  <tr>
    <td><input type="text" name="tel" id="tel" class="validate[required,custom[phone]]" data-prompt-position="bottomLeft"/></td>
    <td><input type="text" name="localiza" id="localiza" class="validate[required]" data-prompt-position="bottomLeft"/></td>
    <td>
    <select id ="especializacion" name="especializacion" class="validate[required]" data-prompt-position="bottomLeft" >
	  <option value=""></option>
      <?php mostrar_especializaciones("en"); ?>  



    </select>

    </td>
  </tr>
  <tr>
    <td colspan="2"><label for="fileToUpload">Send us your resume (doc,docx, pdf, pages)</label>
      <input type="file" name="fileToUpload" id="fileToUpload" class="validate[required]" data-prompt-position="bottomLeft" accept=".doc,.pdf,.docx,.pages" /></td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td align="right">
     <img id="loading" src="img/loading.gif" style="display:none;">
    <input name="" type="image" src="img/contacto/botn_enviar_preg_over.png" id="buttonUpload" onClick="if($('#upload').validationEngine('validate')){return enviar_archivo_por_ajax();}" /></td>
  </tr>
</table>

</form>

  </div>

  <div id="btn_3">
   <div id="mas_menos"><img  id="mas" src="img/contacto/mas.png"/><img src="img/contacto/menos.png" id="menos" style="display:none;"/></div>
   <div id="text">Our commitment is with you. Contact us and visit our office.</div>
  </div>
  <div id="contenido_btn_3" class="contenido_btn">
<iframe width="637" height="225" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=es-419&amp;geocode=&amp;q=%C3%81lvaro+Obreg%C3%B3n,+Ciudad+de+M%C3%A9xico,+M%C3%A9xico,+121,+roma+norte&amp;aq=&amp;sll=19.410413,-99.169541&amp;sspn=0.006911,0.011362&amp;g=%C3%81lvaro+Obreg%C3%B3n,+Ciudad+de+M%C3%A9xico,+M%C3%A9xico,+121&amp;ie=UTF8&amp;hq=&amp;hnear=%C3%81lvaro+Obreg%C3%B3n+121,+Cuauht%C3%A9moc,+Distrito+Federal,+M%C3%A9xico&amp;t=m&amp;ll=19.418415,-99.160666&amp;spn=0.004553,0.013669&amp;z=16&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=es-419&amp;geocode=&amp;q=%C3%81lvaro+Obreg%C3%B3n,+Ciudad+de+M%C3%A9xico,+M%C3%A9xico,+121,+roma+norte&amp;aq=&amp;sll=19.410413,-99.169541&amp;sspn=0.006911,0.011362&amp;g=%C3%81lvaro+Obreg%C3%B3n,+Ciudad+de+M%C3%A9xico,+M%C3%A9xico,+121&amp;ie=UTF8&amp;hq=&amp;hnear=%C3%81lvaro+Obreg%C3%B3n+121,+Cuauht%C3%A9moc,+Distrito+Federal,+M%C3%A9xico&amp;t=m&amp;ll=19.418415,-99.160666&amp;spn=0.004553,0.013669&amp;z=16" style="color:#0000FF;text-align:left" target="_blank">View Larger Map</a></small>
  <img style="position:absolute; bottom:0px; left:0px" src="img/contacto/botn_direcc.jpg" width="637" height="52" /></div>



</div>


</div>







</body>
</html>
<?php
unset($_SESSION['msg_envio']);
unset($_SESSION['open_ficha']);

?>