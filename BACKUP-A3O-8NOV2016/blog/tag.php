<?php
	$id_tag = $_GET['id']; 
	if(!$id_tag){
		echo "<script type='text/javascript'>
					alert('No hash');
					window.location.replace('index.php');
			</script>";
	}
	else{
		include_once 'header.php';
		$tags=$site->selectAllPostfromHash($id_tag); 
		if($tags == false){
			echo "<script type='text/javascript'>
					alert('No post');
					window.location.replace('index.php');
			</script>";
		}
		include_once 'sidebar.php';
	}
?>
<td class = "timeline" width="60%" valign='top'>
	<div class = 'cuadro-timeline' >
		<table class='table-post'>
		<?php
		$num_columns = 3;
		$num_rows = 0;
		$id_post = array();$title = array(); $summary=array(); 
		foreach ($tags as $key => $value) {
			$posts=$site->getAllPost($value);
			while ($row = mysql_fetch_assoc($posts)) {
				if($row['type_post'] != '7'){
					$num_rows++;
					array_push($id_post,$row['id_post']);
					array_push($title,$row['title']);
					array_push($summary,$row['summary']);
				}
			}
		}
		$k=0;
		$total = $num_rows/$num_columns;
		for ($i=0;$i<=($total);$i++){
		    echo '<tr>';
		    $j = 0;
		    while($j<$num_columns){
		    	if(empty($id_post[$k]))
		    		break;
		    	echo '<td align = "center">';
	        	echo "<hr/>";
	        	echo"<div class='timeline_contenedor_post'>";
			$image_thum = $site->getThumbImgforTimeline($id_post[$k]);
			echo $image_thum;
			echo "<div class='timeline_contenedor_texto'>
				<div class='post_title'><h5><a class='link_post' href='post.php?id=".$id_post[$k]."'>".$title[$k]."</a></h5></div>
				<div class='post_text'><p>".$summary[$k]."</p></div>
				<a class='keep_reading' href='post.php?id=".$id_post[$k]."'>Seguir Leyendo</a>
			</div>";
		        echo '</td>';
		        $j++;
		        $k++; 
		    }
		    echo '</tr>';
		}
		?>
		</table>
	</div>
</td>
<?php
include_once 'footer.php';
?>