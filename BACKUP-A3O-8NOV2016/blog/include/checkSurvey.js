/* 	##################
	#
	#	SELECCIONAR TODOS LOS INPUTS
	#
	##################
*/
/*allInputs = $(".contenedor_texto input");
var $emptyFields = allInputs.filter(function() {
    // remove the $.trim if whitespace is counted as filled
    return $.trim(this.value) === "";
});
if (!$emptyFields.length) 
	return 1;
else {
    alert("Faltan datos por completar");
    event.preventDefault();
    return 0;
}*/

$(".submitSurvey").click(function(){
	var inputValues = [];
	var ciclo = $(".totalQuestions").val();
	for( i = 1; i<ciclo; i++){
		band = false;
		div = ".question-container_" + i + " :input";
		total = $(div).length;
		checkSum = 0;
		inputVal = ".count" + i;
		$(div).each(function(index, value){
			var type = $(this).attr("type");
			
			switch(type){
				case "text":
						if($(this).val() == "")
							band = true;
						$(inputVal).val("1");
				break;
				case "checkbox":
						if($(this).is(':checked')){
							checkSum++;
						}
						if (checkSum == 0){
							band = true;
						}
						else{
							band = false;
						}
						$(inputVal).val(checkSum);
				break;
				case "radio":
						if($(this).is(':checked')){
							checkSum++;
						}
						if (checkSum == 0){
							band = true;
						}
						else{
							band = false;
						}
						$(inputVal).val("1");
				break;
			}
		});
		
		if(band == true){
			alert("Responder pregunta " + i);
			event.preventDefault();
    			return 0;
		}
	}
});