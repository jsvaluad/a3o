//add when select dropdown change
$(".selectNumQue").change(function(){
	var numQuestions = parseInt($(".selectNumQue").val());
	if(numQuestions == 0){
		alert("opcion no permitida");
	}
	else{
		$("#buildyourform div").remove();
		for(i = 0; i < numQuestions;i++){
			var intId = $("#buildyourform div").length + 1;
			var fieldWrapper = $("<div class=\"questionContainer\" id=\"question" + intId + "\"/>");
			var line = $("<hr/>")
			var qType = $("<select id='question" + intId + "' name ='typeQuestion" + intId + "' class=\"typeQuestion\"><option value=\"0\">Tipo De Pregunta</option><option value=\"1\">Respuesta Abierta</option><option value=\"2\">Opcion Multiple</option><option value=\"3\">Si/No</option></select>");
			fieldWrapper.append(line);
			fieldWrapper.append(qType);
			$("#buildyourform").append(fieldWrapper);
		}
	}
	
});

$('#buildyourform').bind('change', function(event){
    var pre_id = $(event.target).attr('id');
    var id = pre_id.substring(8,11);
    var type = $(event.target).val();
    var qIn = $("<br/><input type=\"text\" class=\"pregunta\" name='question" + id + "' placeholder='Escribe tu pregunta.' style='width:99%;'/><br/></br>");
    var div = "#question" + id;
    var divRespuestas = "#respuestas" + id;
    $(divRespuestas).remove();
    var fieldWrapper = $("<div id=\"respuestas" + id + "\"/>");
    fieldWrapper.append(qIn);
    switch(type){
    	case "0":
    		$(".pregunta").remove();
    	
    	case "2":
    		var ansOp1 = $("<input type=\"text\" class=\"respuesta1\" name='ans" + id + "_1' placeholder='Respuesta 1.' />");
    		var ansOp2 = $("<input type=\"text\" class=\"respuesta2\" name='ans" + id + "_2' placeholder='Respuesta 2.' />");
    		var ansOp3 = $("<input type=\"text\" class=\"respuesta3\" name='ans" + id + "_3' placeholder='Respuesta 3.' />");
    		var ansOp4 = $("<input type=\"text\" class=\"respuesta4\" name='ans" + id + "_4' placeholder='Respuesta 4.' />");  
    		fieldWrapper.append(ansOp1);
    		fieldWrapper.append(ansOp2);
    		fieldWrapper.append(ansOp3);
    		fieldWrapper.append(ansOp4);
    			 	
	break;
    	case "3":
    		var ansOp1 = $("<label class=\"respuesta1\">Si</label>      ");
    		var ansOp2 = $("<label class=\"respuesta2\">No</label>");
    		fieldWrapper.append(ansOp1);
    		fieldWrapper.append(ansOp2);
    	break;
    }
    $(div).append(fieldWrapper);
 });