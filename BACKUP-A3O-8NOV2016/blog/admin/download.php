<?PHP
	include("header.php");
	include("sidebar.php");
?>
		<td width="65%">
			<div class="container-post">
				<div class="button-downloads-containers">
					<form action="./include/deleteFiles.php" method='post'>
					<a class="lightbox" id='download_label_upload' href="upload_files.php">Upload</a>
					<input class='thrash_img' type='image' src='img/thrash.png'/><label class='download_label_delete' >Borrar</label>
					<?php
						$files=$fgmembersite->getFilesDownload();
						$total = count($files);
						$archivo = array();
						$fecha = array();
						for($x=0;$x<$total;$x++){
							if($x%2 == 0)
								array_push($archivo,$files[$x]);
							else
								array_push($fecha,$files[$x]);
						}
					?>
					<table class='table-download'>
						<tr>
							<th>Archivo</th>
							<th>Tipo</th>
							<th>Fecha</th>
						</tr>
						<?php
							for($x=0;$x<$total/2;$x++){
								$ele = preg_replace('/\\.[^.\\s]{3,4}$/', '', $archivo[$x]);
						    		$type = strstr($archivo[$x],'.');
								echo "
									<tr>
							    			<td>
							 <input name='check_destacado[]' type='checkbox' value='".$archivo[$x]."'/>&nbsp&nbsp<label>".$ele."</label>
							 			</td>
							    			<td>".$type."</td>
							    			<td>".date ('d/m/y ', $fecha[$x])."</td>
					    				</tr>
								";								
							}
						?>
					</table>
					</form>
				</div>
			</div>
		</td>
	</tr>
<?php
	include("footer.php");
?>