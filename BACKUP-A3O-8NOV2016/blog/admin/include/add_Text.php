<?php
	require_once("membersite_config.php");
	if(!$fgmembersite->connect())
	{
		echo "<script type='text/javascript'>
				alert('No me pude conectar a la base de datos');
		</script>";
	}
	else{
		$titulo = $_POST['title'];
		$contenido = $_POST['text'];
		$resumen = $_POST['summary'];
		$tipo_post = $_POST['type_post'];
		$categoria = $_POST['category'];
		$hashs = $_POST['hashtags'];
		$arr_hash = explode(",",$hashs);
		$hashs_length = count($arr_hash);
		$now = date('Y-m-d H:i:s', time());
		
		if(!$resumen)
				$resumen = "";
															//por default va a ser 0
											//id_post	title 	body 	date 	type_post 	destacado=0	id_category	
			if(!$fgmembersite->insertIntoPost($titulo,$contenido,$resumen,$now, $tipo_post, $categoria)){
				echo "<script type='text/javascript'>
							alert(Post NO guardado, intente nuevamente');
							window.location.replace('../index.php');
					</script>";
			}
			else{
				//insertar los hashtags para hacer el link con post
				for($i = 0; $i<$hashs_length;$i++)
				{
					$eti = trim($arr_hash[$i], " ");
					if(!$fgmembersite->insertIntoTag($eti))
					{
						echo "TAG NO guardada<br/>";
					}
				}
				
				//SUBIR FOTOS
				############ Configuration ##############
				$thumb_square_size 		= 200; //Thumbnails will be cropped to 200x200 pixels
				$max_image_width 		= 750; //Maximum image size (width)
				$max_image_height 		= 350; //Maximum image size (height)
				$thumb_prefix			= "thumb_"; //Normal thumb Prefix
				$jpeg_quality 			= 90; //jpeg quality
				$i = 0;
				##########################################
			
				$path = $fgmembersite->createDirectory();
				foreach ($_FILES['image_file']['name'] as $file => $error) {
					//uploaded file info we need to proceed
					$image_name = $_FILES['image_file']['name'][$file]; //file name
					$image_size = $_FILES['image_file']['size'][$file]; //file size
					$image_temp = $_FILES['image_file']['tmp_name'][$file]; //file temp
	
					$image_size_info 	= getimagesize($image_temp); //get image size
					
					if($image_size_info){
						$image_width 		= $image_size_info[0]; //image width
						$image_height 		= $image_size_info[1]; //image height
						$image_type 		= $image_size_info['mime']; //image type
					}else{
						die("Make sure image file is valid!");
					}
	
					//switch statement below checks allowed image type 
					//as well as creates new image from given file 
					switch($image_type){
						case 'image/png':
							$image_res =  imagecreatefrompng($image_temp); break;
						case 'image/gif':
							$image_res =  imagecreatefromgif($image_temp); break;			
						case 'image/jpeg': case 'image/pjpeg':
							$image_res = imagecreatefromjpeg($image_temp); break;
						default:
							$image_res = false;
					}
	
					if($image_res){
						//Get file extension and name to construct new file name 
						$image_info = pathinfo($image_name);
						$image_extension = strtolower($image_info["extension"]); //image extension
						++$i;
						$image_name_only = $i;
						//$image_name_only = strtolower($image_info["filename"]);//file name only, no extension
						
						//create a random name for new image (Eg: fileName_293749.jpg) ;
						$new_file_name = $image_name_only. '_' .  rand(0, 9999999999) . '.' . $image_extension;
						
						//folder path to save resized images and thumbnails
						$thumb_save_folder 	= $path . $thumb_prefix . $new_file_name; 
						$image_save_folder 	= $path . $new_file_name;
						
						//call normal_resize_image() function to proportionally resize image
						if($fgmembersite->normal_resize_image($image_res, $image_save_folder, $image_type, $max_image_width, $max_image_height, $image_width, $image_height, $jpeg_quality))
						{
							//call crop_image_square() function to create square thumbnails
							if(!$fgmembersite->crop_image_square($image_res, $thumb_save_folder, $image_type, $thumb_square_size, $image_width, $image_height, $jpeg_quality))
							{
								die('Error Creating thumbnail');
							}
						}
						imagedestroy($image_res); //freeup memory
					}
				}
	
				$urlConfirm = $fgmembersite->GetAbsoluteURLFolder();
				$urlConfirmN = trim($urlConfirm,"/include");
				$urlConfirmN = trim($urlConfirmN,'adm');
				$urlConfirmY = trim($urlConfirm,"include");
	
				echo "<script type='text/javascript'>
					if(confirm('Deseas introducir otro post?')) {
							window.location.replace('".$urlConfirmY."');
					} else {
							window.location.replace('".$urlConfirmN."');
					}</script>";
			}
		}
	
?>