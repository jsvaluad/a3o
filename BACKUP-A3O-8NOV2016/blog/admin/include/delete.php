<?php
	$id_post = $_GET['id']; 
	if(!$id_post){
		echo "<script type='text/javascript'>
					alert('No post');
					window.location.replace('index.php');
			</script>";
	}
	else{
		require_once("membersite_config.php");
		if(!$fgmembersite->connect())
		{
			echo "<script type='text/javascript'>
					alert('No me pude conectar a la base de datos');
			</script>";
		}
		$fgmembersite->deletePost($id_post);
		$path = "../posts/".$id_post;
		foreach (scandir($path) as $item) {
		        if ($item == '.' || $item == '..') {
		            continue;
		        }
		        unlink($path."/".$item);
		}
		rmdir($path);
		$url = "$_SERVER[HTTP_REFERER]";
		echo "	<script type='text/javascript'>
				alert('Post Eliminado');
				window.location.replace('".$url."');
			</script>";
	}
?>
	