﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico">

<meta name="Description" content="INTEGRATED OUTSOURCING SOLUTIONS, We are an interdisciplinary team with wide experience in business administration and advisory services. In addition to our experience and our talented in house team we add the expertise of our allied external partners, which creates an unrivalled working proposition of the highest standards.">

<meta name="Keywords" content="a3o, a30, a3o integrated outsourcing, optimum solutions company, services, determine, implement, active attention customers, integral part company, control criteria, based process effiency, administration outsourced activity, supporting step needs,company active,attention customers,process effiency,activity based,administration outsourced,integral part,company supporting, Advisory, attention, interdisciplinary team, business Expertise, Experience, Process, Control, excellence, Headhunting, Staff Administration, Finance,Optimizacion de Personal,Outsourcing de Personal,Administracion de Personal,Administracion de Nomina,Maquila,Empleado Temporal,Selección personal,Gestión personal,Outsourcing personal,RRHH,Asesoria recursos humanos,Expertos RRHH,Adecco,Manpower,Tercerizacion,Despacho Contable,Contabilidad PIME,Contabilidad Mexicana,Asesoria contable,Asesoria financiera,Asesoria fiscal,Gestión contable,Gestión financiero,Asesores impuestos,Asesoramiento RRHH,Asesoramiento fiscal,Asesoramiento contable,Despacho fiscal,Fiscalistas,Despacho fiscal contable,Micheal Page,Adecco,Headhunter - Empresa de,Headhunting - Empresa de,Headhunters,HH,Reclutamiento,Reclutamiento Especializado,Busqueda de Ejecutivos,Busqueda de Ejecutivos,Vacantes,Gerencia Media  /Ejecutivos de / Busqueda de,Busqueda de Empleo,Middle Management Search,Headhunting Firm,Headhunting Mexico">

<title>A3O - Aviso de Privacidad</title>

<link href="css/menu.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

<script type = "text/javascript">
    // Define the entry point
$(document).ready(
	function()
	{

$('#ficha_inf').show();
$('#ficha_inf').animate({ left: 1500}, 0);
$('#ficha_inf').animate({ left: 380},{duration: 1000,easing:'easeInQuad'});





});// fin on ready





</script>


<style type="text/css">
body
{
	margin:0px;
}
#triquiback{
   position:fixed;
   left: 50%;
   margin-left:-800px;
   top: 0;
   width:1600px;
   height:1600px;
   overflow:hidden;
   zIndex: -9999;
   background-image:url(img/quienes/back.jpg);
   background-repeat:no-repeat;
}
#main {
	position: absolute;
	left: 50%;
	width: 1500px;
	height: 800px;
	margin-left: -600px;
	overflow:hidden;

}
#menu2 {
	position:absolute;
	width:50px;
	height:100px;
	left: 10px;
	top: 140px;
	color:#666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
}
#menu2  ul li{
	color:#666;
	font-family:Arial, Helvetica, sans-serif;
	font-size:11pt;
}
#menu2 ul li:hover{
	color:#003597;
	cursor:pointer;
}
#menu2 ul {list-style-type: square;}
#ficha_inf ul {list-style-type: square;}

#menu2 A:link {text-decoration: none; color:#666;}
#menu2 A:visited {text-decoration: none; color:#666;}
#menu2 A:active {text-decoration: none; color:#666;}
#menu2 A:hover {text-decoration: none; color:#003597;}
#menu2 strong { color:#003597;}


#ficha_inf {
	position:fixed;
	left: 10px;
	top: 120px;
	color:#000;
	font-family:Arial, Helvetica, sans-serif;
	font-size:8pt;
	padding:30px;
	padding-right:50px;
	display:none;
}
#ficha_inf p {
	margin:0px;
}
#ficha_inf ul {
	margin:5px;
}
</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34899845-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
<div id="logo"></div>

<div id="aviso">AVISO DE PRIVACIDAD DE QUIDFPA S.C. “A3O Headhunting”:</div>

<div id="aviso_txt">
<p style="padding-top:5px;" ><strong>QUIDFPA S.C. “A3O Headhunting” </strong>con domicilio en <strong>Avenida Álvaro Obregón 121 piso 11, Roma Norte CP 06700.</strong> en cumplimiento con 
la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, le avisa que todos los datos personales que usted nos proporciona (de carácter sensibles o no sensibles) derivados de la prestación de 
los servicios que otorga este Despacho, se encuentran bajo la protección de las medidas tecnológicas y administrativas dispuestas por la legislación vigente.</p>
<p style="padding-top:5px;" >El responsable y encargado de protección de sus datos personales es <strong>QUIDFPA S.C. “A3O Headhunting” </strong>con domicilio en <strong>Avenida Álvaro Obregón 121 piso 11, Roma Norte CP 06700.</strong></p>
<p style="padding-top:5px;" ><strong>El tratamiento que se le proporciona a sus datos personales, son (aplica uno de los siguientes o todos indistintamente):</strong>
<ul  style="padding-left:50px;">
    <li>1.    Identificarlo.</li>
    <li>2.    Proporcionarle el servicio que nos solicita.</li>
    <li>3.    Otorgarle información.</li>
    <li>4.    Elaborar un expediente físico y/o electrónico.</li>
    <li>5.    Procesar sus datos en nuestros sistemas internos.</li>
    <li>6.    Ofrecerle otros servicios.</li>
    <li>7.    Compartir la información con clientes de <strong>QUIDFPA S.C. “A3O Headhunting” </strong>para efectos de colocación. </li>
</ul>
<p style="padding-top:5px;" >La protección de sus datos personales es de máxima prioridad para nosotros, es por ello que contamos con equipos físicos y sistemas especializados para resguardar su información, contamos además con políticas, 
procedimientos, estándares y guías que están enfocadas a la seguridad de la información y que  tienen como objetivo limitar y evitar la divulgación de su información, todos sus datos los tenemos clasificados y tratados como confidenciales.</p>
<p style="padding-top:5px;" ><strong>PROCEDIMIENTO PARA SOLICITAR SUS DERECHOS ARCO (ACCESO, RECTIFICACION, CANCELACION U OPOSICION)</strong>
<p style="padding-top:5px;" >Para ejercer sus derechos de acceso, rectificación, cancelación u oposición (ARCO), a partir del 6 de enero 2012, usted podrá acudir a nuestras oficinas ubicadas en la dirección antes indicada mediante solicitud que 
debe contener lo siguiente: nombre, domicilio, documentos que acrediten su identidad, descripción clara y precisa de los datos personales respecto de los que se busca ejercer sus derechos ARCO, medio por el cual desea que le demos respuesta y su firma.</p>
<p style="padding-top:5px;" ><strong>RESPUESTA A SU SOLICITUD</strong>
<p style="padding-top:5px;" >En un plazo máximo de veinte días hábiles contados después de la fecha en que se recibió la solicitud correspondiente, a efectos de que, si resulta procedente, se haga efectiva la misma dentro de los quince días siguientes a la fecha de que se comunica la respuesta.</p>
<p style="padding-top:5px;" ><strong>MODIFICACION AL AVISO</strong>
<p style="padding-top:5px;" >Cualquier cambio en el aviso de privacidad será notificado vía nuestra página de internet (<a href="http://www.a3ogroup.com/aviso.php">http://www.a3ogroup.com/aviso.php</a>) o a través del consultor responsable.</p>
<p style="padding-top:5px;" ><strong>TRATAMIENTO DE LOS DATOS</strong>
<p style="padding-top:5px;" >Se entenderá que el titular consiente tácitamente el tratamiento de sus datos, cuando habiéndose puesto a su disposición el aviso de privacidad no manifieste su oposición.
El tratamiento de datos personales se limitará al cumplimiento de las finalidades previstas en el aviso de privacidad y a fines distintos que resulten compatibles o análogos a los establecidos en el aviso de privacidad, sin que para ello se requiera obtener nuevamente el consentimiento del titular.
El tratamiento de datos personales será el que resulte necesario, adecuado y relevante en relación con las finalidades previstas en el aviso de privacidad, así como con los fines distintos que resulten compatibles o análogos.</p>
<p style="padding-top:5px;" ><strong>TRANSFERENCIA DE DATOS</strong>
<p style="padding-top:5px;" >En los casos en que <strong>QUIDFPA S.C. “A3O Headhunting” </strong>requiera transferir información a terceros nacionales en virtud del cumplimiento de obligaciones contractuales o de entrega de información al titular, <strong>QUIDFPA S.C. “A3O Headhunting” </strong>
se asegurará  que el tercero responsable asuma las mismas obligaciones que correspondan a <strong>QUIDFPA S.C. “A3O Headhunting” </strong>en materia de privacidad de datos y que cumplan con lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares. 
Sus datos no se comparten con terceros salvo para los casos establecidos en la legislación civil, penal, laboral, mercantil, fiscal y bancaria vigente de conformidad con los procesos establecidos.</p>
<br>
</div>

</body>
</html>
