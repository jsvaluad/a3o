<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>CHANGE MANAGEMENT</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/change.css">	
	<link rel="stylesheet" href="css/lightbox.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>
<div class="videoContainer">
	<div id="videoBox">
		<h1>IMPULSA EL CAMBIO<br>EN TU EMPRESA.</h1>
		<!--<p>HAZ CLIC Y CONOCE MÁS</p>
		<a href="#" id="video" class="slvj-link-lightbox 2" data-videoid="Ur7NgsD4Pmc" data-videosite="youtube"><img class="videoPlay" src="img/videoPlay.png"></a><br>-->
	</div>
</div>

<div class="container" id="descriptionContainer">
	<div class="row">
		<div class="one-third column">
			<img src="img/CMicon.png" class="u-max-full-width"/>
		</div>
		<div class="two-thirds column">
			<h1>CHANGE MANAGEMENT</h1>
			<p class="sectionDescription">Implementamos soluciones con resultados tangibles para el desarrollo del capital humano y la optimización de procesos, con una metodología estructurada.</p>
			<p class="orangeText">Para apoyarte a <b>CREAR VALOR</b></p>
			<ul class="descriptionList">
				<li>Mejora el desempeño del capital humano.</li>
				<li>Optimiza los procesos internos.</li>
				<li>Crea una cultura organizacional favorable.</li>
				<li>Facilita la transición al cambio.</li>
				<li>Alinea los objetivos con la visión estratégica de tu empresa.</li>
			</ul>
		</div>
	</div>
</div>
<div class="serviciosContainer">
	<div class="container" id="servicioBox">
		<h1>¿CÓMO TE APOYAMOS?</h1>
		<h2 class="h2Web">SOLUCIONES <b>BACK OFFICE</b> | <b>CHANGE MANAGEMENT</b></h2>
		<h2 class="h2Mobile">SOLUCIONES<br><b>BACK OFFICE</b><br><b>CHANGE MANAGEMENT</b></h2>
		<div class="row">
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>ADMINISTRACIÓN SISTÉMICA DEL CAMBIO</b></p>	
					</div>
					<p class="servicioDescription">Diseñamos e implementamos el proceso de acompañamiento para facilitar la transición del cambio.</p>
				</div>
				
			</div>
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>MADUREZ ORGANIZACIONAL</b></p>	
					</div>
					<p class="servicioDescription">A partir de diagnóstico, determinamos las acciones que tu empresa requiere para mejorar los procesos organizacionales.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>COACHING SISTÉMICO &reg; ORGANIZACIONAL</b></p>	
					</div>
					<p class="servicioDescription">Acompañamiento individual o de equipo para maximizar el desarrollo profesional y personal de tu capital humano.</p>
				</div>
			</div>
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>CAPACITACIÓN</b></p>	
					</div>
					<p class="servicioDescription">Diseñamos cursos a la medida para mejorar las habilidades y el desempeño profesional y personal.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="socialBox">
	<div class="container">
		<div class="row">
			<div class="one-half column"><p>¿Conoces a alguien a quién podría interesarle?<br><b>COMPARTE ESTE SERVICIO</b></p></div>
			<div class="one-half column">
				<a href="https://www.facebook.com/A3O.mx/" target="_blank"><img src="img/socialF.png"/></a>
				<a href="https://mx.linkedin.com/company/a3o-group" target="_blank"><img src="img/socialL.png"/></a>
				<a href=" https://twitter.com/a3ogroup" target="_blank"><img src="img/socialT.png"/></a>
			</div>
		</div>
	</div>
</div>
<div class="container" id="modeloContainer">
	<div class="row">
		<div class="one-half column" id="modeloLeftContainer">
			<ul>
				<li><h2>CAMBIO ENDÓGENO<br><span>Solucion integrada</span></h2></li>
			</ul>
			<p class="greyText">Para impulsar los procesos internos de mejora, individuales y organizacionales, en un contexto de cambio.</p>
		</div>
		<div class="one-half column">
			<img src="img/CMpuntos.png" class="u-max-full-width"/>
		</div>
	</div>
</div>
<div class="culturaContainer">
	<div class="container">
		<h1>¿CÓMO LO HACEMOS?</h1>
		<h2>CON UNA <b>CULTURA DE TRABAJO ESTRUCTURADA</b></h2>
		<div class="row" id="culturaContainerRow">
			<div class="one-third column">
				<img src="img/HHasesoria.png"/>
				<div class="pContainer">
					<p>Analizamos el contexto de tu empresa y diseñamos una propuesta de valor acorde a tus necesidades.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/HHadministracion.png"/>
				<div class="pContainer">
					<p>Implementamos la solución basada en una metodología de administración de proyectos para mostrar los avances y resultados.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/HHatencion.png"/>
				<div class="pContainer">
					<p>Mantenemos una cultura de servicio al cliente basada en la calidad y calidez.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactoContainer">
	<div class="container">
		<h1>NO TE RESISTAS AL CAMBIO</h1>
		<p>ESCRÍBENOS Y NOS PONDREMOS EN CONTACTO CONTIGO</p>
		<form class="formContacto">
			<label>Nombre</label>
			<input type="text" class="name"/>
			<label>Apellidos</label>
			<input type="text" class="apellidos"/>
			<label>Correo electrónico</label>
			<input type="text" class="email"/>
			<label>Teléfono</label>
			<input type="number" class="phone"/>			
			<label>¿Cómo podemos apoyarte?</label>
			<select class="service">
				<option value="0">Selecciona una opcion</option>
				<option value="HEADHUNTING">HEADHUNTING</option>
				<option value="STAFF ADMINISTRATION">STAFF ADMINISTRATION</option>
				<option value="CHANGE MANAGEMENT" selected>CHANGE MANAGEMENT</option>
				<option value="MIGRATION">MIGRATION</option>
				<option value="FINANCE">FINANCE</option>
			</select>
			<label>Mensaje</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>Acepto los<br><a target="_blank" href="A3O-Avisodeprivacidad_2017.pdf">términos y condiciones</a></p>
				<input type="submit" class="enviar" value="">
			</div>
			
		</form>
		<div class="error"></div>
	</div>
	<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/subir.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/service.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
<script type="text/javascript" src="script/googleAnalytics.js"></script>

</body>
</html>