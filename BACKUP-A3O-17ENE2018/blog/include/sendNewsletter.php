<?php
	if(!$_POST['correoNews']){
		echo "<script>
			alert('No correo');
			window.location.replace('../index.php');
		</script>";
	}
	else{
		$mail = $_POST['correoNews'];
		require_once("../admin/include/class.phpmailer.php");
		require_once("../admin/include/formvalidator.php");
		
		$mailer = new PHPMailer();
		$mailer->CharSet = 'utf-8';
		$mailer->FromName = "Blog A3O";
		$mailer->AddAddress("blog@a3o.mx");
		$mailer->Subject = "Nuevo usuario";
		
		$mailer->From = $mail;        
	        $mailer->Body ="El usuario: " . $mail . " se quiere registrar a tu newsletter.\n\n Fecha: " .date('d/m/Y', time());
		
	        if(!$mailer->Send())
	        {
	        	echo "<script>alert('Error al enviar el mail');</script>";
	        }
	        echo "<script>
	        	alert('Tu correo fue enviado');
	        	window.location.replace('../index.php');
	        	</script>";
	}
?>