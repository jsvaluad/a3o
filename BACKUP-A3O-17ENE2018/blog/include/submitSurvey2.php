	<?php
		$id_post = $_POST['id_post'];
		$ciclo = $_POST["totalCiclo"];
		/*	TEXTO_PREGUNTAS		*/
		$questions = $_POST['question_text'];
		
		/*	ID_PREGUNTAS	*/
		$id_questions = $_POST['id_question'];
		$id_que = array(); 
		foreach ($id_questions as $preg) {
			array_push($id_que,$preg);
		}
		
		/*	TIPO_PREGUNTAS	*/
		$type_questions = $_POST['type_question'];
		
		/*	RESPUESTAS	*/
		$respuestas = $_POST['ans'];
		$resp = array();
		foreach($respuestas as $res ) {
			array_push($resp,$res);
		}
		
		/*	NUM RESPUESTAS	*/
		$cou = array();
		for($x = 1; $x < $ciclo; $x++){
			$string = "count_que".$x;
			array_push($cou,$_POST[$string]);
		}
		
		require_once("connection.php");
		//	CICLO PARA INSERTAR 	
		$cont = 0;
		for ($i = 1; $i<$ciclo;$i++){
			$ciclodos = $cou[$i];
			for($j = 0; $j<= $ciclodos; $j++ ){
				if(intval($type_questions[$cont]) == 1){
					//echo "insert ".$resp[$cont]." <br/>";
					$site->insertIntoAnswer($id_post,$id_que[$i],$resp[$cont]);
				}
				else{
					//echo "update ".$resp[$cont]."<br/>";
					$site->updateOfferedAnswer($id_post,$id_que[$i],$resp[$cont]);
				}
				$cont++;
			}
		}
		
		require_once("../admin/include/class.phpmailer.php");
		require_once("../admin/include/formvalidator.php");
		
		$mailer = new PHPMailer();
		$mailer->CharSet = 'utf-8';
		$mailer->FromName = "Blog A3O";
		$mailer->AddAddress("elizabeth.ramescamilla@gmail.com");
		$mailer->Subject = "Encuesta";
		
		$mailer->From = "Blog-A3O";
		$Cuerpo = "Se ha contestado la encuesta:\n";
		$cont = 0;
		for ($i = 1; $i<$ciclo;$i++){
			$ciclodos = $cou[$i];
			for($j = 0; $j<= $ciclodos; $j++ ){
				$ques = $questions[$cont];
				if(intval($type_questions[$cont]) == 1){
					$ans = $resp[$cont];
				}
				else{
					$temp = $site->getAnswersText($resp[$cont]);
					$ans = $temp['answerText'];
				}
				$cont++;
				$Cuerpo = $Cuerpo . "Pregunta. ". $ques."\n Respuesta(s): ".$ans." \n\n";
			}
		}
		
		$mailer->Body = $Cuerpo."\n\n Fecha: ".date('d/m/Y', time());
	        if(!$mailer->Send())
	        {
	        	echo "<script>alert('Error al enviar el mail');</script>";
	        }
		$url = "$_SERVER[HTTP_REFERER]";
		echo "<script type='text/javascript'>{
			alert('Gracias por tu participacion!');
			window.location.replace('".$url."');
		}</script>";
	?>