<?php
class Dbconfig {
    protected $serverName;
    protected $userName;
    protected $passCode;
    protected $dbName;

    public $connectionString;
    public $dataSet;
    private $sqlQuery;

    function Dbconfig() {
        $this -> serverName = 'localhost';
        $this -> userName = 'studiosu_a3o';
        $this -> passCode = 'a3oacc3s0';
        $this -> dbName = 'studiosu_blog-test';
        $this -> connectionString = NULL;
        $this -> sqlQuery = NULL;
        $this -> dataSet = NULL;

    }

    function dbConnect()    {
        $this -> connectionString = mysql_connect($this -> serverName,$this -> userName,$this -> passCode);
        mysql_select_db($this -> dbName,$this -> connectionString);
        if($this->connectionString)
            return true;
        else
            return false;
    }

    function dbDisconnect() {
        $this -> connectionString = NULL;
        $this -> sqlQuery = NULL;
        $this -> dataSet = NULL;
        $this -> dbName = NULL;
        $this -> hostName = NULL;
        $this -> userName = NULL;
        $this -> passCode = NULL;
    }

    function selectAllPost()  {
        $tableName = "post";
        $this -> sqlQuery = 'SELECT * FROM '.$tableName." ORDER BY id_post DESC;";
        $this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
        return $this -> dataSet;
    }

    function GetAbsoluteURLFolder()
    {
        $scriptFolder = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';
        $scriptFolder .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']);
        return $scriptFolder;
    }
    
    function GetCurrentURL(){
    	$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    	return $actual_link;
    }

    function getImagefromPost($id_post){
        $path = "admin/posts/".$id_post . "/";
        $files = preg_grep('/^([^.])/', scandir($path));
        $imagenes = "";
        foreach ($files as &$element) {
            if($this->is_image($path."/".$element)){
                if($this->is_thumb_image($element) == 0){
                    $imagenes = $imagenes . "<div class='image-container'><img class='image-post' src='".$path.$element."'/></div><br/>";
                    break;
                }
            }
        }
        return $imagenes;
    }

    function is_image($path)
    {
        $a = getimagesize($path);
        $image_type = $a[2];
         
        if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
        {
            return true;
        }
        return false;
    }

    function is_thumb_image($element){
        $img_first_name = substr($element, 0, 6);
        if(strcmp($img_first_name, "thumb_") == 0)
            return 1;
        else
            return 0;
    }

    function selectHashs($id_post){
        $TagsString = "";
        //esto nos trae las id ahora necesitamos el nombre de los hash
        $this->getIDTags($id_post);
        if(!$this->dataSet){
            return "";
        }
        else{
            $rows = array();
            while ($row = mysql_fetch_assoc($this->dataSet)){
                //echo "row: " . $row["id_tag"]. "<br/>";
                $results[] = $row;
            }
            foreach ($results as $id) {
                $TagsString = $TagsString . $this->selectHashName($id['id_tag']);
            }
            return $TagsString;
        }
    }

   function selectHashName($id_tag){
        $tableName = "tag";
        $this -> sqlQuery = 'SELECT * FROM '.$tableName." WHERE id_tag='".$id_tag."';";
        $this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
        if(!$this->dataSet)
        	return "";
        $tag = mysql_fetch_assoc($this->dataSet);
           return "<a class='post_tag' href='tag.php?id=" .$tag['id_tag']."'>".$tag['name']."</a> ";
            
    }

	function getFilesfromPost($id_post){
		$path = "admin/posts/".$id_post . "/";
		$files = preg_grep('/^([^.])/', scandir($path));
		$imagenes = "";
		foreach ($files as &$element) {
		    if(!$this->is_image($path."/".$element)){
		    	$ele = preg_replace('/\\.[^.\\s]{3,4}$/', '', $element);
		    	$imagenes = $imagenes . "<div class='file-container'><a class='file-post' href='".$path.$element."'><p>".$ele."</p></a></div>";
		            /*$imagenes = $imagenes . "<div class='file-container'><p>".$element."</p><a class='file-post' href='".$path.$element."'><img src='img/file.png'/></a></div><br/>";*/
		    }
		}
		return $imagenes;
    	}
    	
    	function getFileType($id_post){
    		$path = "admin/posts/".$id_post . "/";
		$files = preg_grep('/^([^.])/', scandir($path));
		$fileExt = "";
		foreach ($files as &$element) {
			$ele = strstr($element,".");
			$fileExt = $fileExt . $ele;
		}
		return $fileExt;
    	}

            	####################
    		#
    		#	FUNCTIONES PARA POST INDIVIDUAL
    		#
    		##################   

    function getAllPost($id){
        $tableName = "post";
        $this -> sqlQuery = 'SELECT * FROM '.$tableName." WHERE id_post='".$id."';";
        $this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
        if(mysql_num_rows($this->dataSet) > 0 )
            return $this -> dataSet;
        return false;
    }


    function getImagesfromPost($id_post){
        $path = "admin/posts/".$id_post . "/";
        $files = preg_grep('/^([^.])/', scandir($path));
        foreach($files as $elementKey => $element) {
            if($this->is_thumb_image($element) == 1) {
                unset($files[$elementKey]);
            }
        }
        $i = count($files);
        if($i > 1){
            $imagenes = "<div class='image-container'><ul class='slideshow'>";
            foreach ($files as &$element) {
                if($this->is_image($path."/".$element)){
                    if($this->is_thumb_image($element) == 0){
                        $imagenes = $imagenes . "<li><img class='image-post' src='".$path.$element."' /></li>";
                    }
                }
            }
            $imagenes = $imagenes . "</ul></div>";
            return $imagenes;
        }
        else{
            foreach ($files as &$element) {
                $imagen = "<div class='image-container'><img class='image-post' src='".$path.$element."'/></div>";
            }
            return $imagen;
        }
    }
    
    function getSRC_img($id_post){
    	$path = "admin/posts/".$id_post . "/";
        $files = preg_grep('/^([^.])/', scandir($path));
        foreach($files as $elementKey => $element) {
            if($this->is_thumb_image($element) == 0) {
                unset($files[$elementKey]);
            }
        }
        $rutas = array();
	    foreach ($files as &$element) {
	        if($this->is_image($path."/".$element)){
	            if($this->is_thumb_image($element) == 1){
	                array_push($rutas,$path.$element);
	            }
	        }
	    }
	    return $rutas;
    }

    function getIDTags($id_post){
            /*GET ID_TAGS FROM ID_POST*/
        $tableName = "tags";
        $this -> sqlQuery = 'SELECT id_tag FROM '.$tableName." WHERE id_post='".$id_post."';";
        $this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
    }

    function getIDPost($id_tag){
            /*GET ID_POST from ID_TAGS*/
        $tableName = "tags";
        $this -> sqlQuery = 'SELECT id_post FROM '.$tableName." WHERE id_tag='".$id_tag."' ORDER BY id_post DESC;";
        $this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
    }

    function search_in_array($needle, $haystack){
        $search = array_search($needle, $haystack);
        return $search; 
    }

    function selectRelatedPost($id){
        //echo "id post: " . $id . "<br/><br/>";
        $this->getIDTags($id);
        $data_tags = array();
        while($tag_post_origen= mysql_fetch_assoc($this->dataSet)){
            $data_tags[] = $tag_post_origen['id_tag'];
        }
        $data_post = array();
        foreach ($data_tags as $key => $value) {
           //echo "id_tag " . $value."<br/>";
            $post_related = $this->getIDPost($value);
            while($post_ids = mysql_fetch_assoc($this->dataSet)){
                //echo "post_id: " .$post_ids['id_post'] . "<br/>";
                if($id != $post_ids['id_post']){
                    if($this->search_in_array($post_ids['id_post'],$data_post) === false){
                        	$data_post[] = $post_ids['id_post'];
                    }
                }
                else{
                    //echo "mismo id <br/>";
                }
            }
            //echo "<br/>";
        }
        //print_r($data_post);
        if (count($data_post) == 0){
            $html = "<div class='posts_related'><p>No post relacionados</p></div>";
        }
        else{
            $html = "<div class='posts_related'><table class='post_related_table'>";
            $i=0;
            $id = array();
            $title = array();
            $image = array();
            $category = array();
            foreach ($data_post as $key => $value) {
                $i++;
                if($i>4)
                    break;
                $post = $this->getAllPost($value);
                $row = mysql_fetch_assoc($post);
                if($row['type_post'] != '7'){
	                array_push($id,$row['id_post']);
	                array_push($title,$row['title']);
	                array_push($category,$row['id_category']);
	                 if($row['type_post'] == 1 || $row['type_post'] == 2 || $row['type_post'] == 5)
	                 	array_push($image,$this->getThumbImg($value));
	                 else
	                 	array_push($image,"<div class='post-thumbimage-container'><img class='image-thumb' src='img/group.png'/></div><br/>");
	           }
           }
           for($x = 0; $x<4;$x++){
           	$html = $html . "<td>".$image[$x]."</td>";
           }
           $html = $html . "</tr><tr>";
           for($x = 0; $x<4;$x++){
           	switch($category[$x]){
           		case '1': $cat = "General";break;
           		case '2': $cat = "Headhunting"; break;
           		case '3': $cat = "Staffing"; break;
           		case '4': $cat = "Development"; break;
           		case '5': $cat = "Finance"; break;
           		default:  $cat = "";
           	}
           	$html = $html . "<td valign='top'><div class='post-related-title'><b><a href='post.php?id=".$id[$x]."'>". $title[$x]."</a></b><br/>".$cat."</div></td>";
           }
           $html = $html . "</tr></table></div>";
        }
        return $html;
    }

    function getThumbImgforTimeline($id_post){
        //$pathPC = "C://xampp/htdocs/a3oBlog-Eli/admin/posts/".$id_post;
        $path = "admin/posts/".$id_post . "/";
        $files = preg_grep('/^([^.])/', scandir($path));
        $imagenes = "";

        foreach ($files as &$element) {
            //echo $pathPC."/".$element;
            if($this->is_image($path."/".$element)){
                if($this->is_thumb_image($element) == 1){
			$imagenes = $imagenes . "<div class='thumbimage-container'><a href='post.php?id=".$id_post."'><img class='image-thumb-post' src='".$path.$element."'/></a></div><br/>";
                    	break;
                }
            }
        }
        return $imagenes;
    }
    
    function getThumbImg($id_post){
        //$pathPC = "C://xampp/htdocs/a3oBlog-Eli/admin/posts/".$id_post;
        $path = "admin/posts/".$id_post . "/";
        $files = preg_grep('/^([^.])/', scandir($path));
        $imagenes = "";

        foreach ($files as &$element) {
            //echo $pathPC."/".$element;
            if($this->is_image($path."/".$element)){
                if($this->is_thumb_image($element) == 1){
                    $imagenes = $imagenes . "<div class='post-thumbimage-container'><img class='image-thumb' src='".$path.$element."'/></div><br/>";
                    break;
                }
            }
        }
        return $imagenes;
    }
    
    	##########################
	#
	#	TRAER TODAS LAS PREGUNTAS
	#
	##########################
	function getAllQuestions($id_post){
		$tableName = "post_question";
        	$this -> sqlQuery = 'SELECT id_question FROM '.$tableName." WHERE id_post =".$id_post.";";
       	 	$this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
       	 	$question_array = array();
	        while($ques_ids = mysql_fetch_assoc($this->dataSet)){
	            $question_array[] = $ques_ids['id_question'];
	        }
	        $question_text = array();
	        foreach ($question_array as $key => $value) {
        		$textoPregunta = $this->getQuestionText($value);
        		$question_text[] = $textoPregunta;
	        }
	        return $question_text;
	        
	}
	
	function getQuestionText($id_question){
		$tableName = "question";
		$this -> sqlQuery = 'SELECT * FROM '.$tableName." WHERE id_question = ".$id_question.";";
       	 	$this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
       	 	$pregunta = mysql_fetch_assoc($this -> dataSet);
       	 	return $pregunta;
	}
	
	function getAnswers($id_question){
		$tableName="post_question_answer";
		$this -> sqlQuery = 'SELECT * FROM '.$tableName." WHERE id_question = ".$id_question.";";
       	 	$this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
       	 	$resp_array = array();
       	 	while($respuesta = mysql_fetch_assoc($this -> dataSet)){
       	 		$resp_array[] = $respuesta['id_offer_answer'];
       	 	}
       	 	$resp_texto = array();
       	 	foreach ($resp_array as $key => $value) {
        		$textoResp= $this->getRespText($value);
        		$resp_texto[] = $textoResp;
	        }
	        return $resp_texto;
	}
	
	function getRespText($id_answer){
		$tableName = "offeredAnswer";
		$this -> sqlQuery = 'SELECT * FROM '.$tableName." WHERE id_offer_Answer = ".$id_answer.";";
       	 	$this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
       	 	$respuesta = mysql_fetch_assoc($this -> dataSet);
       	 	return $respuesta;
	}
    
    
    
    
    /*	##########################
	#
	#	FUNCTIONES PARA PAGINA DE TAGS 
	#
	##########################*/
    function selectAllPostfromHash($id_tag)  {
        $tableName = "tags";
        $this -> sqlQuery = 'SELECT id_post FROM '.$tableName." WHERE id_tag =".$id_tag.";";
        $this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
        $post_array = array();
        while($post_ids = mysql_fetch_assoc($this->dataSet)){
            //echo "post_id: " .$post_ids['id_post'] . "<br/>";
            $post_array[] = $post_ids['id_post'];
        }
        $post_data[] = array();
        foreach ($post_array as $key => $value) {
            $post = $this->getAllPost($value);
            $row = mysql_fetch_assoc($post);
            $post_data[] = $row;
        }
        return $post_array;
    }
    

    /********       FUNCTIONES PARA CADA CATEGORIA      ************/
    function selectAllCategory($string){
        $tableName = "post";
        switch ($string) {
            case 'hh':
                $this -> sqlQuery = 'SELECT * FROM '.$tableName." WHERE id_category = 2 ORDER BY id_post DESC;";
                $this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
                return $this -> dataSet;
                break;
            case 'staff':
                 $this -> sqlQuery = 'SELECT * FROM '.$tableName." WHERE id_category = 3 ORDER BY id_post DESC;";
                $this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
                return $this -> dataSet;
                break;
            case 'do':
                $this -> sqlQuery = 'SELECT * FROM '.$tableName." WHERE id_category = 4 ORDER BY id_post DESC;";
                $this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
                return $this -> dataSet;
                break;
            case 'fin':
                $this -> sqlQuery = 'SELECT * FROM '.$tableName." WHERE id_category = 5 ORDER BY id_post DESC;";
                $this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString);
                return $this -> dataSet;
                break;
            default:
                # code...
                break;
        }
    }
    
    function selectAllDownload(){
		$path = "admin/posts/files/";
		$nombres=array();
		$files = preg_grep('/^([^.])/', scandir($path));
		foreach ($files as &$element) {
		    	array_push($nombres,$element);
		    	array_push($nombres,filemtime($path.$element));
		}
		return $nombres;
                
    }
    
    function searchInDB($word){
    	$this -> sqlQuery = "	SELECT *
    				FROM `post` 
    				WHERE (	   title LIKE '%$word%' 
    					OR summary LIKE '%$word%' 
    					OR body LIKE '%$word%'
    				)";
	$this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString) or die ("No se puede seleccionar la consulta porque ".	Mysql_error());
	
	//ahora regresar tambien el arreglo de post donde esten las tags de esta palabra
	//return $this->dataSet;
	$posts = array();
	while ($row = mysql_fetch_assoc($this->dataSet)){
		//echo "id_post in search: " . $row['id_post']."<br/>";
		$posts[] = $row['id_post'];
	}
	
	$postsWithTag = array();
	$postsWithTag = $this->searchTags($word);
	
	//revisar si con el tag no trae post que ya esten en la busqueda
	$resultado = array_unique(array_merge($posts,$postsWithTag));
	return $resultado;
   }
	
	function searchTags($word){
	$tableName = "tag";
	$this -> sqlQuery = 'SELECT id_tag FROM '.$tableName." WHERE ( name LIKE '$word')";
	$this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString) or die ("No se puede seleccionar la consulta porque ".	Mysql_error());
	$post_with_word=array();
	
	while ($row = mysql_fetch_assoc($this->dataSet)){
		$post_with_word[] = $row['id_tag'];
		//echo "id_tag in search: " . $row['id_tag']."<br/>";
	}
	$tablename = "tags";
	$postfromTags = array();
	foreach ($post_with_word as $key){
		//echo $key."<br/>";
		$algo = $this->getIDPost($key);
		while ($row = mysql_fetch_assoc($this->dataSet)){
			$postfromTags[] = $row['id_post'];
		}
	}
	return $postfromTags;
    }

	/*	############################
		#
		#	SURVEY
		#
		############################
	*/
	function insertIntoAnswer($id_post,$id_question,$textAnswer){
		$this->dbConnect();
		$tableName = "answer";
		//echo $textAnswer."<br/>";
		$this -> sqlQuery = 'INSERT INTO '.$tableName. " (id_post, id_question, answerText) VALUES (".$id_post.",".$id_question.",'".$textAnswer."')";
		//echo $this->sqlQuery;
		$this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString) or die ("No se puede seleccionar la consulta porque: ".Mysql_error());	
	}
	
	function updateOfferedAnswer($id_post,$id_question,$id_offer_answer){
		$this->dbConnect();
		$tableName = "offeredAnswer";
		$this -> sqlQuery = "UPDATE ".$tableName." SET cont = cont+1 WHERE id_offer_answer = '".$id_offer_answer."'";
		//echo $this->sqlQuery;
		$this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString) or die ("No se puede seleccionar la consulta porque: ".Mysql_error());
	}
	
	function getAnswersText($id_offer_answer){
		$this->dbConnect();
		$tableName = "offeredAnswer";
		$this -> sqlQuery = "SELECT answerText FROM ".$tableName." WHERE id_offer_answer = ".$id_offer_answer;
		//echo $this->sqlQuery;
		$this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString) or die ("No se puede seleccionar la consulta porque: ".Mysql_error());
		if(!$this -> dataSet)
			return 0;
		else
			return mysql_fetch_assoc($this -> dataSet);
	}
	
	
	/*	############################
		#
		#	POST DESTACADOS
		#
		############################*/
		
	function getPublicacionesDestacadas(){
		$tableName = "post";
		$this -> sqlQuery = 'SELECT id_post, title,id_category FROM '.$tableName. " WHERE destacado=1";
		$this -> dataSet = mysql_query($this -> sqlQuery,$this -> connectionString) or die ("No se puede seleccionar la consulta porque: ".Mysql_error());
		return $this->dataSet;
	}
	
	function  getThumDestacada($id_post){
		$path = "admin/posts/".$id_post . "/";
	        $files = preg_grep('/^([^.])/', scandir($path));
	        $imagenes = "";
	        foreach ($files as &$element) {
	            if($this->is_image($path."/".$element)){
	                if($this->is_thumb_image($element) == 0){
	                    $imagenes = "<div class='thum-destacado-container'><img class='thum-destacado' src='".$path.$element."'/></div>";
	                    break;
	                }
	            }
	        }
	        return $imagenes;
	}
	
	/*	############################
		#
		#	BITLY URL
		#
		############################*/
		/* make a URL small */
	function make_bitly_url($url,$format = 'xml',$version = '2.0.1')
	{
		//create the URL
		$login = "o_58pk4kauhe";
		$appkey = "R_cae432feb92a4bd4b4f8e6e30a637eec";
		$bitly = 'http://api.bit.ly/shorten?version='.$version.'&longUrl='.urlencode($url).'&login='.$login.'&apiKey='.$appkey.'&format='.$format;
		//get the url
		//could also use cURL here
		$response = file_get_contents($bitly);
		
		//parse depending on desired format
		if(strtolower($format) == 'json')
		{
			$json = @json_decode($response,true);
			return $json['results'][$url]['shortUrl'];
		}
		else //xml
		{
			$xml = simplexml_load_string($response);
			return 'http://bit.ly/'.$xml->results->nodeKeyVal->hash;
		}
	}
	
	
}
?>