<?php
	$id_post = $_GET['id']; 
	if(!$id_post){
		echo "<script type='text/javascript'>
					alert('No post');
					window.location.replace('index.php');
			</script>";
	}
	else{
		//connect to database
		header('Content-Type: text/html; charset=UTF-8');
		include_once("include/connection.php");
		$site->dbConnect();
		$post=$site->getAllPost($id_post);
		if($post == false){
			echo "<script type='text/javascript'>
					alert('No post');
					window.location.replace('index.php');
			</script>";
			return 0;
		}
		$url = $site->GetCurrentURL();
		$row = mysql_fetch_assoc($post);
		if(!$row){
			echo "<script type='text/javascript'>
					window.location.replace('index.php');
			</script>";
			return 0;
		}
		$path = $site->getImagesfromPost($row['id_post']);
		$url_img = $site->getSRC_img($id_post);
	}
?>
<!DOCTYPE html>
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo $row['title'];?>" />
<meta property="og:description" content="<?php echo $row['summary'];?>" />
<?php
	foreach ($url_img as &$value) {
	    	echo '<meta property="og:image" content="http://studio-sub.com/a3o/blog/'.$value.'"/>';
	}
?>
<meta property="og:url" content="<?php echo $url?>" />
<meta property="og:site_name" content="A3O GROUP" />

<title>Blog a3o</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!-- KICKSTART -->
<script src="admin/scripts/kickstart.js"></script> 
<link rel="stylesheet" href="admin/assets/css/kickstart.css" media="all" /> <!-- KICKSTART -->
<!-- Google web fonts -->
<link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel='stylesheet' />
<!--ICONS-->
<link rel="stylesheet" href="admin/assets/css/font-awesome.min.css">

<!--Style for the blog by SUB-->
<link rel="stylesheet" href="css/style-blog.css">
<link rel="stylesheet" href="css/style-post.css">	
</head>
<html>
<body>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '884455831604688',
      xfbml      : true,
      version    : 'v2.2'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<center>
	<table width="90%" class="header">
		<!--HEADER-->
		<tr height="50px">
			<th><img src="http://studio-sub.com/a3o/A3O_Logo.png" border="0"/></th>
			<th><a class="head_link" href="sobreA3O.html">Sobre A<sub>3</sub>O</a></th>
			<th><a class="head_link" href="nuestrosservicios.html">Nuestros Servicios</a></th>
			<th><a class="head_link" href="oxygio.html">Oxigio</a></th>
			<th><a class="head_link" href="index.html">Blog</a></th>
			<th><a class="head_link" href="contacto.html">Contacto</a></th>
			<th><a class="head_link" href="english.html">English</a>/<a class="head_link" href="spanish.html">Espa単ol</a></th>
			<th><a class="head_link" href="registro.html">Registro</a></th>
		</tr>
	</table>
	<div class="master-div">
	<table class="master-table">
<?php 
	include_once 'sidebar.php';
?>
<td class = "timeline" width="60%" valign="top">
	<div class="post_contenedor">
		<div class="post_contenido">
	<?php
		switch($row['id_category']){
			case '1': $cat = "<i>General</i>"; break;
			case '2': $cat = "<i>Headhunting</i>"; break;
			case '3': $cat = "<i>Staffing</i>"; break;
			case '4': $cat = "<i>Development</i>"; break;
			case '5': $cat = "<i>Finance</i>"; break;
		}
		$tipo_post = $row['type_post'];
		switch($tipo_post){
			case "1": 
		    #######################
		    #
		    #	POST IMAGEN Y TEXTO (1)
		    #
		    ####################
		    	echo "<a href='post.php?id=".$row["id_post"]."'>".$path."</a>";
		    	echo "
			    	<div class='post_contenedor_texto'>
			    		<table>
			    		<tr>
			    		<td>
						<div class='post_contenedor_titulo'>
							<h5><a href='post.php?id=".$row["id_post"]."'>".$row["title"]."</a></h5>
						</div>
					</td>
					<td>
						<div class='post_contenedor_categoria'>
							<p>".$cat."</p>
						</div>
					</td>
					</tr>
					</table>";
				echo "	<div class='post_text'>
						<p>".$row["summary"]."</p><p>".$row['body']."</p>";				
				echo "	</div>
				</div>";	//contender_texto
			break;
			case "2":
		    #######################
		    #
		    #	POST IMGEN (2)
		    #
		    ####################
	    		$path = $site->getImagesfromPost($row['id_post']);
	    		echo $path;
	    		echo "<div class='post_contenedor_texto'>";
	    		echo "	<table>
			    		<tr>
			    		<td>
						<div class='post_contenedor_titulo'>
							<h5><a href='post.php?id=".$row["id_post"]."'>".$row["title"]."</a></h5>
						</div>
					</td>
					<td>
						<div class='post_contenedor_categoria'>
							<p>".$cat."</p>
						</div>
					</td>
					</tr>
					</table>
				<div class='post_text'>
					<p>".$row["summary"]."</p>
				</div>";
			echo "</div>";//contender_texto
			break;
			case "4":
		    #######################
		    #
		    #	POST SURVEY (4)
		    #
		    ####################
		    	$path = $site->getImagesfromPost($row['id_post']);
		  	echo $path;
		    	$questions = $site->getAllQuestions($row['id_post']);
		    	echo "	<form action='include/submitSurvey.php' method='post' id='surveyPost'>
				<div class='post_contenedor_texto'>
					<table>
			    		<tr>
			    		<td>
						<div class='post_contenedor_titulo'>
							<h5><a href='post.php?id=".$row["id_post"]."'>".$row["title"]."</a></h5>
						</div>
					</td>
					<td>
						<div class='post_contenedor_categoria'>
							<p>".$cat."</p>
						</div>
					</td>
					
					</tr>
					</table>";
			echo "			<div class='post_text'><p>".$row["summary"]."</p></div>";
			echo "	<input name = 'id_post' type='hidden' value='".$row[id_post]."'>";
			$i=1;
			 foreach ($questions as $key => $value) {
            			echo "<div class = 'question-container_".$i."'>";
            			echo "<label>". $value['questionText'] . "</label><br/>";
            			echo "<input name='id_question[]' type = 'hidden' value = '".$value['id_question']."'>";
            			switch ($value['type_question']){
            				case "1":
            					echo "<input class='count".$i."' name='count_que".$i."' type = 'hidden' value = '1'>";
            					echo "<input type='text' name = 'ans[]'>";
            				break;
            				case "2":
            					$respuestas = $site->getAnswers($value['id_question']);
            					//print_r ($respuestas);
            					echo "<input class='count".$i."'name='count_que".$i."'' type = 'hidden' value = '".count($respuestas)."'>";
            					foreach ($respuestas as $key => $resp) {
            						echo "<input type='checkbox' name='ans[]' value = '".$resp['id_offer_answer']."'>" . $resp['answerText'] . "<br/>";
            					}
            				break;
            				case "3":
            					$respuestas = $site->getAnswers($value['id_question']);
            					echo "<input class='count".$i."'name='count_que".$i."' type = 'hidden' value = '".count($respuestas)."'>";
            					foreach ($respuestas as $key => $resp) {
            						echo "<input type='radio' name='ans[]' value = '".$resp['id_offer_answer']."'>" . $resp['answerText'] . "<br/>";
            					}
            				break;
            			}
            			echo "</div><br/>";	//question-container
            			$i++;
			}
			echo "<input type='hidden' class='totalQuestions' name='totalCiclo' value='".$i."'>";
			echo "<div class='submit-container'><input type='submit' value='Guardar' class='submitSurvey'></div>";
			echo "</div></form>";//contender_texto
			break;
			case "5":
		    #######################
		    #
		    #	POST VIDEO (5)
		    #
		    ####################
		    	echo "	<div class='post_contenedor_texto'>
					<table>
			    		<tr>
			    		<td>
						<div class='post_contenedor_titulo'>
							<h5><a href='post.php?id=".$row["id_post"]."'>".$row["title"]."</a></h5>
						</div>
					</td>
					<td>
						<div class='post_contenedor_categoria'>
							<p>".$cat."</p>
						</div>
					</td>
					</tr>
					</table>
						<iframe width='420' height='315' class='post_video' src='".$row["body"]."'></iframe>";
			echo "		<div class='post_text'><p>".$row['summary']."</p></div>";
			echo "	</div>";
			break;
			case "6":
		    #######################
		    #
		    #	POST FRASE (6)
		    #
		    ####################
		   	$path = $site->getImagesfromPost($row['id_post']);
		  	echo $path;
		     echo "<div class='post_contenedor_texto'>";
		     echo "	<table>
			    		<tr>
			    		<td>
						<div class='post_contenedor_titulo'>
							<h5><a href='post.php?id=".$row["id_post"]."'>".$row["title"]."</a></h5>
						</div>
					</td>
					<td>
						<div class='post_contenedor_categoria'>
							<p>".$cat."</p>
						</div>
					</td>
					</tr>
					</table>
				<div class='post_text'>
					<p>".$row["body"]."</p>
					<p style='font-style:italic;'>".$row["summary"]."</p>
				</div>";
		    echo "</div>";	//contender_texto
		    
		    break;
		    case "7": 
		    #######################
		    #
		    #	POST DOWNLOAD(7)
		    #
		    ####################
			$summary = $row["summary"];
		    	echo "
			    	<div class='contenedor_texto'>
				<div class='post_title'><h5><a href='post.php?id=".$row["id_post"]."'>".$row["title"]."</a></h5></div>";
				if (!$summary)	//hay resumen
				    	echo "<div class='post_text'><p>".$row['body'];
				else	//seguir leyendo
					echo "<div class='post_text'><p>".$summary;
			echo "</p></div></div>";	//contender_texto
			$path = $site->getFilesfromPost($row['id_post']);
		    	echo $path;
			break;
		}
		echo "</div>";	//post_contenido
	    	//contenedor de share tags
	echo "	<div class='container_footer_post'>";
	echo"		<div class='tags_container'>";
				$arr_hash = $site->selectHashs($row['id_post']);
				echo $arr_hash;
	echo "		</div>";
	echo "		<div class='share_container'>&nbsp&nbsp";
	
	/*			$mFacebook = "http://www.facebook.com/sharer.php?u=". $url;
				$mTwitter = "http://twitter.com/home?status=Compartiendo%20con%20A3O%20".$url."'";
				$mLinkedIn = "http://www.linkedin.com/shareArticle?mini=true&url=".$url."&title=A3O_Blog&summary=Compartiendo_A3O_en_LinkedIn";
		
	echo "	&nbsp&nbsp	<a target='_blank' href='".$mFacebook."'><img src='img/A3O_Piezas/RSS/A3OBlog_RRS_Fb.png' alt='Facebook'/></a>&nbsp";
	echo "	&nbsp&nbsp	<a target='_blank' href='".$mTwitter."'><img src='img/A3O_Piezas/RSS/A3OBlog_RRS_Tw.png' alt='Twitter'/></a>&nbsp";
	echo "	&nbsp&nbsp	<a target='_blank' href='".$mLinkedIn."'><img src='img/A3O_Piezas/RSS/A3OBlog_RRS_In.png' alt='LinkeIn'/></a>&nbsp";*/
	$short = $site->make_bitly_url($url,'json');
	echo '	<div class="fb-share-button" data-href="'.$url.'" data-layout="button_count" ></div>';
	echo '	&nbsp&nbsp	<a class="twitter-share-button" href="'.$short.'" data-url="'.$short.'" data-text="'.$row["title"].'" data-count="horizontal">Tweet</a>';
	echo '	
		<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>';
	echo '	<script type="IN/Share" data-url="'.$url.'" data-counter="right"></script>';
	echo "	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
				<input type='image' src='img/A3O_Piezas/RSS/A3OBlog_Ic_Print.png' alt='Imprimir Post' onClick='window.print()'/ > Imprimir
		&nbsp&nbsp&nbsp&nbsp	
				<input type='image' src='img/A3O_Piezas/RSS/A3OBlog_Ic_Mail.png' alt='Enviar Post' class='mailPost' /> Email
			</div>";
	?>
	</div>
	<div class="post-relacionados">
		<h6>Articulos Relacionados</h6>
		<?php
			$rel_post = $site->selectRelatedPost($id_post);
			echo $rel_post;
		?>
	</div>
	<hr class="alt1" />
	<div class="commentarios-face">
		<h6>Comentarios</h6>
		<!--#####
			#
			#	<div class="fb-comments" data-href="http://studio-sub.com/a3o/blog" data-numposts="5" data-colorscheme="light"></div>
			#
			#####-->
		<div class="fb-comments" data-href=<?=$url?> data-numposts="25" data-colorscheme="light"></div>
	</div>
	</div>	<!--post_contenedor-->
</td>
<?php
	include_once 'footer.php';
?>