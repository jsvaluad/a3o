<?PHP
require_once("./include/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("login.php");
    exit;
}

?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" charset="UTF-8">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">

<title>Blog A3O Admin</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="scripts/kickstart.js"></script> <!-- KICKSTART -->
<link rel="stylesheet" href="assets/css/kickstart.css" media="all" /> <!-- KICKSTART -->
<link rel="stylesheet" href="assets/css/style.css"/>
<!-- Google web fonts -->
<link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel='stylesheet' />
<!-- The main CSS file -->
<link href="assets/css/style-admin.css" rel="stylesheet" />
<!--ICONS-->
<link rel="stylesheet" href="assets/css/font-awesome.min.css">

<!--TEXT AREA-->
<link type="text/css" rel="stylesheet" href="assets/css/TextArea/jquery-te-1.4.0.css">

<!--FROALA-->
	 <!-- Include Font Awesome. -->
<link href="assets/froala/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- Include Editor style. -->
<link href="assets/froala/css/froala_editor.min.css" rel="stylesheet" type="text/css">
<link href="assets/froala/css/froala_style.min.css" rel="stylesheet" type="text/css">


</head>
<html>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=625166804248152&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<center>
	<table width="90%">
		<!--HEADER-->
		<tr height="50px">
			<th align="left" ><img src="http://studio-sub.com/a3o/A3O_Logo.png" border="0"/></th>
			<th align="right"><a href="logout.php">Log Out</a></th> 
		</tr>
	</table>
	<table width="90%">