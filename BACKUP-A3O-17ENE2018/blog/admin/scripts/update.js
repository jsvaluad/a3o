/*
	###############
	#
	#	ACTUALIZAR POST DESTACADOS
	#
	###############
*/

$(".update_destacados").click(function(){

	var len = $('[name="check_destacado[]"]:checked').length;
	if(len>3)
	{
		alert("Seleccione solo 3 artículos");
		event.preventDefault();
		return 0;
	}
});

/*
	###############
	#
	#	Borrar Archivos
	#
	###############
*/

$(".thrash_img").click(function(){
	var len = $('[name="check_destacado[]"]:checked').length;
	if(len == 0)
	{
		alert("Seleccione archivo a eliminar");
		event.preventDefault();
		return 0;
	}
});

$('.borrar-link').click(function(){
	if(!confirm("Está seguro que desea eliminar esta publicación?")){
		event.preventDefault();
		return 0;
	}
});



$(".search_img").click(function(){
	var str = $('.search_input').val();
	if(str.length == 0)
	{
		alert("Escriba palabra a buscar");
		event.preventDefault();
		return 0;
	}
});