	<tr>
		<th>COPYRIGHT</th>
	<tr>
</table>
</center>
	
	<script type="text/javascript" src="scripts/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="scripts/jquery.form.min.js"></script>
	<!--TEXT AREA-->
	<script type="text/javascript" src="scripts/TextArea/jquery-te-1.4.0.min.js" charset="utf-8"></script>
	<script>
		$('.jqte-test').jqte();
	</script>

	<!-- JavaScript Includes -->
	<script type="text/javascript" src="scripts/calls.js"></script>
	<!-- CREATE SURVEY-->
	<script type="text/javascript" src="scripts/survey.js"></script>
	<!--UPDATE POST-->
	<script type="text/javascript" src="scripts/update.js"></script>
	
	<script src="assets/froala/js/libs/jquery-1.11.1.min.js"></script>
	<script src="assets/froala/js/froala_editor.min.js"></script>
	  <!--[if lt IE 9]>
	    <script src="assets/froala/js/froala_editor_ie8.min.js"></script>
	  <![endif]-->
	  <!--<script src="assets/froala/js/plugins/tables.min.js"></script>-->
	  <script src="assets/froala/js/plugins/lists.min.js"></script>
	  <!--<script src="assets/froala/js/plugins/colors.min.js"></script>
	  <script src="assets/froala/js/plugins/media_manager.min.js"></script>-->
	  <script src="assets/froala/js/plugins/font_family.min.js"></script>
	  <script src="assets/froala/js/plugins/font_size.min.js"></script>
	  <script src="assets/froala/js/plugins/block_styles.min.js"></script>
	  <script src="assets/froala/js/plugins/file_upload.min.js"></script>
	
	  <script>
	      $(function(){
	          $('#edit')
	            .editable({
	            inlineMode: false,
	            buttons: ['undo', 'redo' , 'sep', 'bold', 'italic', 'underline', 'strikeThrough', "subscript", "superscript", "formatBlock", "blockStyle", "align", "insertOrderedList", "insertUnorderedList", "outdent", "indent",  "createLink", "insertImage", "removeFormat", "insertHorizontalRule"],
	            imageUploadURL: 'include/upload_image.php',
	            maxCharacters: 2000
	            });
	});
	 </script>
</script>
</body>
</html>