<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>Home</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/headhunting.css">	
	<link rel="stylesheet" href="css/lightbox.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>

<div class="videoContainer">
	<div id="videoBox">
		<h1>IDENTIFICAMOS Y SELECCIONAMOS AL TALENTO QUE TU EMPRESA NECESITA.</h1>
		<p>HAZ CLIC Y CONOCE MÁS</p>
		<a href="#" id="video" class="slvj-link-lightbox 2" data-videoid="eBjX-Wt9UvQ" data-videosite="youtube"><img class="videoPlay" src="img/videoPlay.png"></a><br>
	</div>
</div>

<div class="container" id="descriptionContainer">
	<div class="row">
		<div class="one-third column">
			<img src="img/HHicon.png" class="u-max-full-width"/>
		</div>
		<div class="two-thirds column">
			<h1>HEADHUNTING</h1>
			<p class="sectionDescription">Con Expertos especializados por áreas funcionales identificamos al candidato ideal a través de un proceso ágil y estructurado.</p>
			<p class="orangeText">Para apoyarte a <b>CREAR VALOR</b></p>
			<ul class="descriptionList">
				<li>Definimos juntos las características del candidato ideal.</li>
				<li>Realizamos la búsqueda del candidato a través de nuestra red de contactos, networking y por conocimiento de la industria.</li>
				<li>Evaluamos a nuestros candidatos con filtros y herramientas de selección.</li>
				<li>Entregables para el cliente para conocer el avance del proceso.</li>
				<li>Facilitamos la integración del candidato con un programa de acompañamiento.</li>
			</ul>
		</div>
	</div>
</div>
<div class="serviciosContainer">
	<div class="container" id="servicioBox">
		<h1>¿CÓMO TE APOYAMOS?</h1>
		<h2>SOLUCIONES <b>BACK OFFICE</b> | <b>HEADHUNTING</b></h2>
		<div class="row">
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>MANAGEMENT</b></p>	
					</div>
					<p class="servicioDescription">Reclutamiento Especializado para perfiles de gerencia media a alta dirección.</p>
				</div>
			</div>
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>TALENT</b></p>	
					</div>
					<p class="servicioDescription">Reclutamiento Especializado para perfiles de entrada a gerencias medias.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="socialBox">
	<div class="container">
		<div class="row">
			<div class="one-half column"><p>¿Conoces a alguien a quién podría interesarle?<br><b>COMPARTE ESTE SERVICIO</b></p></div>
			<div class="one-half column">
				<a href="https://www.facebook.com/A3O.mx/" target="_blank"><img src="img/socialF.png"/></a>
				<a href="https://mx.linkedin.com/company/a3o-group" target="_blank"><img src="img/socialL.png"/></a>
				<a href=" https://twitter.com/a3ogroup" target="_blank"><img src="img/socialT.png"/></a>
			</div>
		</div>
	</div>
</div>
<div class="container" id="modeloContainer">
	<div class="row">
		<div class="one-half column" id="modeloLeftContainer">
			<ul>
				<li><h2>ENTENDIENDO TUS EXPECTATIVAS</h2></li>
			</ul>
			<p class="greyText">PARA FACILITAR LA BÚSQUEDA Y ACERTAR CON EL CANDIDATO IDEAL</p>
			<p class="blackText">Te acompañamos en la Definición del Perfil que necesita su empresa basándonos en el <b>Modelo Matricial</b> que aplica tres factores clave:</p>
		</div>
		<div class="one-half column">
			<img src="img/HHPuntos.png" class="u-max-full-width"/>
		</div>
	</div>
</div>
<div class="culturaContainer">
	<div class="container">
		<h1>¿CÓMO LO HACEMOS?</h1>
		<h2>CON UNA <b>CULTURA DE TRABAJO ESTRUCTURADA</b></h2>
		<div class="row" id="culturaContainerRow">
			<div class="one-third column">
				<img src="img/HHasesoria.png"/>
				<div class="pContainer">
					<p>Diseñamos un perfil con base a tus requerimientos y respaldado por expertos.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/HHadministracion.png"/>
				<div class="pContainer">
					<p>Identificamos el talento preciso dentro de nuestra base de datos y/o red de contactos, acompañado con el conocimiento que tenemos de cada industria.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/HHatencion.png"/>
				<div class="pContainer">
					<p>Acompañamos al candidato selecionado a través de un programa de acompañamiento para su pronta integración en tu empresa.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactoContainer">
	<div class="container">
		<h1>SUMA TALENTO PARA TU EMPRESA</h1>
		<p>ESCRÍBENOS Y NOS PONDREMOS EN CONTACTO CONTIGO</p>
		<form class="formContacto">
			<label>Nombre</label>
			<input type="text" class="name"/>
			<label>Apellidos</label>
			<input type="text" class="apellidos"/>
			<label>Correo electrónico</label>
			<input type="text" class="email"/>
			<label>Teléfono</label>
			<input type="number" class="phone"/>			
			<label>¿Cómo podemos apoyarte?</label>
			<select class="service">
				<option value="0">Selecciona una opcion</option>
				<option value="HEADHUNTING" selected>HEADHUNTING</option>
				<option value="STAFF ADMINISTRATION">STAFF ADMINISTRATION</option>
				<option value="CHANGE MANAGEMENT">CHANGE MANAGEMENT</option>
				<option value="MIGRATION">MIGRATION</option>
				<option value="FINANCE">FINANCE</option>
			</select>
			<label>Mensaje</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>Acepto los<br><a target="_blank" href="A3O-Avisodeprivacidad_2017.pdf">términos y condiciones</a></p>
				<input type="submit" class="enviar" value="">
			</div>
			
		</form>
		<div class="error"></div>
	</div>
	<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/subir.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/service.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
<script type="text/javascript" src="script/googleAnalytics.js"></script>
</body>
</html>