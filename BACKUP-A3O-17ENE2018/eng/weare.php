<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>Home</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/nosotros.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>
<div class="bannerContainer">
	<img class="full" src="img/WeAre-Banner.png"/>
</div>
<div class="container" id="cultura">
	<h1>KNOW THE <b>A<span style="font-size:30px;">3</span>O CULTURE</b> </h1>
	<div class="row">
		<div class="one-third column"><img class="agente" src="img/WeAre-Advisory.png"/><p class="culturaText"><b>We understand your needs</b> to propose ideal solutions for your company.</p></div>
		<div class="one-third column"><img class="agente" src="img/WeAre-Admin.png"/><p class="culturaText">We apply the solution with <b>controlled processes and online tools</b> that guarantee the fulfillment of your operations.</p></div>
		<div class="one-third column"><img class="agente" src="img/WeAre-Attention.png"/><p class="culturaText">Experts accompany you at each stage to respond to your requests and <b>we will back you when you need them.</b></p></div>
	</div>
</div>
<div class="ventajasContainer">
<div class="container">
	<div class="row">
		<div class="one-half column" id="bullets">
			<h3>ADVANTAGES OF <b>A<span style="font-size:25px;">3</span>O AS STRATEGIC PARTNER</b></h3>
			<ul>
				<li><p><b>Structured Working Culture</b><br>Ensure a process of constant improvement in our services.</p></li>
				<li><p><b>Complete solution</b><br>Integrate the main activities of Human Resources and Finance.</p></li>
				<li><p><b>Expert support</b><br>Accompany you effectively.</p></li>
				<li><p><b>Adaptability</b><br>Structure solutions from small companies to multinational organizations.</p></li>
			</ul>
		</div>
		<div class="one-half column">
			<div id=spriteContainer>
			</div>
			<div class="gifContainer">
				<img src="img/pira/Piramide-giro(460x500)/Piramide-giro(460x500).gif" class="pira">
			</div>
		</div>
	</div>
</div>
</div>
<div class="misionContainer" id="mision">
	<div class="container">
		<p class="socioTitle">OUR <b>MISSION</b></p>
		<p class="socioText"><b>TO BE YOUR STRATEGIC PARTNER</b></p>
	</div><div class="subirBox"><div class="subirContainer"><img class="subir" src="img/UP-Buttom.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>-->
<script type="text/javascript" src="script/nosotros.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
</body>
</html>