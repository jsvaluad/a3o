<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>MIGRATION </title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/migration.css">	
	<link rel="stylesheet" href="css/lightbox.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>

<div class="videoContainer">
	<div id="videoBox">
		<h1>FACILITATE THE ENTRY OF FOREIGN STAFF IN YOUR COMPANY.</h1>
		<p>CLICK AND SEE MORE</p>
		<a href="#" id="video" class="slvj-link-lightbox 2" data-videoid="Cwonm4_-jZA" data-videosite="youtube"><img class="videoPlay" src="img/videoPlay.png"></a><br>
	</div>
</div>

<div class="container" id="descriptionContainer">
	<div class="row">
		<div class="one-third column">
			<img src="img/Micon.png" class="u-max-full-width"/>
		</div>
		<div class="two-thirds column">
			<h1>MIGRATION SERVICES</h1>
			<p class="sectionDescription">We assist your foreign staff in each stage of the immigration process through a process and assistance that will facilitate your stay.</p>
			<p class="orangeText">We provide support in the <b>CREATION OF VALUE</b></p>
			<ul class="descriptionList">
				<li>Ideal for hiring foreign personnel.</li>
				<li>Adaptive solution: individual or family.</li>
				<li>Personalized attention at each stage.</li>
				<li>Service of trust, security and confidentiality.</li>
				<li>Management of migratory, fiscal procedures for foreigners.</li>
			</ul>
		</div>
	</div>
</div>
<div class="serviciosContainer">
	<div class="container" id="servicioBox">
		<h1>HOW MAY WE HELP YOU?</h1>
		<h2 class="h2Web"><b>BACK OFFICE</b> SOLUTIONS | <b>MIGRATION</b></h2>
		<h2 class="h2Mobile"><br><b>BACK OFFICE</b> SOLUTIONS<br><b>MIGRATION</b></h2>
		<div class="row">
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>MIGRATORY PROCESSES</b></p>	
					</div>
					<p class="servicioDescription">We fully manage the immigration process of your foreign staff and their families to ensure your legal stay in Mexico.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>FISCAL PROCEEDINGS</b></p>	
					</div>
					<p class="servicioDescription">We manage the fiscal procedures to fulfill your tax obligations.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseRoja.png"/><p class="servicioTitle"><b>SPECIAL PROCEDURES</b></p>	
					</div>
					<p class="servicioDescription">Special procedures for formalities and requests for official documentation.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="socialBox">
	<div class="container">
		<div class="row">
			<div class="one-half column"><p>Do you know anyone who might be interested?<br><b>SHARE THIS SERVICE</b></p></div>
			<div class="one-half column">
				<a href="https://www.facebook.com/A3O.mx/" target="_blank"><img src="img/socialF.png"/></a>
				<a href="https://mx.linkedin.com/company/a3o-group" target="_blank"><img src="img/socialL.png"/></a>
				<a href=" https://twitter.com/a3ogroup" target="_blank"><img src="img/socialT.png"/></a>
			</div>
		</div>
	</div>
</div>
<div class="container" id="modeloContainer">
	<div class="row">
		<div class="one-half column" id="modeloLeftContainer">
			<ul>
				<li><h2>BACK UP AT EVERY TIME</h2></li>
			</ul>
			<p class="blackText">For your foreign staff, from the first contact to know your requirements, migratory, until you complete your paperwork.</b></p>
		</div>
		<div class="one-half column">
			<img src="img/Mpuntos.png" class="u-max-full-width"/>
		</div>
	</div>
</div>
<div class="culturaContainer">
	<div class="container">
		<h1>HOW DO WE DO IT?</h1>
		<h2>WITH A <b>STRUCTURED WORK CULTURE</b></h2>
		<div class="row" id="culturaContainerRow">
			<div class="one-third column">
				<img src="img/Advisory-How.png"/>
				<div class="pContainer">
					<p>An expert guides you based on the regulations of the immigration procedures you need.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/Administration-How.png"/>
				<div class="pContainer">
					<p>We manage your procedures before the corresponding authorities and we inform you of the progress until its term.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/Attention-How.png"/>
				<div class="pContainer">
					<p>We provide advance tracking of renewal requests and documentation updates.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactoContainer">
	<div class="container">
		<h1>RELEASE OF FORMALITIES </h1>
		<p>WRITE US AND WE WILL CONTACT YOU</p>
		<form class="formContacto">
			<label>Name</label>
			<input type="text" class="name"/>
			<label>Lastname</label>
			<input type="text" class="apellidos"/>
			<label>Email</label>
			<input type="text" class="email"/>
			<label>Phone number</label>
			<input type="number" class="phone"/>			
			<label>How we may help you?</label>
			<select class="service">
				<option value="0">Select an option</option>
				<option value="HEADHUNTING">HEADHUNTING</option>
				<option value="STAFF ADMINISTRATION">STAFF ADMINISTRATION</option>
				<option value="CHANGE MANAGEMENT">CHANGE MANAGEMENT</option>
				<option value="MIGRATION" selected>MIGRATION</option>
				<option value="FINANCE">FINANCE</option>
			</select>
			<label>Message</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>I agree to the<br><a target="_blank" href="../A3O-Avisodeprivacidad_2017.pdf">terms and conditions</a></p>
				<input type="submit" class="enviar" value="SEND">
			</div>
		</form>
		<div class="error"></div>
	</div>
	<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/UP-Buttom.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/service.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
</body>
</html>