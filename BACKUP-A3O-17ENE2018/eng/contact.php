<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>CONTACTO</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/contacto.css">	
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>
	<div class="contactoContainer">
		<h1>CONTACT US</h1>
		<div class="contactoBox">
			<p><b>Start creating value for your business. If you have interest or doubts in any of our solutions, write us and one of our experts will contact you.</b> </p>
		</div>
		<form class="formContacto">
			
			<label>Name</label>
			<input type="text" class="name"/>
			<label>Lastname</label>
			<input type="text" class="apellidos"/>
			<label>Email</label>
			<input type="text" class="email"/>			
			<label>Status</label>
			<select class="status">
				<option value="0">Select an option</option>
				<option value="PERSONA">CANDIDATE</option>
				<option value="EMPRESA">COMPANY</option>
			</select>
			<label>Message</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>I agree to the<br><a target="_blank" href="../A3O-Avisodeprivacidad_2017.pdf">terms and conditions</a></p>
				<input type="submit" class="enviar" value="SEND">
			</div>
			
		</form>
		<div class="error"></div>
	</div>
	<div class="container" id="map">
		<div class="row">
			<div class="one-half column">
				<h1>LOCATION</h1>
				<p>Álvaro Obregón 121-11,<br>Roma Norte, CDMX<br>TEL (55) 4172 0660<br><a href="mailto:contacto@a3ogroup.mx">contacto@a3o.mx</a></p>
			</div>
			<div class="one-half column">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3762.8580890267704!2d-99.16264158564509!3d19.418536586892298!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1ff3a438f8ac3%3A0xeae8bc7df41a3810!2sAv.+%C3%81lvaro+Obreg%C3%B3n+121%2C+Roma+Nte.%2C+06700+Ciudad+de+M%C3%A9xico%2C+D.F.!5e0!3m2!1sen!2smx!4v1478662148414" width="560" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
		
		<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/UP-Buttom.png"/></div></div>
	</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/contacto.js"></script>
</body>
</html>