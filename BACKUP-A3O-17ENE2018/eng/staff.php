<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>STAFF ADMINISTRATION</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/staff.css">	
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>
<div class="blankSpace">&nbsp;</div>
<div class="videoContainer">
	<div id="videoBox">
		<h1>WE FACILITATE THE PROCESSES OF ADMINISTRATION OF PERSONNEL AND PAYROLL FOR YOUR COMPANY </h1>
	</div>
</div>

<div id="descriptionContainer">
	<div class="row">
		<div class="one-third column">
			<img src="img/Sicon.png" class="u-max-full-width"/>
		</div>
		<div class="two-thirds column">
			<h1>STAFF ADMINISTRATION</h1>
			<p class="sectionDescription">We guarantee the control and attention of your personnel to reduce risks and labor contingencies.</p>
			<p class="orangeText">We provide support in the <b>CREATION OF VALUE</b></p>
			<ul class="descriptionList">
				<li>Eliminate payroll errors, contingencies and occupational hazards.</li>
				<li>Support of experts specialized in IMSS-Payroll and HR.</li>
				<li>Advisory and Attention to Company and Employees.</li>
				<li>Information online and incidences of staff 24 hours a day.</li>
				<li>Implementation of Service Agreements at each stage.</li>
				<li>We create a customized solution to your needs.</li>
			</ul>
		</div>
	</div>
</div>
<div class="serviciosContainer">
	<div class="container" id="servicioBox">
		<h1>HOW MAY WE HELP YOU?</h1>
		<h2 class="h2Web"><b>BACK OFFICE</b> SOLUTIONS | <b>STAFF ADMINISTRATION</b></h2>
		<h2 class="h2Mobile">SOLUCIONES<br><b>BACK OFFICE</b><br><b>STAFF ADMINISTRATION</b></h2>
		<div class="row">
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>STAFF ADMINISTRATION</b></p>	
					</div>
					<p class="servicioDescription">We hire the human capital you need to fulfill your employer responsibilities.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>PAYROLL ADMINISTRATION</b></p>	
					</div>
					<p class="servicioDescription">We process your company&#39;s payroll with accuracy and security for you and your staff within the regulatory framework.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseRoja.png"/><p class="servicioTitle"><b>STAFFING</b></p>	
					</div>
					<p class="servicioDescription">We recruit and manage personnel for your company according to your strategic needs.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="socialBox">
	<div class="container">
		<div class="row">
			<div class="one-half column"><p>Do you know anyone who might be interested?<br><b>SHARE THIS SERVICE</b></p></div>
			<div class="one-half column">
				<a href="https://www.facebook.com/A3O.mx/" target="_blank"><img src="img/socialF.png"/></a>
				<a href="https://mx.linkedin.com/company/a3o-group" target="_blank"><img src="img/socialL.png"/></a>
				<a href=" https://twitter.com/a3ogroup" target="_blank"><img src="img/socialT.png"/></a>
			</div>
		</div>
	</div>
</div>
<div class="container" id="modeloContainer">
	<div class="row">
		<div class="one-half column" id="modeloLeftContainer">
			<ul>
				<li><h2>SUPPORTING YOUR EMPLOYMENT OBLIGATIONS</h2></li>
			</ul>
			<p class="greyText">BASED ON A MULTIDIMENSIONAL MODEL </p>
			<p class="blackText"><b>We create a customized solution</b> for the management of your human capital.</p>
		</div>
		<div class="one-half column">
			<img src="img/STAFF-Matrix-model.png" class="u-max-full-width"/>
		</div>
	</div>
</div>
<div class="culturaContainer">
	<div class="container">
		<h1>HOW DO WE DO IT?</h1>
		<h2>WITH A <b>STRUCTURED WORK CULTURE </b></h2>
		<div class="row" id="culturaContainerRow">
			<div class="one-third column">
				<img src="img/Advisory-How.png"/>
				<div class="pContainer">
					<p>We support you in structuring a solution to facilitate the administration of personnel and payroll to fulfill your strategic needs.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/Administration-How.png"/>
				<div class="pContainer">
					<p>We accompany you with efficient processes and accessible online tools for the improvement of your area of human resources.</p>
				</div>
			</div>	
			<div class="one-third column">
				<img src="img/Attention-How.png"/>
				<div class="pContainer">
					<p>We take care of you in a continuous and personalized way in your requests and those of your staff..</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactoContainer">
	<div class="container">
		<h1>PAYROLL WITHOUT SETBACKS AND STAFF WITHOUT RISKS </h1>
		<p>WRITE US AND WE WILL CONTACT YOU</p>
		<form class="formContacto">
			<label>Name</label>
			<input type="text" class="name"/>
			<label>Lastname</label>
			<input type="text" class="apellidos"/>
			<label>Email</label>
			<input type="text" class="email"/>
			<label>Phone number</label>
			<input type="number" class="phone"/>			
			<label>How we may help you?</label>
			<select class="service">
				<option value="0">Select an option</option>
				<option value="HEADHUNTING">HEADHUNTING</option>
				<option value="STAFF ADMINISTRATION" selected>STAFF ADMINISTRATION</option>
				<option value="CHANGE MANAGEMENT">CHANGE MANAGEMENT</option>
				<option value="MIGRATION" >MIGRATION</option>
				<option value="FINANCE">FINANCE</option>
			</select>
			<label>Message</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>I agree to the<br><a target="_blank" href="A3O-Avisodeprivacidad_2017.pdf">terms and conditions</a></p>
				<input type="submit" class="enviar" value="SEND">
			</div>
			
		</form>
		<div class="error"></div>
	</div>
	<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/UP-Buttom.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
  <script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/service.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
</body>
</html>