<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>FINANCE</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/finance.css">	
	<link rel="stylesheet" href="css/lightbox.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>

<div class="videoContainer">
	<div id="videoBox">
		<h1>WITH A STRATEGIC PARTNER.</h1>
		<h2>NOW TAKING FINANCIAL DECISIONS IS SIMPLER</h2>
		<p>CLICK AND SEE MORE</p>
		<a href="#" id="video" class="slvj-link-lightbox 2" data-videoid="Ntp5ifzXH1k" data-videosite="youtube"><img class="videoPlay" src="img/videoPlay.png"></a><br>
	</div>
</div>

<div class="container" id="descriptionContainer">
	<div class="row">
		<div class="one-third column">
			<img src="img/Ficon.png" class="u-max-full-width"/>
		</div>
		<div class="two-thirds column">
			<h1>FINANCE</h1>
			<p class="sectionDescription">We accompany you in the financial administration of your company with the support of experts who make in a timely manner the fulfillment of your financial obligations.</p>
			<p class="orangeText">We provide support in the <b>CREATION OF VALUE</b></p>
			<ul class="descriptionList">
				<li>Structuring of Accounting Scheme.</li>
				<li>Preparation and analysis of accounting information.</li>
				<li>Tax Consulting and Structuring.</li>
				<li>Elaboration of Tax Declarations.</li>
				<li>Elaboration of Financial KPIs.</li>
			</ul>
		</div>
	</div>
</div>
<div class="serviciosContainer">
	<div class="container" id="servicioBox">
		<h1>HOW MAY WE HELP YOU?</h1>
		<h2 class="h2Web"><b>BACK OFFICE</b> SOLUTIONS | <b>FINANCE</b></h2>
		<h2 class="h2Mobile"><br><b>BACK OFFICE</b> SOLUTIONS<br><b>FINANCE</b></h2>
		<div class="row">
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>ACCOUNTING</b></p>	
					</div>
					<p class="servicioDescription">We elaborate and analyze your accounting information that will allow you to understand the current financial state of your company.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>TAXES</b></p>	
					</div>
					<p class="servicioDescription">We advise on tax matters, calculate and present your taxes for the timely fulfillment of your tax obligations.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseRoja.png"/><p class="servicioTitle"><b>FINANCIAL ADMINISTRATION</b></p>	
					</div>
					<p class="servicioDescription">We analyze your financial management through indicators and online tools to evaluate the performance of your company.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="socialBox">
	<div class="container">
		<div class="row">
			<div class="one-half column"><p>Do you know anyone who might be interested?<br><b>SHARE THIS SERVICE</b></p></div>
			<div class="one-half column">
				<a href="https://www.facebook.com/A3O.mx/" target="_blank"><img src="img/socialF.png"/></a>
				<a href="https://mx.linkedin.com/company/a3o-group" target="_blank"><img src="img/socialL.png"/></a>
				<a href=" https://twitter.com/a3ogroup" target="_blank"><img src="img/socialT.png"/></a>
			</div>
		</div>
	</div>
</div>
<div class="container" id="modeloContainer">
	<div class="row">
		<div class="one-half column" id="modeloLeftContainer">
			<ul>
				<li><h2>MAKING EASIER YOUR FINANCIAL DECISIONS<br><span>WITH ASSERTIVE INFORMATION</span></h2></li>
			</ul>
			<p class="greyText">WE SUPPORT THE FINANCIAL PERFORMANCE WITH THE PRINCIPLE:<br><b>"MEASURE TO IMPROVE"</b></p>
		</div>
		<div class="one-half column">
			<img src="img/Finance-Matrix-Model.png" class="u-max-full-width"/>
		</div>
	</div>
</div>
<div class="culturaContainer">
	<div class="container">
		<h1>HOW DO WE DO IT?</h1>
		<h2>WITH A <b>STRUCTURED WORK CULTURE</b></h2>
		<div class="row" id="culturaContainerRow">
			<div class="one-third column">
				<img src="img/Advisory-How.png"/>
				<div class="pContainer">
					<p>We guide you to correctly fulfill your financial obligations through a solution adapted to your needs.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/Administration-How.png"/>
				<div class="pContainer">
					<p>We process and analyze your accounting-fiscal and financial information with efficient processes, controlled and according to your requirements.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/Attention-How.png"/>
				<div class="pContainer">
					<p>We support you in financial interpretation, clarify your accounting queries, and inform you of fiscal updates.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactoContainer">
	<div class="container">
		<h1>CATCH UP ON YOUR FINANCES </h1>
		<p>WRITE US AND WE WILL CONTACT YOU</p>
		<form class="formContacto">
			<label>Name</label>
			<input type="text" class="name"/>
			<label>Lastname</label>
			<input type="text" class="apellidos"/>
			<label>Email</label>
			<input type="text" class="email"/>
			<label>Phone number</label>
			<input type="number" class="phone"/>			
			<label>How we may help you?</label>
			<select class="service">
				<option value="0">Select an option</option>
				<option value="HEADHUNTING">HEADHUNTING</option>
				<option value="STAFF ADMINISTRATION">STAFF ADMINISTRATION</option>
				<option value="CHANGE MANAGEMENT">CHANGE MANAGEMENT</option>
				<option value="MIGRATION">MIGRATION</option>
				<option value="FINANCE" selected>FINANCE</option>
			</select>
			<label>Message</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>I agree to the<br><a target="_blank" href="../A3O-Avisodeprivacidad_2017.pdf">terms and conditions</a></p>
				<input type="submit" class="enviar" value="SEND">
			</div>
			
		</form>
		<div class="error"></div>
	</div>
	<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/subir.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/service.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
</body>
</html>