<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>Home</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/index.css">	
	<link rel="stylesheet" href="css/lightbox.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>

<div class="videoContainer">
	<div id="videoBox">
		<h1>WE ARE THE STRATEGIC PARTNER</h1>
		<h4>THAT SUPPORTS YOU TO <b>CREATE VALUE</b> IN YOUR BUSINESS</h4>
		<p>CLICK AND SEE MORE</p>
		<a href="#" id="video" class="slvj-link-lightbox 2" data-videoid="s8PWTyZQNxg" data-videosite="youtube"><img class="videoPlay" src="img/videoPlay.png"></a><br>
	</div>
</div><div class="socioContainer"><div class="socioBox">
		<p class="socioTitle">A<span>3</span>O <b>IS YOUR STRATEGIC PARTNER</b></p>
		<p class="socioText">AS EXPERTS, WE <b>ACCOMPANY</b> YOU TO IMPROVE YOUR AREAS OF <b>HUMAN RESOURCES AND FINANCE</b> WITH <b>BACK OFFICE SOLUTIONS</b></p>
	</div>
</div>
<div class="container" id="solucionesContainer">
	<div class="row"><p class="solucionesTitle"><b>BACK OFFICE</b> SOLUTIONS</p></div>
	<div class="row">
		<div class="one-third column">
			<img src="img/headhuntingIcon.png" class="icon">
			<div class="iconContainer">
				<div class="titleContainer">
					<p class="iconTitle">HEADHUNTING</p>
				</div>
				<div class="descContainer">
					<p class="iconDesc">Attracts the ideal talent to be part of your business.</p>
				</div>
				<div class="linkContainer">
					<a class="linkIcon" href="headhunting.php">READ MORE</a>
				</div>
			</div>
		</div>
		<div class="one-third column">
			<img src="img/staffIcon.png" class="icon">
			<div class="iconContainer">
				<div class="titleContainer">
					<p class="iconTitle">STAFF ADMINISTRATION</p>
				</div>
				<div class="descContainer">
					<p class="iconDesc">Controls the processes of personnel administration and payroll efficiently, to protect the risk and labor contingencies.</p>
				</div>
				<div class="linkContainer">
					<a class="linkIcon" href="staff.php">READ MORE</a>
				</div>
			</div>	
		</div>
		<div class="one-third column">
			<img src="img/changeIcon.png" class="icon">
			<div class="iconContainer">
				<div class="titleContainer">
					<p class="iconTitle">CHANGE MANAGEMENT</p>
				</div>
				<div class="descContainer">
					<p class="iconDesc">Drives your company and work team to achieve organizational development.</p>
				</div>
				<div class="linkContainer">
					<a class="linkIcon" href="change.php">READ MORE</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row" id="secondRow">
		<div class="one-half column">
			<img src="img/migrationIcon.png" class="icon">
			<div class="iconContainer">
				<div class="titleContainer">
					<p class="iconTitle">MIGRATION</p>
				</div>
				<div class="descContainer">
					<p class="iconDesc">Manages the immigration procedures for the legal stay of the foreign personnel that is part of your company.</p>
				</div>
				<div class="linkContainer">
					<a class="linkIcon" href="migration.php">READ MORE</a>
				</div>
			</div>
		</div>
		<div class="one-half column">
			<img src="img/financeIcon.png" class="icon">
			<div class="iconContainer">
				<div class="titleContainer">
					<p class="iconTitle">FINANCE</p>
				</div>
				<div class="descContainer">
					<p class="iconDesc">Provides reliable and accurate information for better financial decision making.</p>
				</div>
				<div class="linkContainer">
					<a class="linkIcon" href="finance.php">READ MORE</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="culturaBackground">
	<div class="culturaContainer">
		<img id="icoGraph" class="admin" src="img/Admin.png"/>
		<img id="icoGraph" class="atencion" src="img/Attention.png"/>
		<img id="icoGraph" class="asesoria" src="img/Advisory.png"/>
		<img class="rueda" src="img/A-highly-structured.png" alt="rueda"/>
		<div class="subirContainer"><img class="subir" src="img/UP-Buttom.png"/></div>
	</div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/index.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
</body>
</html>