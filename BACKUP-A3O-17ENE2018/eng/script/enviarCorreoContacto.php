<?php
	$to = "contacto@a3ogroup.mx";
	$persona = $_POST['name']." ".$_POST['apellidos'];
	$email = $_POST['email'];
	$status = $_POST['status'];
	$mensaje = $_POST['mensaje'];
	
	$subject = 'Correo desde CONTACTO';
	$message = '<table><tr><td>Correo desde a3ogroup.com/eng</td></tr>';
	$message .= '<tr><td>Datos de la persona que envio el correo'.'</td></tr>';
	$message .= '<tr><td>Nombre Completo: '.$persona.'</td></tr>';
	$message .= '<tr><td>Correo: ' . $email.'</td></tr>';
	$message .= '<tr><td>Status: ' . $status.'</td></tr>';
	$message .= '<tr><td>Mensaje: ' . $mensaje.'</td></tr>';
	$message .= '<tr><td>Enviado el ' .date('d/m/Y', time()).'</td></tr></table>';
	
	$headers = "From:" . $email."\r\n";
	$headers.="X-Mailer: PHP/".phpversion()." \r\n";
	$headers.="Mime-Version: 1.0 \r\n";
	$headers.='Content-Type: text/html; charset=ISO-8859-1'."\r\n";
	
	if(mail($to, $subject, $message , $headers)){
		header("Content-Type: application/json; charset=utf-8", true);
		echo json_encode("Correo Enviado");
	}
	else{
		header("Content-Type: application/json; charset=utf-8", true);
		echo json_encode("Correo No Enviado");
	}
	
?>