/*
|-------------------------------------------
|	Simple Lightbox Video jQuery
|-------------------------------------------
|
|
|	Author: Pedro Marcelo
|	Author URI: https://github.com/pedromarcelojava
|	Plugin URI: https://github.com/pedromarcelojava/Simple-Lightbox-Video-jQuery
|	Version: 2.0
*/

(function($)
{
	$.extend($.fn, {
		simpleLightboxVideo : function()
		{
			var defaults = {
				delayAnimation: 300,
				keyCodeClose: 27
			};

			$.simpleLightboxVideo.vars = $.extend({}, defaults);

			var video = this;

			video.click(function()
			{
				if (window.innerHeight > 801)
				{
					//var margintop = (window.innerHeight - 540) / 2;
					var marginTop = 0;
				}
				else
				{
					var margintop = 0;
				}
				height = window.innerHeight;
				width = window.innerWidth - 100;
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
					var ifr = '<iframe src="" width="300" height="173" id="slvj-video-embed" style="border:0;"></iframe>';
					var back = '<div id="slvj-back-lightbox">';
				}
				else{
					//var ifr = '<iframe src="" width="640" height="480" id="slvj-video-embed" style="border:0;"></iframe>';
					vWidth =  window.innerWidth - 160;
					vHeight = window.innerHeight - 60;
					console.log(width);
					console.log(height);
					var ifr = '<iframe src="" width="'+vWidth+'" height="'+vHeight+'" id="slvj-video-embed" style="border:0;"></iframe>';
					var back = '<div id="slvj-back-lightbox" style="width:'+width+'px;height:'+height+'px">';
				}
				var close = '<div id="slvj-close-icon"></div>';
				var lightbox = '<div class="slvj-lightbox" style="margin-top:' + marginTop + 'px">';
				var bclo = '<div id="slvj-background-close"></div>';
				var win = '<div id="slvj-window">';
				var end = '</div></div></div>';

				$('body').append(win + bclo + back + lightbox + close + ifr + end);
				$('#slvj-window').hide();

				if ($(this).data('videosite') == "youtube") {
					var url = 'http://www.youtube.com/embed/' + $(this).data('videoid') + '?autoplay=1';
				} else if($(this).data('videosite') == "vimeo") {
					var url = 'http://player.vimeo.com/video/' + $(this).data('videoid') + '?autoplay=1';
				}

				$('#slvj-window').fadeIn();
				$('#slvj-video-embed').attr('src', url);

				$('#slvj-close-icon').click(function()
				{
					$('#slvj-window').fadeOut($.simpleLightboxVideo.vars.delayAnimation, function()
					{
						$(this).remove();
					});
				});

				$('#slvj-background-close').click(function()
				{
					$('#slvj-window').fadeOut($.simpleLightboxVideo.vars.delayAnimation, function()
					{
						$(this).remove();
					});
				});

				return false;
			});

			$(document).keyup(function(e)
			{
				if (e.keyCode == 27)
				{
					$('#slvj-window').fadeOut($.simpleLightboxVideo.vars.delayAnimation, function()
					{
						$(this).remove();
					});
				}
			});
			
			$(window).resize(function()
			{
				/*if (window.innerHeight > 540)
				{
					var margintop = (window.innerHeight - 540) / 2;
				}
				else
				{
					var margintop = 0;
				}*/
				var margintop = 0;
				height = window.innerHeight;
				width = window.innerWidth - 100;
				vWidth =  window.innerWidth - 160;
				vHeight = window.innerHeight - 60;
				$('#slvj-back-lightbox').css({
					width:width,
					height:height
				});
				$("#slvj-video-embed").css({
					width:vWidth,
					height:vHeight
				});
				
			});

			return false;
		}
	});
})(jQuery);

(function($)
{
	$.simpleLightboxVideo = function(options, video)
	{
		return $(video).simpleLightboxVideo();
	}
})(jQuery);