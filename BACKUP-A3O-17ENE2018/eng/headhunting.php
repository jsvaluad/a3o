<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>Home</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/headhunting.css">	
	<link rel="stylesheet" href="css/lightbox.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>

<div class="videoContainer">
	<div id="videoBox">
		<h1>WE IDENTIFY AND SELECT THE TALENT YOUR COMPANY NEEDS</h1>
		<p>CLICK AND SEE MORE</p>
		<a href="#" id="video" class="slvj-link-lightbox 2" data-videoid="eBjX-Wt9UvQ" data-videosite="youtube"><img class="videoPlay" src="img/videoPlay.png"></a><br>
	</div>
</div>

<div class="container" id="descriptionContainer">
	<div class="row">
		<div class="one-third column">
			<img src="img/HHicon.png" class="u-max-full-width"/>
		</div>
		<div class="two-thirds column">
			<h1>HEADHUNTING</h1>
			<p class="sectionDescription">With specialized experts by functional areas we identify the ideal candidate through an agile and structured process.</p>
			<p class="orangeText">We provide support in the <b>CREATION OF VALUE</b></p>
			<ul class="descriptionList">
				<li>Together we define the characteristics of the ideal candidate.</li>
				<li>We search the candidate through our network of contacts, networking and industry knowledge.</li>
				<li>We evaluate our candidates with filters and selection tools.</li>
				<li>Deliverables for the client to know the progress of the process.</li>
				<li>We facilitate the assimilation of the candidate with an accompanying program.</li>
			</ul>
		</div>
	</div>
</div>
<div class="serviciosContainer">
	<div class="container" id="servicioBox">
		<h1>HOW MAY WE HELP YOU?</h1>
		<h2><b>BACK OFFICE</b> SOLUTIONS | <b>HEADHUNTING</b></h2>
		<div class="row">
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>MANAGEMENT</b></p>	
					</div>
					<p class="servicioDescription">Specialized Recruitment for medium to senior management profiles.</p>
				</div>
			</div>
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>TALENT</b></p>	
					</div>
					<p class="servicioDescription">Specialized Recruitment for entry profiles to middle management.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="socialBox">
	<div class="container">
		<div class="row">
			<div class="one-half column"><p>Do you know anyone who might be interested?<br><b>SHARE THIS SERVICE</b></p></div>
			<div class="one-half column">
				<a href="https://www.facebook.com/A3O.mx/" target="_blank"><img src="img/socialF.png"/></a>
				<a href="https://mx.linkedin.com/company/a3o-group" target="_blank"><img src="img/socialL.png"/></a>
				<a href=" https://twitter.com/a3ogroup" target="_blank"><img src="img/socialT.png"/></a>
			</div>
		</div>
	</div>
</div>
<div class="container" id="modeloContainer">
	<div class="row">
		<div class="one-half column" id="modeloLeftContainer">
			<ul>
				<li><h2>UNDERSTANDING YOUR EXPECTATIONS</h2></li>
			</ul>
			<p class="greyText">TO FACILITATE THE SEARCH AND TO ACQUIRE WITH THE IDEAL CANDIDATE.</p>
			<p class="blackText">We accompany you in the Definition of the Profile that your company needs based on the <b>Matrix Model</b> that applies three key factors:</p>
		</div>
		<div class="one-half column">
			<img src="img/HH-Matrix-Model.png" class="u-max-full-width"/>
		</div>
	</div>
</div>
<div class="culturaContainer">
	<div class="container">
		<h1>HOW DO WE DO IT?</h1>
		<h2>WITH A <b>STRUCTURED WORK CULTURE</b></h2>
		<div class="row" id="culturaContainerRow">
			<div class="one-third column">
				<img src="img/Advisory-How.png"/>
				<div class="pContainer">
					<p>We design a profile based on your requirements.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/Administration-How.png"/>
				<div class="pContainer">
					<p>We identify the precise talent within our database and / or network of contacts, accompanied by the knowledge we have of each industry.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/Attention-How.png"/>
				<div class="pContainer">
					<p>We accompany the selected candidate through an accompanying program for their early integration into your company.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactoContainer">
	<div class="container">
		<h1>JOIN TALENT FOR YOUR COMPANY</h1>
		<p>WRITE US AND WE WILL CONTACT YOU</p>
		<form class="formContacto">
			<label>Name</label>
			<input type="text" class="name"/>
			<label>Lastname</label>
			<input type="text" class="apellidos"/>
			<label>Email</label>
			<input type="text" class="email"/>
			<label>Phone Number</label>
			<input type="number" class="phone"/>			
			<label>How we may help you?</label>
			<select class="service">
				<option value="0">Select an option/option>
				<option value="HEADHUNTING" selected>HEADHUNTING</option>
				<option value="STAFF ADMINISTRATION">STAFF ADMINISTRATION</option>
				<option value="CHANGE MANAGEMENT">CHANGE MANAGEMENT</option>
				<option value="MIGRATION">MIGRATION</option>
				<option value="FINANCE">FINANCE</option>
			</select>
			<label>Message</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>I agree to the <br><a target="_blank" href="../A3O-Avisodeprivacidad_2017.pdf">terms and conditions</a></p>
				<input type="submit" class="enviar" value="SEND">
			</div>
			
		</form>
		<div class="error"></div>
	</div>
	<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/UP-Buttom.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/service.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
</body>
</html>