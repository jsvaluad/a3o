<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>CHANGE MANAGEMENT</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/change.css">	
	<link rel="stylesheet" href="css/lightbox.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>
<div class="videoContainer">
	<div id="videoBox">
		<h1>IMPULSE THE CHANGE IN YOUR COMPANY</h1>
		<!--<p>HAZ CLIC Y CONOCE MÁS</p>
		<a href="#" id="video" class="slvj-link-lightbox 2" data-videoid="Ur7NgsD4Pmc" data-videosite="youtube"><img class="videoPlay" src="img/videoPlay.png"></a><br>-->
	</div>
</div>

<div class="container" id="descriptionContainer">
	<div class="row">
		<div class="one-third column">
			<img src="img/CMicon.png" class="u-max-full-width"/>
		</div>
		<div class="two-thirds column">
			<h1>CHANGE MANAGEMENT</h1>
			<p class="sectionDescription">We implement solutions with tangible results for the development of human capital and process optimization, with a structured methodology.</p>
			<p class="orangeText">We provide support in the <b>CREATION OF VALUE</b></p>
			<ul class="descriptionList">
				<li>Improve the performance of human capital.</li>
				<li>Optimizes internal processes.</li>
				<li>Creates a favorable organizational culture.</li>
				<li>Facilitates the transition to change.</li>
				<li>Align the objectives with the strategic vision of your company.</li>
			</ul>
		</div>
	</div>
</div>
<div class="serviciosContainer">
	<div class="container" id="servicioBox">
		<h1>HOW MAY WE HELP YOU?</h1>
		<h2 class="h2Web"><b>BACK OFFICE</b> SOLUTIONS | <b>CHANGE MANAGEMENT</b></h2>
		<h2 class="h2Mobile">SOLUCIONES<br><b>BACK OFFICE</b><br><b>CHANGE MANAGEMENT</b></h2>
		<div class="row">
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>SYSTEMIC ADMINISTRATION OF CHANGE</b></p>	
					</div>
					<p class="servicioDescription">We design and implement the accompanying process to facilitate the transition of change.</p>
				</div>
				
			</div>
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>ORGANIZATIONAL MATURITY</b></p>	
					</div>
					<p class="servicioDescription">From diagnosis, we determine the actions your company requires to improve organizational processes.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>ORGANIZATIONAL SYSTEMIC COACHING &reg;</b></p>	
					</div>
					<p class="servicioDescription">Individual or team accompaniment to maximize the professional and personal development of your human capital.</p>
				</div>
			</div>
			<div class="one-half column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>TRAINING</b></p>	
					</div>
					<p class="servicioDescription">We designed tailor-made courses to improve professional and personal skills and performance.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="socialBox">
	<div class="container">
		<div class="row">
			<div class="one-half column"><p>Do you know anyone who might be interested?<br><b>SHARE THIS SERVICE</b></p></div>
			<div class="one-half column">
				<a href="https://www.facebook.com/A3O.mx/" target="_blank"><img src="img/socialF.png"/></a>
				<a href="https://mx.linkedin.com/company/a3o-group" target="_blank"><img src="img/socialL.png"/></a>
				<a href=" https://twitter.com/a3ogroup" target="_blank"><img src="img/socialT.png"/></a>
			</div>
		</div>
	</div>
</div>
<div class="container" id="modeloContainer">
	<div class="row">
		<div class="one-half column" id="modeloLeftContainer">
			<ul>
				<li><h2>ENDOGENOUS CHANGE<br><span>Integrated solution</span></h2></li>
			</ul>
			<p class="greyText">In order to boost the internal processes of improvement, individual and organizational, in a context of change.</p>
		</div>
		<div class="one-half column">
			<img src="img/CMpuntos.png" class="u-max-full-width"/>
		</div>
	</div>
</div>
<div class="culturaContainer">
	<div class="container">
		<h1>HOW DO WE DO IT?</h1>
		<h2>WITH A <b>STRUCTURED WORKING CULTURE</b></h2>
		<div class="row" id="culturaContainerRow">
			<div class="one-third column">
				<img src="img/Advisory-How.png"/>
				<div class="pContainer">
					<p>We analyze the context of your company and design a value proposition according to your needs.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/Administration-How.png"/>
				<div class="pContainer">
					<p>We implemented the solution based on a project management methodology to show the progress and results.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/Attention-How.png"/>
				<div class="pContainer">
					<p>We maintain a culture of customer service based on quality and proximity.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactoContainer">
	<div class="container">
		<h1>DO NOT RESIST TO CHANGE</h1>
		<p>WRITE US AND WE WILL CONTACT YOU</p>
		<form class="formContacto">
			<label>Name</label>
			<input type="text" class="name"/>
			<label>Lastname</label>
			<input type="text" class="apellidos"/>
			<label>Email</label>
			<input type="text" class="email"/>
			<label>Phone number</label>
			<input type="number" class="phone"/>			
			<label>How we may help you?</label>
			<select class="service">
				<option value="0">Select an option</option>
				<option value="HEADHUNTING">HEADHUNTING</option>
				<option value="STAFF ADMINISTRATION">STAFF ADMINISTRATION</option>
				<option value="CHANGE MANAGEMENT" selected>CHANGE MANAGEMENT</option>
				<option value="MIGRATION">MIGRATION</option>
				<option value="FINANCE">FINANCE</option>
			</select>
			<label>Message</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>I agree to the <br><a target="_blank" href="../A3O-Avisodeprivacidad_2017.pdf">terms and conditions</a></p>
				<input type="submit" class="enviar" value="SEND">
			</div>
			
		</form>
		<div class="error"></div>
	</div>
	<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/UP-Buttom.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/service.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
</body>
</html>