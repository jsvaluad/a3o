<img class="barraFooter" src="img/barraFooter.png"/>
<div class="footerBackground">
	<div class="flexbox">
		<div class="col">
			<img src="img/logoFooter.png" class="logoFooter"/>
			<p class="footerText1" style="padding:20px 0 0 0;">CONTACT US:</p>
			<p class="footerText2">Álvaro Obregón 121, Col. Roma</p>
			<p class="footerText2">Norte, México D.F. C.P. 06700</p>
			<p class="footerText2">(+52 55) 4172 0660</p>
			<p class="footerText2">contacto@a3ogroup.com</p>
			<p class="footerText1" style="padding:20px 0 0 0;">FOLLOW US:</p>
			<div class="row">
				<div class="two columns" id="firstSocial"><a target="_blank" href="https://www.facebook.com/A3O.mx/"><img src="img/social/A3Oface.png"></a></div>
				<div class="two columns"><a target="_blank" href="https://www.linkedin.com/company-beta/2729495/"><img src="img/social/A3Olinked.png"></a></div>
				<div class="two columns"><a target="_blank" href="https://twitter.com/A3OGroup"><img src="img/social/A3OTwitter.png"></a></div>
				<div class="two columns"><a target="_blank" href="https://www.youtube.com/user/A3OGroup"><img src="img/social/A3OYoutube.png"></a></div>
			</div>
		</div>
		<div class="col">
			<p class="footerText1">SOLUTIONS</p>
			<p class="footerText2"><a href="headhunting.php">HEADHUNTING</a></p>
			<p class="footerText2"><a href="staff.php">STAFF ADMINISTRATION</a></p>
			<p class="footerText2"><a href="migration.php">MIGRATION</a></p>
			<p class="footerText2"><a href="change.php">CHANGE MANAGEMENT</a></p>
			<p class="footerText2"><a href="finance.php">FINANCE</a></p>

			<p class="footerText1" style="padding:20px 0 0 0;"><a href="weare.php">ABOUT A3O</a></p>
			<!--<p class="footerText2"><a href="nosotros.php#cultura">CULTURA DE TRABAJO</a></p>
			<p class="footerText2"><a href="nosotros.php#mision">MISIÓN Y VISIÓN</a></p>
			<p class="footerText2"><a href="#">CLIENTES</a></p>-->

			<p class="footerText1" style="padding:20px 0 0 0;"><a href="#">BLOG</a></p>
			<p class="footerText1" style="padding:20px 0 0 0;"><a href="contact.php">CONTACT</a></p>
			<p class="footerText1" style="padding:20px 0 0 0;"><a href="../index.php">SITIO EN ESPAÑOL</a></p>
		</div>
		<div class="col" id="lastColumn">
			<div class="thirdColumnBox">
				<p class="" id="fCandidatos"><a href="http://www.oxygio.com" target="_blank">CANDIDATES</a></p><br>
				<p class="" id="fEmpleados"><a href="">EMPLOYEES</a></p>
				<div class="searchContainer">
					<input class="inputSearch" type="text" placeholder="Type your search"/>
					<img class="submitSearch" src="img/search.png" alt="buscar"/>
				</div>
			</div>
		</div>
	</div>
	<div class="container" id="footerContainer">
		<div class="one-third column">
			<img src="img/logoFooter.png" class="logoFooter"/>
			<p class="footerText1" style="padding:20px 0 0 0;">CONTACT US:</p>
			<p class="footerText2">Álvaro Obregón 121, Col. Roma</p>
			<p class="footerText2">Norte, México D.F. C.P. 06700</p>
			<p class="footerText2">(+52 55) 4172 0660</p>
			<p class="footerText2">contacto@a3ogroup.com</p>

			<p class="footerText1" style="padding:20px 0 0 0;">FOLLOW US:</p>
			<a target="_blank" href="http://www.facebook.com/"><img src="img/social/A3Oface.png"></a>
			<a target="_blank" href="http://www.linkedin.com/"><img src="img/social/A3Olinked.png"></a>
			<a target="_blank" href="http://www.twitter.com/"><img src="img/social/A3OTwitter.png"></a>
			<a target="_blank" href="http://www.youtube.com/"><img src="img/social/A3OYoutube.png"></a>
		</div>
		<div class="one-third column">
			<p class="footerText1">SOLUTIONS</p>
			<p class="footerText2"><a href="headhunting.php">HEADHUNTING</a></p>
			<p class="footerText2"><a href="staff.php">STAFF ADMINISTRATION</a></p>
			<p class="footerText2"><a href="migration.php">MIGRATION</a></p>
			<p class="footerText2"><a href="change.php">CHANGE MANAGEMENT</a></p>
			<p class="footerText2"><a href="finance.php">FINANCE</a></p>

			<p class="footerText1" style="padding:20px 0 0 0;"><a href="weare.php">ACERCA DE A3O</a></p>
			<!--<p class="footerText2"><a href="nosotros.php#cultura">CULTURA DE TRABAJO</a></p>
			<p class="footerText2"><a href="nosotros.php#mision">MISIÓN Y VISIÓN</a></p>
			<p class="footerText2"><a href="#">CLIENTES</a></p>-->

			<p class="footerText1" style="padding:20px 0 0 0;"><a href="">BLOG</a></p>
			<p class="footerText1" style="padding:20px 0 0 0;"><a href="contact.php">CONTACT</a></p>
			<p class="footerText1" style="padding:20px 0 0 0;"><a href="../index.php">SITIO EN ESPAÑOL</a></p>
		</div>
		<div class="one-third column">
			<p class="" id="fCandidatos"><a href="#">CANDIDATES</a></p><br>
			<p class="" id="fEmpleados"><a href="#">EMPLOYEE</a></p>
			<div class="searchContainer">
				<input class="inputSearch" type="text" placeholder="Type your search"/>
				<img class="submitSearch" src="img/search.png" alt="buscar"/>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<ul class="footerList">
				<li><p class="footerText2">A<span>3</span>O <?php /*echo date("Y");*/?> - All Right Restricted</p></li>
				<li><a href="#">Site Map</a></li>
				<li><a target="_blank" href="../A3O-Avisodeprivacidad_2017.pdf">Privacy Policy</a></li>
			</ul>
		</div>
	</div>
</div>
