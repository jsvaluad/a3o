$(document).ready(function(){
	$(".nosotros").css({"font-weight":"700","color":"#0a1a72"});
	
	$(".subir").on("click",function(){
		$("html, body").animate({ scrollTop: 0 }, "slow");
	});
	    
	function pad(num, size) {
	    var s = num+"";
	    while (s.length < size) s = "0" + s;
	    return s;
	}
	var index = 0;
	function changeBack(){
		if(index>=239){
			index = 0;
		}
		i = pad(index,5);
		//console.log(i);
		var back2 = "img/pira/small/Piramide-giro_"+i+".png";
		$(".pira").attr("src",back2);
		index = index + 2;
	}
	
	var timer = null;
	function startSetInterval() {timer = setInterval( changeBack, 100);}
	
	// start function on page load
	/*startSetInterval();
	$('.pira').hover(function(ev){
		clearInterval(timer);
	}, function(ev){
		startSetInterval();
	});*/
	
	//MOBILE HOVER
	/*$('.pira').bind('touchstart', function() {clearInterval(timer);});
	$('.pira').bind('touchend', function() {startSetInterval();});*/
	
	
	/*	DETECT BROWSER	*/
	var BrowserDetect = {
	        init: function () {
	            this.browser = this.searchString(this.dataBrowser) || "Other";
	            this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
	        },
	        searchString: function (data) {
	            for (var i = 0; i < data.length; i++) {
	                var dataString = data[i].string;
	                this.versionSearchString = data[i].subString;
	
	                if (dataString.indexOf(data[i].subString) !== -1) {
	                    return data[i].identity;
	                }
	            }
	        },
	        searchVersion: function (dataString) {
	            var index = dataString.indexOf(this.versionSearchString);
	            if (index === -1) {
	                return;
	            }
	
	            var rv = dataString.indexOf("rv:");
	            if (this.versionSearchString === "Trident" && rv !== -1) {
	                return parseFloat(dataString.substring(rv + 3));
	            } else {
	                return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
	            }
	        },
	
	        dataBrowser: [
	            {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
	            {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
	            {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
	            {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
	            {string: navigator.userAgent, subString: "Opera", identity: "Opera"},  
	            {string: navigator.userAgent, subString: "OPR", identity: "Opera"},  
	
	            {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"}, 
	            {string: navigator.userAgent, subString: "Safari", identity: "Safari"}       
	        ]
	    };
    
    /*BrowserDetect.init();
    if(BrowserDetect.browser == 'Explorer'){
    	console.log("explorer");
    }*/
    
	if( (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0){
		console.log("// Opera 8.0+");
	}else{
		if( isFirefox = typeof InstallTrigger !== 'undefined' ){
			console.log("Firefox 1.0+");
			$("#spriteContainer").css({
				"background-image": "url('../img/pira/piraFirefox2.png')",
				"animation":"spriteF 5s steps(11) infinite",
				//"display":"none"
			});
			//$(".pira").css({"display":"block"});
		}else{
			if(Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0){
				console.log("  // At least Safari 3+");
			}else{
				if(/*@cc_on!@*/false || !!document.documentMode){
					console.log("// Internet Explorer 6-11");
					//$("#spriteContainer").css({"-ms-animation":"sprite 15s steps(120) infinite"});
				}else{
					if(!isIE && !!window.StyleMedia){
						console.log("Edge 20+");
					}else{
						if(!!window.chrome && !!window.chrome.webstore){
							console.log("// Chrome 1+");
						}else{
							if((isChrome || isOpera) && !!window.CSS){
								console.log("// Blink engine detection");
							}
						}
					}
				}
			
			}
		}
	} 
	
});