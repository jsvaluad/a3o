$("document").ready(function(){
	
	$(".subir").on("click",function(){
		$("html, body").animate({ scrollTop: 0 }, "slow");
	});
	
	function isEmail(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}
	
	function checkForm(){
		$(".formContacto input[type='text'],.formContacto input[type='number'],.formContacto select,.formContacto textarea").css({"background-color":"rgb(255,255,255)"});
		var goOn = 1;$(".error p").remove();
		var name = $(".name").val();
		if(name == ""){$(".name").css({"background-color":"rgba(253, 130, 4, 1)"});goOn = 0;}
		var apellidos = $(".apellidos").val();
		if(apellidos == ""){$(".apellidos").css({"background-color":"rgba(253, 130, 4, 1)"});goOn = 0;}
		var email = $(".email").val();
		if(email == ""){$(".email").css({"background-color":"rgba(253, 130, 4, 1)"});goOn = 0;}
		if(!isEmail(email)){$(".email").css({"background-color":"rgba(253, 130, 4, 1)"});goOn = 0;}
		var status = $(".status").val();
		if(status == "0"){$(".status").css({"background-color":"rgba(253, 130, 4, 1)"});goOn = 0;}
		var mensaje = $(".mensaje").val();
		if(mensaje == "0"){$(".mensaje").css({"background-color":"rgba(253, 130, 4, 1)"});goOn = 0;}
		if(goOn == 0){
			var apendhtml = "<p>Llenar todos los campos</p>";
			$(".error").append(apendhtml);
			$(".error").show().delay(5000).fadeOut();
			return false;
		}
		if(!$(".acepto").is(':checked')){
			var apendhtml = "<p>Aceptar los términos y condiciones</p>";
			$(".error").append(apendhtml);
			$(".error").show().delay(5000).fadeOut();
			return false;
		}
		var fd = new FormData();    
    		fd.append( 'name', name );
		fd.append( 'apellidos', apellidos);
		fd.append( 'email', email );
		fd.append( 'status', status);
		fd.append( 'mensaje', mensaje);
		return fd;
		
	}
	
	$(".formContacto").on("submit",function(e){
		e.preventDefault();
		var datos = checkForm();
		if(!datos){
			return false;	
		}
		else{
			$.ajax({
				url: 'script/enviarCorreoContacto.php',
				data: datos,
				processData: false,
				contentType: false,
				type: 'POST',
				beforeSend:function(){
					$(".error p").remove();
				},
				success: function(data){
				    	console.log(data);
					var apendhtml = "<p>"+data+"</p>";
					$(".error").append(apendhtml);
					$(".error").show().delay(5000).fadeOut();
					$(".formContacto input[type='text'],.formContacto input[type='number'],.formContacto textarea").val("");
					$(".formContacto select").val("HEADHUNTING");
					$(".acepto").prop('checked',false);
					
				},
				error: function (jqXHR, exception) {
			            var msg = '';
			            if (jqXHR.status === 0) {
			                msg = 'Not connect.\n Verify Network.';
			            } else if (jqXHR.status == 404) {
			                msg = 'Requested page not found. [404]';
			            } else if (jqXHR.status == 500) {
			                msg = 'Internal Server Error [500].';
			            } else if (exception === 'parsererror') {
			                msg = 'Requested JSON parse failed.';
			            } else if (exception === 'timeout') {
			                msg = 'Time out error.';
			            } else if (exception === 'abort') {
			                msg = 'Ajax request aborted.';
			            } else {
			                msg = 'Uncaught Error.\n' + jqXHR.responseText;
			            }
			            alert(msg);
			        }
			});
		}
	});
});