<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>STAFF ADMINISTRATION</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/staff.css">	
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>
<div class="blankSpace">&nbsp;</div>
<div class="videoContainer">
	<div id="videoBox">
		<h1>FACILITAMOS LOS PROCESOS DE ADMINISTRACIÓN DE PERSONAL Y NÓMINA PARA TU EMPRESA</h1>
	</div>
</div>

<div id="descriptionContainer">
	<div class="row">
		<div class="one-third column">
			<img src="img/Sicon.png" class="u-max-full-width"/>
		</div>
		<div class="two-thirds column">
			<h1>STAFF ADMINISTRATION</h1>
			<p class="sectionDescription">Garantizamos el control y atención de tu personal para reducir riesgos y contingencias laborales en tu empresa.</p>
			<p class="orangeText">Para apoyarte a <b>CREAR VALOR</b></p>
			<ul class="descriptionList">
				<li>Eliminar errores de cálculo de nómina, contingencias y riesgos laborales.</li>
				<li>Respaldo de expertos especializados en IMSS-Nómina y RRHH.</li>
				<li>Asesoría y Atención a Empresa y Empleado.</li>
				<li>Consulta online de información e incidencias del personal las 24 hrs.</li>
				<li>Implementación de Acuerdos de Servicio en cada etapa.</li>
				<li>Creamos una solución personalizada a tus necesidades.</li>
			</ul>
		</div>
	</div>
</div>
<div class="serviciosContainer">
	<div class="container" id="servicioBox">
		<h1>¿CÓMO TE APOYAMOS?</h1>
		<h2 class="h2Web">SOLUCIONES <b>BACK OFFICE</b> | <b>STAFF ADMINISTRATION</b></h2>
		<h2 class="h2Mobile">SOLUCIONES<br><b>BACK OFFICE</b><br><b>STAFF ADMINISTRATION</b></h2>
		<div class="row">
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>ADMINISTRACIÓN DE PERSONAL</b></p>	
					</div>
					<p class="servicioDescription">Contratamos al capital humano que necesitas cumpliendo con tus responsabilidades patronales.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>ADMINISTRACIÓN DE NÓMINA</b></p>	
					</div>
					<p class="servicioDescription">Procesamos la nómina de tu empresa con exactitud y seguridad para ti y tu personal dentro del marco normativo.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseRoja.png"/><p class="servicioTitle"><b>STAFFING</b></p>	
					</div>
					<p class="servicioDescription">Reclutamos y administramos al personal para tu empresa acorde a tus necesidades estratégicas.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="socialBox">
	<div class="container">
		<div class="row">
			<div class="one-half column"><p>¿Conoces a alguien a quién podría interesarle?<br><b>COMPARTE ESTE SERVICIO</b></p></div>
			<div class="one-half column">
				<a href="https://www.facebook.com/A3O.mx/" target="_blank"><img src="img/socialF.png"/></a>
				<a href="https://mx.linkedin.com/company/a3o-group" target="_blank"><img src="img/socialL.png"/></a>
				<a href=" https://twitter.com/a3ogroup" target="_blank"><img src="img/socialT.png"/></a>
			</div>
		</div>
	</div>
</div>
<div class="container" id="modeloContainer">
	<div class="row">
		<div class="one-half column" id="modeloLeftContainer">
			<ul>
				<li><h2>RESPALDANDO TUS OBLIGACIONES PATRONALES</h2></li>
			</ul>
			<p class="greyText">CON BASE EN UN MODELO MULTIDIMENSIONAL</p>
			<p class="blackText"><b>Creamos una solución personalizada</b> para la gestión de tu capital humano.</p>
		</div>
		<div class="one-half column">
			<img src="img/Spuntos.png" class="u-max-full-width"/>
		</div>
	</div>
</div>
<div class="culturaContainer">
	<div class="container">
		<h1>¿CÓMO LO HACEMOS?</h1>
		<h2>CON UNA <b>CULTURA DE TRABAJO ESTRUCTURADA</b></h2>
		<div class="row" id="culturaContainerRow">
			<div class="one-third column">
				<img src="img/HHasesoria.png"/>
				<div class="pContainer">
					<p>Te apoyamos en estructurar una solución para facilitar la administración de personal y nómina para cumplir con tus necesidades estratégicas.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/HHadministracion.png"/>
				<div class="pContainer">
					<p>Te acompañamos con procesos eficientes y herramientas accesibles y online para la mejora de tu área de recursos humanos.</p>
				</div>
			</div>	
			<div class="one-third column">
				<img src="img/HHatencion.png"/>
				<div class="pContainer">
					<p>Te atendemos de manera continua y personalizada en tus solicitudes y las de tu personal.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactoContainer">
	<div class="container">
		<h1>NÓMINA EXACTA, SIN CONTRATIEMPOS Y PERSONAL SIN RIESGOS</h1>
		<p>ESCRÍBENOS Y NOS PONDREMOS EN CONTACTO CONTIGO</p>
		<form class="formContacto">
			<label>Nombre</label>
			<input type="text" class="name"/>
			<label>Apellidos</label>
			<input type="text" class="apellidos"/>
			<label>Correo electrónico</label>
			<input type="text" class="email"/>
			<label>Teléfono</label>
			<input type="number" class="phone"/>			
			<label>¿Cómo podemos apoyarte?</label>
			<select class="service">
				<option value="0">Selecciona una opcion</option>
				<option value="HEADHUNTING">HEADHUNTING</option>
				<option value="STAFF ADMINISTRATION" selected>STAFF ADMINISTRATION</option>
				<option value="CHANGE MANAGEMENT">CHANGE MANAGEMENT</option>
				<option value="MIGRATION" >MIGRATION</option>
				<option value="FINANCE">FINANCE</option>
			</select>
			<label>Mensaje</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>Acepto los<br><a target="_blank" href="A3O-Avisodeprivacidad_2017.pdf">términos y condiciones</a></p>
				<input type="submit" class="enviar" value="">
			</div>
			
		</form>
		<div class="error"></div>
	</div>
	<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/subir.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
  <script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/service.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
<script type="text/javascript" src="script/googleAnalytics.js"></script>
</body>
</html>