<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>MIGRATION </title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/migration.css">	
	<link rel="stylesheet" href="css/lightbox.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>

<div class="videoContainer">
	<div id="videoBox">
		<h1>FACILITA EL INGRESO DE PERSONAL EXTRANJERO EN TU EMPRESA.</h1>
		<p>HAZ CLIC Y CONOCE MÁS</p>
		<a href="#" id="video" class="slvj-link-lightbox 2" data-videoid="xPN9zsn-AHs" data-videosite="youtube"><img class="videoPlay" src="img/videoPlay.png"></a><br>
	</div>
</div>

<div class="container" id="descriptionContainer">
	<div class="row">
		<div class="one-third column">
			<img src="img/Micon.png" class="u-max-full-width"/>
		</div>
		<div class="two-thirds column">
			<h1>MIGRATION SERVICES</h1>
			<p class="sectionDescription">Asistimos a tu personal extranjero en cada etapa del procedimiento migratorio a través de un procesos y asistencia que facilitará su estancia.</p>
			<p class="orangeText">Para apoyarte a <b>CREAR VALOR</b></p>
			<ul class="descriptionList">
				<li>Ideal para contratación de personal extranjero.</li>
				<li>Solución adaptable: individual o familiar.</li>
				<li>Atención personalizada en cada etapa.</li>
				<li>Servicio de confianza, seguridad y confidencialidad.</li>
				<li>Gestión de trámites migratorias, fiscales para extranjeros.</li>
			</ul>
		</div>
	</div>
</div>
<div class="serviciosContainer">
	<div class="container" id="servicioBox">
		<h1>¿CÓMO TE APOYAMOS?</h1>
		<h2 class="h2Web">SOLUCIONES <b>BACK OFFICE</b> | <b>MIGRATION</b></h2>
		<h2 class="h2Mobile">SOLUCIONES<br><b>BACK OFFICE</b><br><b>MIGRATION</b></h2>
		<div class="row">
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>TRÁMITES MIGRATORIOS</b></p>	
					</div>
					<p class="servicioDescription">Gestionamos por completo el proceso migratorio de tu personal extranjero y sus familias para asegurar su estancia legal en México.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>TRÁMITES FISCALES</b></p>	
					</div>
					<p class="servicioDescription">Gestionamos los trámites fiscales para cumplir con sus obligaciones tributarias.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseRoja.png"/><p class="servicioTitle"><b>TRÁMITES ESPECIALES</b></p>	
					</div>
					<p class="servicioDescription">Gestiones especiales de trámites y solicitudes de documentación oficial.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="socialBox">
	<div class="container">
		<div class="row">
			<div class="one-half column"><p>¿Conoces a alguien a quién podría interesarle?<br><b>COMPARTE ESTE SERVICIO</b></p></div>
			<div class="one-half column">
				<a href="https://www.facebook.com/A3O.mx/" target="_blank"><img src="img/socialF.png"/></a>
				<a href="https://mx.linkedin.com/company/a3o-group" target="_blank"><img src="img/socialL.png"/></a>
				<a href=" https://twitter.com/a3ogroup" target="_blank"><img src="img/socialT.png"/></a>
			</div>
		</div>
	</div>
</div>
<div class="container" id="modeloContainer">
	<div class="row">
		<div class="one-half column" id="modeloLeftContainer">
			<ul>
				<li><h2>RESPALDO EN CADA MOMENTO</h2></li>
			</ul>
			<p class="blackText">Para tu personal extranjero, desde el primer contacto para saber sus requisitos, migratorios, hasta concluir con sus trámites.</b></p>
		</div>
		<div class="one-half column">
			<img src="img/Mpuntos.png" class="u-max-full-width"/>
		</div>
	</div>
</div>
<div class="culturaContainer">
	<div class="container">
		<h1>¿CÓMO LO HACEMOS?</h1>
		<h2>CON UNA <b>CULTURA DE TRABAJO ESTRUCTURADA</b></h2>
		<div class="row" id="culturaContainerRow">
			<div class="one-third column">
				<img src="img/HHasesoria.png"/>
				<div class="pContainer">
					<p>Un experto te orienta con base en la normatividad de los trámites migratorios que necesitas.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/HHadministracion.png"/>
				<div class="pContainer">
					<p>Gestionamos tu trámites ante las autoridades correspondientes y te informamos del avance hasta su término.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/HHatencion.png"/>
				<div class="pContainer">
					<p>Brindamos seguimiento anticipado a solicitudes de renovaciones y actualizaciones de documentación.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactoContainer">
	<div class="container">
		<h1>LIBÉRATE DE TRÁMITES</h1>
		<p>ESCRÍBENOS Y NOS PONDREMOS EN CONTACTO CONTIGO</p>
		<form class="formContacto">
			<label>Nombre</label>
			<input type="text" class="name"/>
			<label>Apellidos</label>
			<input type="text" class="apellidos"/>
			<label>Correo electrónico</label>
			<input type="text" class="email"/>
			<label>Teléfono</label>
			<input type="number" class="phone"/>			
			<label>¿Cómo podemos apoyarte?</label>
			<select class="service">
				<option value="0">Selecciona una opcion</option>
				<option value="HEADHUNTING">HEADHUNTING</option>
				<option value="STAFF ADMINISTRATION">STAFF ADMINISTRATION</option>
				<option value="CHANGE MANAGEMENT">CHANGE MANAGEMENT</option>
				<option value="MIGRATION" selected>MIGRATION</option>
				<option value="FINANCE">FINANCE</option>
			</select>
			<label>Mensaje</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>Acepto los<br><a target="_blank" href="A3O-Avisodeprivacidad_2017.pdf">términos y condiciones</a></p>
				<input type="submit" class="enviar" value="">
			</div>
			
		</form>
		<div class="error"></div>
	</div>
	<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/subir.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/service.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
<script type="text/javascript" src="script/googleAnalytics.js"></script>
</body>
</html>