<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>Home</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/index.css">	
	<link rel="stylesheet" href="css/lightbox.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>

<div class="videoContainer">
	<div id="videoBox">
		<h1>SOMOS EL SOCIO ESTRATÉGICO</h1>
		<h4>QUE TE APOYA A <b>CREAR VALOR</b> EN TU EMPRESA</h4>
		<p>HAZ CLICK Y CONOCE MÁS</p>
		<a href="#" id="video" class="slvj-link-lightbox 2" data-videoid="Ur7NgsD4Pmc" data-videosite="youtube"><img class="videoPlay" src="img/videoPlay.png"></a><br>
	</div>
</div><div class="socioContainer"><div class="socioBox">
		<p class="socioTitle">A<span>3</span>O <b>ES TU SOCIO ESTRATÉGICO</b></p>
		<p class="socioText">COMO EXPERTOS, TE <b>ACOMPAÑAMOS</b> PARA MEJORAR TUS ÁREAS DE <b>RECURSOS HUMANOS Y FINANZAS</b> CON SOLUCIONES <b>BACK OFFICE</b></p>
	</div>
</div>
<div class="container" id="solucionesContainer">
	<div class="row"><p class="solucionesTitle">SOLUCIONES <b>BACK OFFICE</b></p></div>
	<div class="row">
		<div class="one-third column">
			<img src="img/headhuntingIcon.png" class="icon">
			<div class="iconContainer">
				<div class="titleContainer">
					<p class="iconTitle">HEADHUNTING</p>
				</div>
				<div class="descContainer">
					<p class="iconDesc">Atraemos al talento ideal para que forme parte de tu empresa.</p>
				</div>
				<div class="linkContainer">
					<a class="linkIcon" href="headhunting.php">LEER MAS</a>
				</div>
			</div>
		</div>
		<div class="one-third column">
			<img src="img/staffIcon.png" class="icon">
			<div class="iconContainer">
				<div class="titleContainer">
					<p class="iconTitle">STAFF ADMINISTRATION</p>
				</div>
				<div class="descContainer">
					<p class="iconDesc">Controlamos los procesos de administración de personal y nómina eficientemente, para protegerte de riesgos y contingencias laborales.</p>
				</div>
				<div class="linkContainer">
					<a class="linkIcon" href="staff.php">LEER MAS</a>
				</div>
			</div>	
		</div>
		<div class="one-third column">
			<img src="img/changeIcon.png" class="icon">
			<div class="iconContainer">
				<div class="titleContainer">
					<p class="iconTitle">CHANGE MANAGEMENT</p>
				</div>
				<div class="descContainer">
					<p class="iconDesc">Impulsamos a tu empresa y equipo de trabajo para lograr un desarrollo organizacional.</p>
				</div>
				<div class="linkContainer">
					<a class="linkIcon" href="change.php">LEER MAS</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row" id="secondRow">
		<div class="one-half column">
			<img src="img/migrationIcon.png" class="icon">
			<div class="iconContainer">
				<div class="titleContainer">
					<p class="iconTitle">MIGRATION</p>
				</div>
				<div class="descContainer">
					<p class="iconDesc">Gestionamos los trámites migratorios para la estancia legal del personal extranjero que forma parte de tu empresa.</p>
				</div>
				<div class="linkContainer">
					<a class="linkIcon" href="migration.php">LEER MAS</a>
				</div>
			</div>
		</div>
		<div class="one-half column">
			<img src="img/financeIcon.png" class="icon">
			<div class="iconContainer">
				<div class="titleContainer">
					<p class="iconTitle">FINANCE</p>
				</div>
				<div class="descContainer">
					<p class="iconDesc">Facilitamos información confiable y exacta para la mejor toma de decisiones financieras.</p>
				</div>
				<div class="linkContainer">
					<a class="linkIcon" href="finance.php">LEER MAS</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="culturaBackground">
	<div class="culturaContainer">
		<img id="icoGraph" class="admin" src="img/administracion.png"/>
		<img id="icoGraph" class="atencion" src="img/atencion.png"/>
		<img id="icoGraph" class="asesoria" src="img/asesoria.png"/>
		<img class="rueda" src="img/ruedaA3O.png" alt="rueda"/>
		<div class="subirContainer"><img class="subir" src="img/subir.png"/></div>
	</div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/index.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
<script type="text/javascript" src="script/googleAnalytics.js"></script>
</body>
</html>