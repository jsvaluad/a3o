<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>FINANCE</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/finance.css">	
	<link rel="stylesheet" href="css/lightbox.min.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>

<div class="videoContainer">
	<div id="videoBox">
		<h1>CON UN SOCIO ESTRATÉGICO.</h1>
		<h2>AHORA TOMAR DECISIONES FINANCIERAS ES MÁS SIMPLE</h2>
		<p>HAZ CLIC Y CONOCE MÁS</p>
		<a href="#" id="video" class="slvj-link-lightbox 2" data-videoid="Ntp5ifzXH1k" data-videosite="youtube"><img class="videoPlay" src="img/videoPlay.png"></a><br>
	</div>
</div>

<div class="container" id="descriptionContainer">
	<div class="row">
		<div class="one-third column">
			<img src="img/Ficon.png" class="u-max-full-width"/>
		</div>
		<div class="two-thirds column">
			<h1>FINANCE</h1>
			<p class="sectionDescription">Te acompañamos en la administración financiera de tu empresa con el respaldo de expertos que hacen oportuno el cumplimiento de tus obligaciones financieras.</p>
			<p class="orangeText">Para apoyarte a <b>CREAR VALOR</b></p>
			<ul class="descriptionList">
				<li>Estructuración de Esquema Contable.</li>
				<li>Elaboración y análisis de Información Contable.</li>
				<li>Asesoría y Estructuración Fiscal.</li>
				<li>Elaboración y Declaraciones Fiscales.</li>
				<li>Elaboracion de KPI´s Financieros.</li>
			</ul>
		</div>
	</div>
</div>
<div class="serviciosContainer">
	<div class="container" id="servicioBox">
		<h1>¿CÓMO TE APOYAMOS?</h1>
		<h2 class="h2Web">SOLUCIONES <b>BACK OFFICE</b> | <b>FINANCE</b></h2>
		<h2 class="h2Mobile">SOLUCIONES<br><b>BACK OFFICE</b><br><b>FINANCE</b></h2>
		<div class="row">
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseAzul.png"/><p class="servicioTitle"><b>CONTABILIDAD</b></p>	
					</div>
					<p class="servicioDescription">Elaboramos y analizamos tu información contable que te permitirá entender el estado financiero actual de tu empresa.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseNaranaja.png"/><p class="servicioTitle"><b>IMPUESTOS</b></p>	
					</div>
					<p class="servicioDescription">Asesoramos en materia fiscal, calculamos y presentamos tus impuestos para el cumplimiento oportuno de tus obligaciones tributarias.</p>
				</div>
			</div>
			<div class="one-third column">
				<div class="serviceCaja">
					<div class="serviceTitleContainer">
						<img src="img/elipseRoja.png"/><p class="servicioTitle"><b>ADMINISTRACIÓN FINANCIERA</b></p>	
					</div>
					<p class="servicioDescription">Analizamos tu gestión financiera por medio de indicadores y herramientas en línea para que evalues el desempeño de tu empresa.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="socialBox">
	<div class="container">
		<div class="row">
			<div class="one-half column"><p>¿Conoces a alguien a quién podría interesarle?<br><b>COMPARTE ESTE SERVICIO</b></p></div>
			<div class="one-half column">
				<a href="https://www.facebook.com/A3O.mx/" target="_blank"><img src="img/socialF.png"/></a>
				<a href="https://mx.linkedin.com/company/a3o-group" target="_blank"><img src="img/socialL.png"/></a>
				<a href=" https://twitter.com/a3ogroup" target="_blank"><img src="img/socialT.png"/></a>
			</div>
		</div>
	</div>
</div>
<div class="container" id="modeloContainer">
	<div class="row">
		<div class="one-half column" id="modeloLeftContainer">
			<ul>
				<li><h2>FACILITANDO TUS DECISIONES FINANCIERAS<br><span>CON INFORMACIÓN ASERTIVA</span></h2></li>
			</ul>
			<p class="greyText">APOYAMOS EL DESEMPEÑO FINANCIERO DE TU EMPRESA CON EL PRINCIPIO:<br><b>"MEDIR PARA MEJORAR"</b></p>
		</div>
		<div class="one-half column">
			<img src="img/Fpuntos.png" class="u-max-full-width"/>
		</div>
	</div>
</div>
<div class="culturaContainer">
	<div class="container">
		<h1>¿CÓMO LO HACEMOS?</h1>
		<h2>CON UNA <b>CULTURA DE TRABAJO ESTRUCTURADA</b></h2>
		<div class="row" id="culturaContainerRow">
			<div class="one-third column">
				<img src="img/HHasesoria.png"/>
				<div class="pContainer">
					<p>Te orientamos para que cumplas correctamente tus obligaciones financieras a través de una solución adaptada a tus necesidades.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/HHadministracion.png"/>
				<div class="pContainer">
					<p>Procesamos y analizamos tu información contable-fiscal y financiera con procesos eficientes, controlados y acorde a tus requerimientos.</p>
				</div>
			</div>
			<div class="one-third column">
				<img src="img/HHatencion.png"/>
				<div class="pContainer">
					<p>Te respaldamos en la interpretación financiera, aclaramos tus consultas contables, y te informamos de las actualizaciones fiscales.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactoContainer">
	<div class="container">
		<h1>PON AL DÍA TUS FINANZAS</h1>
		<p>ESCRÍBENOS Y NOS PONDREMOS EN CONTACTO CONTIGO</p>
		<form class="formContacto">
			<label>Nombre</label>
			<input type="text" class="name"/>
			<label>Apellidos</label>
			<input type="text" class="apellidos"/>
			<label>Correo electrónico</label>
			<input type="text" class="email"/>
			<label>Teléfono</label>
			<input type="number" class="phone"/>			
			<label>¿Cómo podemos apoyarte?</label>
			<select class="service">
				<option value="0">Selecciona una opcion</option>
				<option value="HEADHUNTING">HEADHUNTING</option>
				<option value="STAFF ADMINISTRATION">STAFF ADMINISTRATION</option>
				<option value="CHANGE MANAGEMENT">CHANGE MANAGEMENT</option>
				<option value="MIGRATION">MIGRATION</option>
				<option value="FINANCE" selected>FINANCE</option>
			</select>
			<label>Mensaje</label>
			<textarea class="mensaje"></textarea>
			<div class="formFooter">
				<input type="checkbox" id="check" class="acepto"><label for="check" class="acepto2"></label>
				<p>Acepto los<br><a target="_blank" href="A3O-Avisodeprivacidad_2017.pdf">términos y condiciones</a></p>
				<input type="submit" class="enviar" value="">
			</div>
			
		</form>
		<div class="error"></div>
	</div>
	<div class="subirBox"><div class="subirContainer"><img class="subir" src="img/subir.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="script/lightbox.js"></script>
<script type="text/javascript" src="script/service.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
<script type="text/javascript" src="script/googleAnalytics.js"></script>
</body>
</html>