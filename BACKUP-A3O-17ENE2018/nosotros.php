<!doctype html>
<html>
  <head>
  	<meta http-equiv="Cache-control" content="max-age=2592000, public">
	<title>Home</title>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  	<link rel="stylesheet" href="css/skeleton.css">
  	<link rel='stylesheet' href='css/header.css' type='text/css'>
	<link rel="stylesheet" href="css/nosotros.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<link rel="icon" type="image/png" href="img/unnamed.ico">
  </head>
<body>
<?php include("header.php");?>
<div class="bannerContainer">
	<img class="full" src="img/acercaBanner.png"/>
</div>
<div class="container" id="cultura">
	<h1>CONOCE LA <b>CULTURA A<span style="font-size:30px;">3</span>O</b></h1>
	<div class="row">
		<div class="one-third column"><img class="agente" src="img/agenteAzul.png"/><p class="culturaText"><b>Entendemos tus necesidades</b> para proponerte soluciones ideales para tu empresa.</p></div>
		<div class="one-third column"><img class="agente" src="img/agenteNaranja.png"/><p class="culturaText">Aplicamos la solución con <b>procesos controlados y herramientas online</b> que garantizan el cumplimiento de tus operaciones.</p></div>
		<div class="one-third column"><img class="agente" src="img/agenteRojo.png"/><p class="culturaText">Expertos te acompañan en cada etapa para responder a tus solicitudes y <b>te respaldamos cuando lo necesites.</b></p></div>
	</div>
</div>
<div class="ventajasContainer">
<div class="container">
	<div class="row">
		<div class="one-half column" id="bullets">
			<h3>VENTAJAS DE CONTAR CON <b>A<span style="font-size:25px;">3</span>O COMO SOCIO ESTRATÉGICO</b></h3>
			<ul>
				<li><p><b>Cultura de Trabajo estructurada</b><br>Aseguramos un proceso de mejora constante en nuestros servicios.</p></li>
				<li><p><b>Solución completa</b><br>Integramos las principales actividades de Recursos Humanos y Finanzas.</p></li>
				<li><p><b>Respaldo de Expertos</b><br>Te acompañamos de manera efectiva.</p></li>
				<li><p><b>Adaptabilidad</b><br>Estructuramos soluciones desde empresas pequeñas hasta organizaciones multinacionales.</p></li>
			</ul>
		</div>
		<div class="one-half column">
			<div id=spriteContainer>
			</div>
			<div class="gifContainer">
				<img src="img/pira/Piramide-giro(460x500)/Piramide-giro(460x500).gif" class="pira">
			</div>
		</div>
	</div>
</div>
</div>
<div class="misionContainer" id="mision">
	<div class="container">
		<p class="socioTitle">NUESTRA <b>MISIÓN</b></p>
		<p class="socioText"><b>SER TU SOCIO ESTRATÉGICO</b></p>
	</div><div class="subirBox"><div class="subirContainer"><img class="subir" src="img/subir.png"/></div></div>
</div>

<?php include("footer.php");?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>-->
<script type="text/javascript" src="script/nosotros.js"></script>
<script type="text/javascript" src="script/chat.js"></script>
<script type="text/javascript" src="script/googleAnalytics.js"></script>
</body>
</html>